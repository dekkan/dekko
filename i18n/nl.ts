<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="nl">
<context>
    <name>AccountSettingsList</name>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/settings/AccountSettingsList.qml" line="26"/>
        <source>Account Settings: %1</source>
        <translation>Accountinstellingen: %1</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/settings/AccountSettingsList.qml" line="56"/>
        <source>Details</source>
        <translation>Details</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/settings/AccountSettingsList.qml" line="60"/>
        <source>Incoming Server</source>
        <translation>Inkomende server</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/settings/AccountSettingsList.qml" line="64"/>
        <source>Outgoing Server</source>
        <translation>Uitgaande server</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/settings/AccountSettingsList.qml" line="68"/>
        <source>Copies and Folders</source>
        <translation>Kopieën en mappen</translation>
    </message>
</context>
<context>
    <name>AccountSetup</name>
    <message>
        <location filename="../Dekko/stores/Accounts/AccountSetup.qml" line="220"/>
        <source>Invalid email address</source>
        <translation>Het e-mailadres is ongeldig</translation>
    </message>
    <message>
        <location filename="../Dekko/stores/Accounts/AccountSetup.qml" line="536"/>
        <source>Sent with Dekko</source>
        <translation>Verstuurd met Dekko</translation>
    </message>
</context>
<context>
    <name>AccountsWorker</name>
    <message>
        <location filename="../Dekko/workers/AccountsWorker.qml" line="36"/>
        <location filename="../Dekko/workers/AccountsWorker.qml" line="90"/>
        <source>Account removal failed</source>
        <translation>Het account kan niet worden verwijderd</translation>
    </message>
    <message>
        <location filename="../Dekko/workers/AccountsWorker.qml" line="54"/>
        <source>Remove %1</source>
        <translation>%1 verwijderen</translation>
    </message>
    <message>
        <location filename="../Dekko/workers/AccountsWorker.qml" line="54"/>
        <source>Are you sure you wish to remove this account?</source>
        <translation>Weet je zeker dat je dit account wilt verwijderen?</translation>
    </message>
    <message>
        <location filename="../Dekko/workers/AccountsWorker.qml" line="87"/>
        <source>Account removed</source>
        <translation>Het account is verwijderd</translation>
    </message>
</context>
<context>
    <name>AddAnotherUI</name>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/setupwizard/components/AddAnotherUI.qml" line="31"/>
        <source>Success</source>
        <translation>Voltooid</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/setupwizard/components/AddAnotherUI.qml" line="90"/>
        <source>New account created.</source>
        <translation>Het account is aangemaakt.</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/setupwizard/components/AddAnotherUI.qml" line="99"/>
        <source>Continue</source>
        <translation>Doorgaan</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/setupwizard/components/AddAnotherUI.qml" line="115"/>
        <source>Add another</source>
        <translation>Nog een account toevoegen</translation>
    </message>
</context>
<context>
    <name>AddressBookList</name>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/contacts/AddressBookList.qml" line="12"/>
        <source>Addressbooks</source>
        <translation>Adresboeken</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/contacts/AddressBookList.qml" line="65"/>
        <source>Add Collection</source>
        <translation>Verzameling toevoegen</translation>
    </message>
</context>
<context>
    <name>AttachmentPanel</name>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/components/AttachmentPanel.qml" line="85"/>
        <source>Attachments</source>
        <translation>Bijlagen</translation>
    </message>
</context>
<context>
    <name>AttachmentPopover</name>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/popovers/AttachmentPopover.qml" line="46"/>
        <source>Delete</source>
        <translation>Verwijderen</translation>
    </message>
</context>
<context>
    <name>AuthenticationSelector</name>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/components/AuthenticationSelector.qml" line="45"/>
        <source>Authentication</source>
        <translation>Verificatie</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/components/AuthenticationSelector.qml" line="60"/>
        <source>PLAIN</source>
        <translation>PLAIN</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/components/AuthenticationSelector.qml" line="61"/>
        <source>LOGIN</source>
        <translation>LOGIN</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/components/AuthenticationSelector.qml" line="62"/>
        <source>CRAM-MD5</source>
        <translation>CRAM-MD5</translation>
    </message>
</context>
<context>
    <name>AutoConfigState</name>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/setupwizard/states/AutoConfigState.qml" line="35"/>
        <source>Searching for configuration.</source>
        <translation>Bezig met zoeken naar instellingen…</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/setupwizard/states/AutoConfigState.qml" line="77"/>
        <source>IMAP server found</source>
        <translation>Imap-server gevonden</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/setupwizard/states/AutoConfigState.qml" line="78"/>
        <source>A IMAP server configuration was found for your domain.

Would you like to use this instead?</source>
        <translation>Er zijn imap-serverinstellingen aangetroffen voor dit domein.

Wil je die instellingen gebruiken?</translation>
    </message>
</context>
<context>
    <name>BottomEdgeComposer</name>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/composer/BottomEdgeComposer.qml" line="38"/>
        <source>New message</source>
        <translation>Nieuw bericht</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/composer/BottomEdgeComposer.qml" line="66"/>
        <source>Attachments</source>
        <translation>Bijlagen</translation>
    </message>
</context>
<context>
    <name>CacheSettings</name>
    <message>
        <location filename="../plugins/cache-manager-plugin/qml/CacheSettings.qml" line="7"/>
        <source>Manage cache</source>
        <translation>Cache beheren</translation>
    </message>
</context>
<context>
    <name>CacheSettingsPage</name>
    <message>
        <location filename="../plugins/cache-manager-plugin/qml/CacheSettingsPage.qml" line="26"/>
        <source>Manage cache: %1</source>
        <translation>Cache beheren: %1</translation>
    </message>
    <message>
        <location filename="../plugins/cache-manager-plugin/qml/CacheSettingsPage.qml" line="35"/>
        <source>One Week</source>
        <translation>Eén week</translation>
    </message>
    <message>
        <location filename="../plugins/cache-manager-plugin/qml/CacheSettingsPage.qml" line="36"/>
        <source>Fortnight</source>
        <translation>Twee weken</translation>
    </message>
    <message>
        <location filename="../plugins/cache-manager-plugin/qml/CacheSettingsPage.qml" line="37"/>
        <source>One Month</source>
        <translation>Eén maand</translation>
    </message>
    <message>
        <location filename="../plugins/cache-manager-plugin/qml/CacheSettingsPage.qml" line="38"/>
        <source>Three Months</source>
        <translation>Drie maanden</translation>
    </message>
    <message>
        <location filename="../plugins/cache-manager-plugin/qml/CacheSettingsPage.qml" line="39"/>
        <source>Six Months</source>
        <translation>Zes maanden</translation>
    </message>
    <message>
        <location filename="../plugins/cache-manager-plugin/qml/CacheSettingsPage.qml" line="45"/>
        <source>Clear messages from cache</source>
        <translation>Gecachete berichten wissen</translation>
    </message>
    <message>
        <location filename="../plugins/cache-manager-plugin/qml/CacheSettingsPage.qml" line="46"/>
        <source>This will clear messages older than the given period</source>
        <translation>Hierdoor worden berichten ouder dan de ingestelde periode gewist</translation>
    </message>
    <message>
        <location filename="../plugins/cache-manager-plugin/qml/CacheSettingsPage.qml" line="75"/>
        <source>Clear now</source>
        <translation>Nu wissen</translation>
    </message>
</context>
<context>
    <name>CleanMessagePage</name>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/messageview/CleanMessagePage.qml" line="29"/>
        <source>Message</source>
        <translation>Bericht</translation>
    </message>
</context>
<context>
    <name>ClientService</name>
    <message>
        <location filename="../Dekko/backend/mail/service/ClientService.cpp" line="78"/>
        <location filename="../Dekko/backend/mail/service/ClientService.cpp" line="82"/>
        <source>messages</source>
        <translation>berichten</translation>
    </message>
    <message>
        <location filename="../Dekko/backend/mail/service/ClientService.cpp" line="78"/>
        <location filename="../Dekko/backend/mail/service/ClientService.cpp" line="82"/>
        <source>message</source>
        <translation>bericht</translation>
    </message>
</context>
<context>
    <name>ComposeWindow</name>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/composer/ComposeWindow.qml" line="24"/>
        <source>Dekko Composer</source>
        <translation>Opstellen</translation>
    </message>
</context>
<context>
    <name>Composer</name>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/composer/Composer.qml" line="56"/>
        <source>Attach</source>
        <translation>Bijlage</translation>
    </message>
</context>
<context>
    <name>ComposerStore</name>
    <message>
        <location filename="../Dekko/stores/Composer/ComposerStore.qml" line="86"/>
        <source>Please select a sender. Otherwise no drafts will be saved automatically. Consider setting a default identity.</source>
        <translation>Kies een afzender om concepten automatisch te bewaren. Overweeg ook een standaardidentiteit in te stellen.</translation>
    </message>
    <message>
        <location filename="../Dekko/stores/Composer/ComposerStore.qml" line="90"/>
        <source>Somethings fishy with your Identity. Please select a valid sender.</source>
        <translation>Er is iets mis met je identiteit - kies een geldige afzender.</translation>
    </message>
    <message>
        <location filename="../Dekko/stores/Composer/ComposerStore.qml" line="99"/>
        <source>Message queued.</source>
        <translation>Het bericht staat in de wachtrij.</translation>
    </message>
    <message>
        <location filename="../Dekko/stores/Composer/ComposerStore.qml" line="103"/>
        <source>Draft saved.</source>
        <translation>Er is een concept bewaard.</translation>
    </message>
</context>
<context>
    <name>ComposerStoreActions</name>
    <message>
        <location filename="../Dekko/stores/Composer/ComposerStoreActions.qml" line="26"/>
        <source>Send</source>
        <translation>Versturen</translation>
    </message>
    <message>
        <location filename="../Dekko/stores/Composer/ComposerStoreActions.qml" line="33"/>
        <source>Save draft</source>
        <translation>Concept opslaan</translation>
    </message>
    <message>
        <location filename="../Dekko/stores/Composer/ComposerStoreActions.qml" line="40"/>
        <source>Discard</source>
        <translation>Niet opslaan</translation>
    </message>
    <message>
        <location filename="../Dekko/stores/Composer/ComposerStoreActions.qml" line="47"/>
        <source>Attach</source>
        <translation>Bijvoegen</translation>
    </message>
</context>
<context>
    <name>ComposerWorker</name>
    <message>
        <location filename="../Dekko/workers/ComposerWorker.qml" line="120"/>
        <source>Discard message</source>
        <translation>Bericht afbreken</translation>
    </message>
    <message>
        <location filename="../Dekko/workers/ComposerWorker.qml" line="120"/>
        <source>Are you sure you want to discard this message?</source>
        <translation>Weet je zeker dat je dit bericht wilt afbreken?</translation>
    </message>
</context>
<context>
    <name>ConfirmationDialog</name>
    <message>
        <location filename="../plugins/ubuntu-plugin/imports/dialogs/ConfirmationDialog.qml" line="51"/>
        <source>Cancel</source>
        <translation>Annuleren</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/imports/dialogs/ConfirmationDialog.qml" line="63"/>
        <source>Confirm</source>
        <translation>Bevestigen</translation>
    </message>
</context>
<context>
    <name>ContactFilterView</name>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/views/ContactFilterView.qml" line="113"/>
        <source>Add contact</source>
        <translation>Contactpersoon toevoegen</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/views/ContactFilterView.qml" line="125"/>
        <source>Send message</source>
        <translation>Bericht versturen</translation>
    </message>
</context>
<context>
    <name>ContactListPage</name>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/contacts/ContactListPage.qml" line="11"/>
        <source>Address book</source>
        <translation>Adresboek</translation>
    </message>
</context>
<context>
    <name>ContactView</name>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/contacts/ContactView.qml" line="11"/>
        <source>Contact</source>
        <translation>Contactpersoon</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/contacts/ContactView.qml" line="61"/>
        <source>Email</source>
        <translation>E-mailadres</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/contacts/ContactView.qml" line="78"/>
        <source>Phone</source>
        <translation>Telefoonnummer</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/contacts/ContactView.qml" line="92"/>
        <source>Address</source>
        <translation>Adres</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/contacts/ContactView.qml" line="96"/>
        <source>Street</source>
        <translation>Straat</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/contacts/ContactView.qml" line="102"/>
        <source>City</source>
        <translation>Plaatsnaam</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/contacts/ContactView.qml" line="108"/>
        <source>Zip</source>
        <translation>Postcode</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/contacts/ContactView.qml" line="114"/>
        <source>Country</source>
        <translation>Land</translation>
    </message>
</context>
<context>
    <name>ContactsListView</name>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/contacts/ContactsListView.qml" line="36"/>
        <source>Search</source>
        <translation>Zoeken</translation>
    </message>
</context>
<context>
    <name>ContentBlockedNotice</name>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/webview/ContentBlockedNotice.qml" line="46"/>
        <source>Remote content blocked</source>
        <translation>Externe inhoud geblokkeerd</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/webview/ContentBlockedNotice.qml" line="62"/>
        <source>Allow</source>
        <translation>Toestaan</translation>
    </message>
</context>
<context>
    <name>ContributorsPage</name>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/views/ContributorsPage.qml" line="25"/>
        <source>Contributors</source>
        <translation>Bijdragers</translation>
    </message>
</context>
<context>
    <name>CopyFoldersGroup</name>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/settings/CopyFoldersGroup.qml" line="31"/>
        <source>Copies and Folders: %1</source>
        <translation>Kopieën en mappen: %1</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/settings/CopyFoldersGroup.qml" line="120"/>
        <source>Detect standard folders</source>
        <translation>Standaardmappen ophalen</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/settings/CopyFoldersGroup.qml" line="127"/>
        <source>Detect</source>
        <translation>Ophalen</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/settings/CopyFoldersGroup.qml" line="147"/>
        <source>Base folder</source>
        <translation>Basismap</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/settings/CopyFoldersGroup.qml" line="149"/>
        <source>Leave empty if you are unsure</source>
        <translation>Bij twijfel: leeglaten</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/settings/CopyFoldersGroup.qml" line="164"/>
        <source>Inbox folder</source>
        <translation>Postvak in-map</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/settings/CopyFoldersGroup.qml" line="180"/>
        <source>Drafts folder</source>
        <translation>Conceptenmap</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/settings/CopyFoldersGroup.qml" line="196"/>
        <source>Spam folder</source>
        <translation>Ongewenste e-mailmap</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/settings/CopyFoldersGroup.qml" line="212"/>
        <source>Sent folder</source>
        <translation>Verzondenmap</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/settings/CopyFoldersGroup.qml" line="228"/>
        <source>Outbox folder</source>
        <translation>Postvak uit-map</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/settings/CopyFoldersGroup.qml" line="244"/>
        <source>Trash folder</source>
        <translation>Prullenbakmap</translation>
    </message>
</context>
<context>
    <name>DefaultMessagePage</name>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/messageview/DefaultMessagePage.qml" line="183"/>
        <source>From:</source>
        <translation>Van:</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/messageview/DefaultMessagePage.qml" line="236"/>
        <source>To:</source>
        <translation>Aan:</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/messageview/DefaultMessagePage.qml" line="241"/>
        <source>Cc:</source>
        <translation>Cc:</translation>
    </message>
</context>
<context>
    <name>DefaultPlugin</name>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/extensions/addressbook/DefaultPlugin.qml" line="11"/>
        <source>Internal</source>
        <translation>Intern</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/extensions/addressbook/DefaultPlugin.qml" line="106"/>
        <source>Default</source>
        <translation>Standaard</translation>
    </message>
</context>
<context>
    <name>DekkoHeader</name>
    <message>
        <location filename="../plugins/ubuntu-plugin/imports/components/DekkoHeader.qml" line="206"/>
        <source>Enter search...</source>
        <translation>Voer een zoekopdracht in…</translation>
    </message>
</context>
<context>
    <name>DekkoWebView</name>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/webview/DekkoWebView.qml" line="303"/>
        <source>Open in browser?</source>
        <translation>Webbrowserverzoek</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/webview/DekkoWebView.qml" line="304"/>
        <source>Confirm to open %1 in web browser</source>
        <translation>Wil je %1 openen in de webbrowser?</translation>
    </message>
</context>
<context>
    <name>DetailList</name>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/messageview/DetailList.qml" line="48"/>
        <source>Details</source>
        <translation>Details</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/messageview/DetailList.qml" line="53"/>
        <source>To:</source>
        <translation>Aan:</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/messageview/DetailList.qml" line="59"/>
        <source>Cc:</source>
        <translation>Cc:</translation>
    </message>
</context>
<context>
    <name>DetailsGroup</name>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/settings/DetailsGroup.qml" line="27"/>
        <source>Details: %1</source>
        <translation>Details: %1</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/settings/DetailsGroup.qml" line="48"/>
        <source>Account name</source>
        <translation>Accountnaam</translation>
    </message>
</context>
<context>
    <name>DialogWorker</name>
    <message>
        <location filename="../Dekko/workers/DialogWorker.qml" line="39"/>
        <source>Error</source>
        <translation>Foutmelding</translation>
    </message>
    <message>
        <location filename="../Dekko/workers/DialogWorker.qml" line="48"/>
        <source>Notice</source>
        <translation>Let op</translation>
    </message>
</context>
<context>
    <name>DisplaySettings</name>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/settings/DisplaySettings.qml" line="28"/>
        <source>Navigation menu</source>
        <translation>Navigatiemenu</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/settings/DisplaySettings.qml" line="32"/>
        <source>Show smart folders</source>
        <translation>Slimme mappen tonen</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/settings/DisplaySettings.qml" line="42"/>
        <source>Show favourite folders</source>
        <translation>Favoriete mappen tonen</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/settings/DisplaySettings.qml" line="53"/>
        <source>Messages</source>
        <translation>Berichten</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/settings/DisplaySettings.qml" line="57"/>
        <source>Show avatars</source>
        <translation>Gebruikersafbeeldingen tonen</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/settings/DisplaySettings.qml" line="67"/>
        <source>Prefer plain text</source>
        <translation>Plattetekstopmaak (indien mogelijk)</translation>
    </message>
</context>
<context>
    <name>DisplaySettingsPage</name>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/settings/DisplaySettingsPage.qml" line="21"/>
        <source>Display Settings</source>
        <translation>Weergave-instellingen</translation>
    </message>
</context>
<context>
    <name>DisplaySettingsPopup</name>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/settings/DisplaySettingsPopup.qml" line="21"/>
        <source>Display Settings</source>
        <translation>Weergave-instellingen</translation>
    </message>
</context>
<context>
    <name>EncryptionSelector</name>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/components/EncryptionSelector.qml" line="46"/>
        <source>Encryption</source>
        <translation>Versleuteling</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/components/EncryptionSelector.qml" line="61"/>
        <source>No encryption</source>
        <translation>Geen versleuteling</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/components/EncryptionSelector.qml" line="62"/>
        <source>Use encryption (STARTTLS)</source>
        <translation>Versleuteling gebruiken (STARTTLS)</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/components/EncryptionSelector.qml" line="63"/>
        <source>Force encryption (SSL/TLS)</source>
        <translation>Versleuteling afdwingen (SSL/TLS)</translation>
    </message>
</context>
<context>
    <name>ErrorsWorker</name>
    <message>
        <location filename="../Dekko/workers/ErrorsWorker.qml" line="78"/>
        <location filename="../Dekko/workers/ErrorsWorker.qml" line="82"/>
        <source>No connection</source>
        <translation>Geen internetverbinding</translation>
    </message>
    <message>
        <location filename="../Dekko/workers/ErrorsWorker.qml" line="86"/>
        <source>Connection unavailable</source>
        <translation>Geen internetverbinding</translation>
    </message>
    <message>
        <location filename="../Dekko/workers/ErrorsWorker.qml" line="102"/>
        <source>Action timed out</source>
        <translation>Verbinding verlopen</translation>
    </message>
</context>
<context>
    <name>ExpandablePanel</name>
    <message>
        <location filename="../plugins/ubuntu-plugin/imports/components/ExpandablePanel.qml" line="63"/>
        <source>Attachments</source>
        <translation>Bijlagen</translation>
    </message>
</context>
<context>
    <name>ExportUpdatesAction</name>
    <message>
        <location filename="../Dekko/backend/mail/service/ClientServiceAction.cpp" line="144"/>
        <source>Syncing changes for %1 account</source>
        <translation>Bezig met synchroniseren van %1-account…</translation>
    </message>
</context>
<context>
    <name>FilePickerDialog</name>
    <message>
        <location filename="../plugins/ubuntu-plugin/imports/dialogs/FilePickerDialog.qml" line="23"/>
        <source>Add Attachment</source>
        <translation>Bijlage toevoegen</translation>
    </message>
</context>
<context>
    <name>FlagsAction</name>
    <message>
        <location filename="../Dekko/backend/mail/service/ClientServiceAction.cpp" line="166"/>
        <source>important</source>
        <translation>belangrijk</translation>
    </message>
    <message>
        <location filename="../Dekko/backend/mail/service/ClientServiceAction.cpp" line="169"/>
        <source>not important</source>
        <translation>onbelangrijk</translation>
    </message>
    <message>
        <location filename="../Dekko/backend/mail/service/ClientServiceAction.cpp" line="178"/>
        <source>read</source>
        <translation>gelezen</translation>
    </message>
    <message>
        <location filename="../Dekko/backend/mail/service/ClientServiceAction.cpp" line="181"/>
        <source>unread</source>
        <translation>ongelezen</translation>
    </message>
    <message>
        <location filename="../Dekko/backend/mail/service/ClientServiceAction.cpp" line="190"/>
        <source>as todo</source>
        <translation>als taak</translation>
    </message>
    <message>
        <location filename="../Dekko/backend/mail/service/ClientServiceAction.cpp" line="193"/>
        <source>no longer todo</source>
        <translation>taak afgerond</translation>
    </message>
    <message>
        <location filename="../Dekko/backend/mail/service/ClientServiceAction.cpp" line="198"/>
        <source>Marking %1 messages %2</source>
        <translation>Bezig met markeren van %1 berichten als %2…</translation>
    </message>
</context>
<context>
    <name>Folder</name>
    <message>
        <location filename="../Dekko/backend/mail/Folder.cpp" line="103"/>
        <source>Drafts</source>
        <translation>Concepten</translation>
    </message>
    <message>
        <location filename="../Dekko/backend/mail/Folder.cpp" line="105"/>
        <source>Inbox</source>
        <translation>Postvak in</translation>
    </message>
    <message>
        <location filename="../Dekko/backend/mail/Folder.cpp" line="107"/>
        <source>Spam</source>
        <translation>Ongewenst</translation>
    </message>
    <message>
        <location filename="../Dekko/backend/mail/Folder.cpp" line="109"/>
        <source>Outbox</source>
        <translation>Postvak uit</translation>
    </message>
    <message>
        <location filename="../Dekko/backend/mail/Folder.cpp" line="111"/>
        <source>Sent</source>
        <translation>Verzonden</translation>
    </message>
    <message>
        <location filename="../Dekko/backend/mail/Folder.cpp" line="113"/>
        <source>Trash</source>
        <translation>Prullenbak</translation>
    </message>
</context>
<context>
    <name>FolderListDelegate</name>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/delegates/FolderListDelegate.qml" line="53"/>
        <source>Un-favourite</source>
        <translation>Verwijderen uit favorieten</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/delegates/FolderListDelegate.qml" line="53"/>
        <source>Favourite</source>
        <translation>Toevoegen aan favorieten</translation>
    </message>
</context>
<context>
    <name>FolderListView</name>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/views/FolderListView.qml" line="29"/>
        <source>Folders</source>
        <translation>Mappen</translation>
    </message>
</context>
<context>
    <name>ForbiddenReply</name>
    <message>
        <location filename="../Dekko/backend/network/ForbiddenReply.cpp" line="24"/>
        <source>Remote content is banned</source>
        <translation>Externe inhoud is geblokkeerd</translation>
    </message>
</context>
<context>
    <name>HtmlViewer</name>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/extensions/html-viewer/HtmlViewer.qml" line="11"/>
        <source>HTML Viewer</source>
        <translation>Html-weergave</translation>
    </message>
</context>
<context>
    <name>IdentitiesListPage</name>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/settings/IdentitiesListPage.qml" line="15"/>
        <source>Identities</source>
        <translation>Identiteiten</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/settings/IdentitiesListPage.qml" line="107"/>
        <source> (Default)</source>
        <translation> (standaard)</translation>
    </message>
</context>
<context>
    <name>IdentityInput</name>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/settings/IdentityInput.qml" line="55"/>
        <source>Default identity</source>
        <translation>Standaardidentiteit</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/settings/IdentityInput.qml" line="68"/>
        <source>Account</source>
        <translation>Account</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/settings/IdentityInput.qml" line="106"/>
        <source>Name</source>
        <translation>Naam</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/settings/IdentityInput.qml" line="111"/>
        <source>Email Address</source>
        <translation>E-mailadres</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/settings/IdentityInput.qml" line="116"/>
        <source>Reply-To</source>
        <translation>Beantwoorden</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/settings/IdentityInput.qml" line="120"/>
        <source>Signature</source>
        <translation>Handtekening</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/settings/IdentityInput.qml" line="138"/>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/settings/IdentityInput.qml" line="146"/>
        <source>New identity</source>
        <translation>Nieuwe identiteit</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/settings/IdentityInput.qml" line="170"/>
        <source>Edit identity</source>
        <translation>Identiteit bewerken</translation>
    </message>
</context>
<context>
    <name>IdleProtocol</name>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapclient.cpp" line="566"/>
        <source>Idle Error occurred</source>
        <translation>Afwezigheidsfout</translation>
    </message>
</context>
<context>
    <name>ImagePickerScript</name>
    <message>
        <location filename="../upstream/quick-flux/quickflux/examples/photoalbum/scripts/ImagePickerScript.qml" line="12"/>
        <source>Pick Image</source>
        <translation>Afbeelding kiezen</translation>
    </message>
</context>
<context>
    <name>ImagePreview</name>
    <message>
        <location filename="../upstream/quick-flux/quickflux/examples/photoalbum/views/ImagePreview.qml" line="31"/>
        <source>Cancel</source>
        <translation>Annuleren</translation>
    </message>
    <message>
        <location filename="../upstream/quick-flux/quickflux/examples/photoalbum/views/ImagePreview.qml" line="39"/>
        <source>Confirm</source>
        <translation>Bevestigen</translation>
    </message>
</context>
<context>
    <name>ImageViewer</name>
    <message>
        <location filename="../upstream/quick-flux/quickflux/examples/photoalbum/views/ImageViewer.qml" line="35"/>
        <source>Pick Image</source>
        <translation>Afbeelding kiezen</translation>
    </message>
</context>
<context>
    <name>ImapClient</name>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapclient.cpp" line="740"/>
        <source>Cannot open connection without IMAP server configuration</source>
        <translation>Er kan geen verbinding worden opgesteld zonder imap-serverinstellingen</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapclient.cpp" line="868"/>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapclient.cpp" line="959"/>
        <source>Checking capabilities</source>
        <translation>Bezig met controleren van mogelijkheden…</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapclient.cpp" line="886"/>
        <source>Starting TLS</source>
        <translation>Bezig met starten van tls…</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapclient.cpp" line="900"/>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapclient.cpp" line="998"/>
        <source>Logging in idle connection</source>
        <translation>Bezig met inloggen op idle-verbinding…</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapclient.cpp" line="918"/>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapclient.cpp" line="934"/>
        <source>Logging in</source>
        <translation>Bezig met inloggen…</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapclient.cpp" line="1611"/>
        <source>Unable to read fetched data</source>
        <translation>De opgehaalde gegevens kunnen niet worden uitgelezen</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapclient.cpp" line="1618"/>
        <source>Unable to map fetched data</source>
        <translation>De opgehaalde gegevens kunnen niet worden toegewezen</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapclient.cpp" line="1625"/>
        <source>Unable to update part body</source>
        <translation>De berichtinhoud kan niet worden bijgewerkt</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapclient.cpp" line="1648"/>
        <source>Unable to handle dataFetched without context</source>
        <translation>De opgehaalde gegevens kunnen niet worden verwerkt zonder context</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapclient.cpp" line="1685"/>
        <source>Cannot send message; socket in use</source>
        <translation>Het bericht kan niet worden verstuurd - de socket is in gebruik</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapclient.cpp" line="1785"/>
        <source>Logging out</source>
        <translation>Bezig met uitloggen…</translation>
    </message>
</context>
<context>
    <name>ImapProtocol</name>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapprotocol.cpp" line="3346"/>
        <source>Connection failed</source>
        <translation>De verbinding is mislukt</translation>
    </message>
</context>
<context>
    <name>ImapService</name>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapservice.cpp" line="1665"/>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapservice.cpp" line="1740"/>
        <source>Account disabled</source>
        <translation>Dit account is uitgeschakeld</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapservice.cpp" line="1754"/>
        <source>Initiating push email</source>
        <translation>Bezig met opzetten van pushmail…</translation>
    </message>
</context>
<context>
    <name>ImapService::Source</name>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapservice.cpp" line="238"/>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapservice.cpp" line="313"/>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapservice.cpp" line="358"/>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapservice.cpp" line="409"/>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapservice.cpp" line="449"/>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapservice.cpp" line="493"/>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapservice.cpp" line="538"/>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapservice.cpp" line="592"/>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapservice.cpp" line="614"/>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapservice.cpp" line="704"/>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapservice.cpp" line="732"/>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapservice.cpp" line="766"/>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapservice.cpp" line="835"/>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapservice.cpp" line="1033"/>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapservice.cpp" line="1060"/>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapservice.cpp" line="1123"/>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapservice.cpp" line="1151"/>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapservice.cpp" line="1199"/>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapservice.cpp" line="1222"/>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapservice.cpp" line="1240"/>
        <source>Account disabled</source>
        <translation>Account is uitgeschakeld</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapservice.cpp" line="243"/>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapservice.cpp" line="318"/>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapservice.cpp" line="543"/>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapservice.cpp" line="597"/>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapservice.cpp" line="619"/>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapservice.cpp" line="1038"/>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapservice.cpp" line="1065"/>
        <source>No account specified</source>
        <translation>Er is geen account gekozen</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapservice.cpp" line="363"/>
        <source>No messages to retrieve</source>
        <translation>Er zijn geen berichten om op te halen</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapservice.cpp" line="414"/>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapservice.cpp" line="454"/>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapservice.cpp" line="498"/>
        <source>No message to retrieve</source>
        <translation>Er is geen bericht om op te halen</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapservice.cpp" line="418"/>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapservice.cpp" line="502"/>
        <source>No part specified</source>
        <translation>En is geen onderdeel opgegeven</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapservice.cpp" line="422"/>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapservice.cpp" line="458"/>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapservice.cpp" line="506"/>
        <source>Invalid message specified</source>
        <translation>Er is een ongeldig bericht opgegeven</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapservice.cpp" line="463"/>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapservice.cpp" line="510"/>
        <source>No minimum specified</source>
        <translation>Er is geen minimumaantal opgegeven</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapservice.cpp" line="660"/>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapservice.cpp" line="689"/>
        <source>Could not delete messages</source>
        <translation>De berichten kunnen niet worden verwijderd</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapservice.cpp" line="737"/>
        <source>No messages to copy</source>
        <translation>Er zijn geen te kopiëren berichten</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapservice.cpp" line="741"/>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapservice.cpp" line="775"/>
        <source>Invalid destination folder</source>
        <translation>Ongeldige bestemmingsmap</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapservice.cpp" line="771"/>
        <source>No messages to move</source>
        <translation>Er zijn geen berichten om te verplaatsen</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapservice.cpp" line="840"/>
        <source>No messages to flag</source>
        <translation>Er zijn geen berichten om te markeren</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapservice.cpp" line="844"/>
        <source>No flags to be applied</source>
        <translation>Er zijn geen markeringen toe te passen</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapservice.cpp" line="1044"/>
        <source>Cannot create empty named folder</source>
        <translation>Naamloze mappen kunnen niet worden aangemaakt</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapservice.cpp" line="1082"/>
        <source>Drafts</source>
        <translation>Concepten</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapservice.cpp" line="1085"/>
        <source>Sent</source>
        <translation>Verzonden</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapservice.cpp" line="1088"/>
        <source>Junk</source>
        <translation>Ongewenst</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapservice.cpp" line="1091"/>
        <source>Trash</source>
        <translation>Prullenbak</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapservice.cpp" line="1128"/>
        <source>Deleting invalid folder</source>
        <translation>Bezig met verwijderen van ongeldige map…</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapservice.cpp" line="1156"/>
        <source>Cannot rename to an empty folder</source>
        <translation>Lege mappen kunnen niet van naam worden gewijzigd</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapservice.cpp" line="1160"/>
        <source>Cannot rename an invalid folder</source>
        <translation>Ongeldige mappen kunnen niet van naam worden gewijzigd</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapservice.cpp" line="1205"/>
        <source>Empty search provided</source>
        <translation>Er is geen zoekopdracht ingevoerd</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapservice.cpp" line="1245"/>
        <source>No messages to prepare</source>
        <translation>Er zijn geen berichten om voor te bereiden</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapservice.cpp" line="1337"/>
        <source>Destination message failed to match source message</source>
        <translation>Het bestemmingsbericht komt niet overeen met het bronbericht</translation>
    </message>
</context>
<context>
    <name>ImapSettings</name>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapsettings.ui" line="14"/>
        <source>Form</source>
        <translation>Formulier</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapsettings.ui" line="59"/>
        <source>Username</source>
        <translation>Gebruikersnaam</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapsettings.ui" line="85"/>
        <source>Password</source>
        <translation>Wachtwoord</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapsettings.ui" line="114"/>
        <source>Server</source>
        <translation>Server</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapsettings.ui" line="140"/>
        <source>Port</source>
        <translation>Poort</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapsettings.ui" line="166"/>
        <source>Encryption</source>
        <translation>Versleuteling</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapsettings.ui" line="183"/>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapsettings.ui" line="224"/>
        <source>None</source>
        <translation>Geen</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapsettings.ui" line="188"/>
        <source>SSL</source>
        <translation>Ssl</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapsettings.ui" line="193"/>
        <source>TLS</source>
        <translation>Tls</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapsettings.ui" line="207"/>
        <source>Authentication</source>
        <translation>Verificatie</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapsettings.ui" line="229"/>
        <source>Login</source>
        <translation>Inloggen</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapsettings.ui" line="234"/>
        <source>Plain</source>
        <translation>Platte tekst</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapsettings.ui" line="239"/>
        <source>Cram MD5</source>
        <translation>Cram md5</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapsettings.ui" line="253"/>
        <source>Remove deleted messages from server</source>
        <translation>Verwijderde berichten ook van server verwijderen</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapsettings.ui" line="275"/>
        <source>Download limit</source>
        <translation>Downloadbeperking</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapsettings.ui" line="294"/>
        <source>K</source>
        <translation>K</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapsettings.ui" line="313"/>
        <source>Prefer HTML content over Plain</source>
        <translation>Html-inhoud tonen</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapsettings.ui" line="332"/>
        <source>Interval</source>
        <translation>Controleren, elke</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapsettings.ui" line="348"/>
        <source>min</source>
        <comment>short for minutes</comment>
        <translation>min.</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapsettings.ui" line="382"/>
        <source>Disable when Roaming</source>
        <translation>Uitschakelen tijdens roamen</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapsettings.ui" line="395"/>
        <source>Base folder</source>
        <translation>Basismap</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapsettings.ui" line="445"/>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapsettings.ui" line="513"/>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapsettings.ui" line="581"/>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapsettings.ui" line="649"/>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapsettings.ui" line="717"/>
        <source>...</source>
        <translation>…</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapsettings.ui" line="460"/>
        <source>Drafts folder</source>
        <translation>Conceptenmap</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapsettings.ui" line="528"/>
        <source>Sent folder</source>
        <translation>Verzondenmap</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapsettings.ui" line="596"/>
        <source>Trash folder</source>
        <translation>Prullenbakmap</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapsettings.ui" line="664"/>
        <source>Junk folder</source>
        <translation>Ongewenstmap</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapsettings.ui" line="738"/>
        <source>Push Enabled</source>
        <translation>Push inschakelen</translation>
    </message>
</context>
<context>
    <name>IncomingServerGroup</name>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/settings/IncomingServerGroup.qml" line="29"/>
        <source>Incoming Server: %1</source>
        <translation>Inkomende server: %1</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/settings/IncomingServerGroup.qml" line="106"/>
        <source>Hostname</source>
        <translation>Hostnaam</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/settings/IncomingServerGroup.qml" line="115"/>
        <source>Port</source>
        <translation>Poort</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/settings/IncomingServerGroup.qml" line="124"/>
        <source>Username</source>
        <translation>Gebruikersnaam</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/settings/IncomingServerGroup.qml" line="133"/>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/settings/IncomingServerGroup.qml" line="136"/>
        <source>Password</source>
        <translation>Wachtwoord</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/settings/IncomingServerGroup.qml" line="143"/>
        <source>Show password</source>
        <translation>Wachtwoord tonen</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/settings/IncomingServerGroup.qml" line="149"/>
        <source>Security settings</source>
        <translation>Beveiligingsinstellingen</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/settings/IncomingServerGroup.qml" line="184"/>
        <source>Allow untrusted certificates</source>
        <translation>Onvertrouwde certificaten toestaan</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/settings/IncomingServerGroup.qml" line="189"/>
        <source>Server settings</source>
        <translation>Serverinstellingen</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/settings/IncomingServerGroup.qml" line="194"/>
        <source>Check for new mail on start</source>
        <translation>Nieuwe berichten ophalen na opstarten</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/settings/IncomingServerGroup.qml" line="200"/>
        <source>Enable IMAP IDLE</source>
        <translation>IMAP-IDLE inschakelen</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/settings/IncomingServerGroup.qml" line="207"/>
        <source>Check interval (minutes)</source>
        <translation>Controleren, elke (in minuten)</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/settings/IncomingServerGroup.qml" line="214"/>
        <source>Check when roaming</source>
        <translation>Controleren tijdens roamen</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/settings/IncomingServerGroup.qml" line="220"/>
        <source>Maximum mail size (MB)</source>
        <translation>Maximale berichtgrootte (in mb)</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/settings/IncomingServerGroup.qml" line="226"/>
        <source>No maximum mail size</source>
        <translation>Geen maximale berichtgrootte</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/settings/IncomingServerGroup.qml" line="234"/>
        <source>Automatically download attachments</source>
        <translation>Bijlagen automatisch ophalen</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/settings/IncomingServerGroup.qml" line="240"/>
        <source>Allowed to delete mail</source>
        <translation>Berichten verwijderen toestaan</translation>
    </message>
</context>
<context>
    <name>LicensesPage</name>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/views/LicensesPage.qml" line="25"/>
        <source>Licenses</source>
        <translation>Licenties</translation>
    </message>
</context>
<context>
    <name>MailSettings</name>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/settings/MailSettings.qml" line="11"/>
        <source>Mail Settings</source>
        <translation>E-mailinstellingen</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/settings/MailSettings.qml" line="24"/>
        <source>Accounts</source>
        <translation>Accounts</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/settings/MailSettings.qml" line="34"/>
        <source>Identities</source>
        <translation>Identiteiten</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/settings/MailSettings.qml" line="44"/>
        <source>Display</source>
        <translation>Weergave</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/settings/MailSettings.qml" line="51"/>
        <source>Privacy</source>
        <translation>Privacy</translation>
    </message>
</context>
<context>
    <name>MailSettingsAction</name>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/settings/MailSettingsAction.qml" line="7"/>
        <source>Mail</source>
        <translation>E-mail</translation>
    </message>
</context>
<context>
    <name>MailStore</name>
    <message>
        <location filename="../Dekko/stores/Mail/MailStore.qml" line="38"/>
        <source>All</source>
        <translation>Alles</translation>
    </message>
    <message>
        <location filename="../Dekko/stores/Mail/MailStore.qml" line="38"/>
        <source>Unread</source>
        <translation>Ongelezen</translation>
    </message>
    <message>
        <location filename="../Dekko/stores/Mail/MailStore.qml" line="38"/>
        <source>Starred</source>
        <translation>Favorieten</translation>
    </message>
    <message>
        <location filename="../Dekko/stores/Mail/MailStore.qml" line="38"/>
        <source>Replied</source>
        <translation>Beantwoord</translation>
    </message>
    <message>
        <location filename="../Dekko/stores/Mail/MailStore.qml" line="38"/>
        <source>Forwarded</source>
        <translation>Doorgestuurd</translation>
    </message>
    <message>
        <location filename="../Dekko/stores/Mail/MailStore.qml" line="38"/>
        <source>Attachments</source>
        <translation>Bijlagen</translation>
    </message>
    <message>
        <location filename="../Dekko/stores/Mail/MailStore.qml" line="38"/>
        <source>Calendar</source>
        <translation>Agenda</translation>
    </message>
    <message>
        <location filename="../Dekko/stores/Mail/MailStore.qml" line="39"/>
        <source>Local</source>
        <translation>Lokaal</translation>
    </message>
</context>
<context>
    <name>MailStoreActions</name>
    <message>
        <location filename="../Dekko/stores/Mail/MailStoreActions.qml" line="36"/>
        <source>Unselect all</source>
        <translation>Niets selecteren</translation>
    </message>
    <message>
        <location filename="../Dekko/stores/Mail/MailStoreActions.qml" line="36"/>
        <source>Select all</source>
        <translation>Alles selecteren</translation>
    </message>
    <message>
        <location filename="../Dekko/stores/Mail/MailStoreActions.qml" line="49"/>
        <source>Star</source>
        <translation>Toev. aan favorieten</translation>
    </message>
    <message>
        <location filename="../Dekko/stores/Mail/MailStoreActions.qml" line="49"/>
        <source>Remove star</source>
        <translation>Verw. uit favorieten</translation>
    </message>
    <message>
        <location filename="../Dekko/stores/Mail/MailStoreActions.qml" line="57"/>
        <source>Delete</source>
        <translation>Verwijderen</translation>
    </message>
    <message>
        <location filename="../Dekko/stores/Mail/MailStoreActions.qml" line="62"/>
        <source>Mark as un-read</source>
        <translation>Markeren als ongelezen</translation>
    </message>
    <message>
        <location filename="../Dekko/stores/Mail/MailStoreActions.qml" line="62"/>
        <source>Mark as read</source>
        <translation>Markeren als gelezen</translation>
    </message>
</context>
<context>
    <name>MailUtils</name>
    <message>
        <location filename="../plugins/ubuntu-plugin/imports/constants/MailUtils.qml" line="27"/>
        <source>To</source>
        <translation>Aan</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/imports/constants/MailUtils.qml" line="29"/>
        <source>Cc</source>
        <translation>Cc</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/imports/constants/MailUtils.qml" line="31"/>
        <source>Bcc</source>
        <translation>Bcc</translation>
    </message>
</context>
<context>
    <name>MailboxPickerPage</name>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/views/MailboxPickerPage.qml" line="34"/>
        <source>Select folder</source>
        <translation>Kies een map</translation>
    </message>
</context>
<context>
    <name>MailboxWorker</name>
    <message>
        <location filename="../Dekko/workers/MailboxWorker.qml" line="161"/>
        <source>Message moved</source>
        <translation>Het bericht is verplaatst</translation>
    </message>
</context>
<context>
    <name>MainUI</name>
    <message>
        <location filename="../plugins/ubuntu-plugin/qml/MainUI.qml" line="17"/>
        <source>Dekko Mail</source>
        <translation>Dekko Mail</translation>
    </message>
</context>
<context>
    <name>ManageAccountsPage</name>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/settings/ManageAccountsPage.qml" line="26"/>
        <source>Manage accounts</source>
        <translation>Accounts beheren</translation>
    </message>
</context>
<context>
    <name>ManualInputUI</name>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/setupwizard/components/ManualInputUI.qml" line="28"/>
        <source>Server configuration</source>
        <translation>Serverinstellingen</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/setupwizard/components/ManualInputUI.qml" line="33"/>
        <source>IMAP Server:</source>
        <translation>Imap-server:</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/setupwizard/components/ManualInputUI.qml" line="46"/>
        <source>POP3 Server:</source>
        <translation>Pop3-server:</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/setupwizard/components/ManualInputUI.qml" line="59"/>
        <source>SMTP Server:</source>
        <translation>Smtp-server:</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/setupwizard/components/ManualInputUI.qml" line="71"/>
        <source>Back</source>
        <translation>Vorige</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/setupwizard/components/ManualInputUI.qml" line="75"/>
        <source>Next</source>
        <translation>Volgende</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/setupwizard/components/ManualInputUI.qml" line="135"/>
        <source>Password empty</source>
        <translation>Er is geen wachtwoord opgegeven</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/setupwizard/components/ManualInputUI.qml" line="135"/>
        <source>Would you like to continue?</source>
        <translation>Wil je doorgaan?</translation>
    </message>
</context>
<context>
    <name>MarkdownEditor</name>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/extensions/Markdown/MarkdownEditor.qml" line="67"/>
        <source>Preview</source>
        <translation>Voorvertoning</translation>
    </message>
</context>
<context>
    <name>Message</name>
    <message>
        <location filename="../Dekko/backend/mail/Message.cpp" line="228"/>
        <source>you</source>
        <translation>ik</translation>
    </message>
    <message>
        <location filename="../Dekko/backend/mail/Message.cpp" line="230"/>
        <source>to %1</source>
        <translation>aan %1</translation>
    </message>
</context>
<context>
    <name>MessageActionPopover</name>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/popovers/MessageActionPopover.qml" line="44"/>
        <source>Reply all</source>
        <translation>Allen beantwoorden</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/popovers/MessageActionPopover.qml" line="52"/>
        <source>Forward</source>
        <translation>Doorsturen</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/popovers/MessageActionPopover.qml" line="65"/>
        <source>Move</source>
        <translation>Verplaatsen</translation>
    </message>
</context>
<context>
    <name>MessageBuilder</name>
    <message>
        <location filename="../Dekko/backend/mail/MessageBuilder.cpp" line="268"/>
        <source>On %1, %2 wrote:
</source>
        <translation>Op %1 schreef %2:
</translation>
    </message>
    <message>
        <location filename="../Dekko/backend/mail/MessageBuilder.cpp" line="325"/>
        <source>Forwarded Message</source>
        <translation>Doorgestuurd bericht</translation>
    </message>
    <message>
        <location filename="../Dekko/backend/mail/MessageBuilder.cpp" line="326"/>
        <source>Date: </source>
        <translation>Datum: </translation>
    </message>
    <message>
        <location filename="../Dekko/backend/mail/MessageBuilder.cpp" line="327"/>
        <source>From: </source>
        <translation>Van: </translation>
    </message>
    <message>
        <location filename="../Dekko/backend/mail/MessageBuilder.cpp" line="328"/>
        <source>To: </source>
        <translation>Aan: </translation>
    </message>
    <message>
        <location filename="../Dekko/backend/mail/MessageBuilder.cpp" line="329"/>
        <source>Subject: </source>
        <translation>Onderwerp: </translation>
    </message>
</context>
<context>
    <name>MessageFilterCollection</name>
    <message>
        <location filename="../Dekko/backend/mail/MessageFilterCollection.cpp" line="79"/>
        <source>All inboxes</source>
        <translation>Alle postvakken</translation>
    </message>
    <message>
        <location filename="../Dekko/backend/mail/MessageFilterCollection.cpp" line="89"/>
        <source>Drafts</source>
        <translation>Concepten</translation>
    </message>
    <message>
        <location filename="../Dekko/backend/mail/MessageFilterCollection.cpp" line="99"/>
        <source>Spam</source>
        <translation>Ongewenst</translation>
    </message>
    <message>
        <location filename="../Dekko/backend/mail/MessageFilterCollection.cpp" line="109"/>
        <source>Outbox</source>
        <translation>Postvak uit</translation>
    </message>
    <message>
        <location filename="../Dekko/backend/mail/MessageFilterCollection.cpp" line="119"/>
        <source>Sent</source>
        <translation>Verzonden</translation>
    </message>
    <message>
        <location filename="../Dekko/backend/mail/MessageFilterCollection.cpp" line="128"/>
        <source>Trash</source>
        <translation>Prullenbak</translation>
    </message>
    <message>
        <location filename="../Dekko/backend/mail/MessageFilterCollection.cpp" line="140"/>
        <source>Today, %1</source>
        <translation>Vandaag (%1)</translation>
    </message>
    <message>
        <location filename="../Dekko/backend/mail/MessageFilterCollection.cpp" line="149"/>
        <source>To-do</source>
        <translation>Taak</translation>
    </message>
    <message>
        <location filename="../Dekko/backend/mail/MessageFilterCollection.cpp" line="158"/>
        <source>Done</source>
        <translation>Afgerond</translation>
    </message>
</context>
<context>
    <name>MessageHeader</name>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/messageview/MessageHeader.qml" line="47"/>
        <source>Hide details</source>
        <translation>Details verbergen</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/messageview/MessageHeader.qml" line="47"/>
        <source>View details</source>
        <translation>Details tonen</translation>
    </message>
</context>
<context>
    <name>MessageListActionPopover</name>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/popovers/MessageListActionPopover.qml" line="45"/>
        <source>Mark as unread</source>
        <translation>Markeren als ongelezen</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/popovers/MessageListActionPopover.qml" line="45"/>
        <source>Mark as read</source>
        <translation>Markeren als gelezen</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/popovers/MessageListActionPopover.qml" line="53"/>
        <source>Mark as not important</source>
        <translation>Markeren als onbelangrijk</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/popovers/MessageListActionPopover.qml" line="53"/>
        <source>Mark as important</source>
        <translation>Markeren als belangrijk</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/popovers/MessageListActionPopover.qml" line="61"/>
        <source>Mark as spam</source>
        <translation>Markeren als ongewenst</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/popovers/MessageListActionPopover.qml" line="74"/>
        <source>To-do</source>
        <translation>Taak</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/popovers/MessageListActionPopover.qml" line="91"/>
        <source>Done</source>
        <translation>Voltooid</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/popovers/MessageListActionPopover.qml" line="105"/>
        <source>Reply</source>
        <translation>Beantwoorden</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/popovers/MessageListActionPopover.qml" line="113"/>
        <source>Reply all</source>
        <translation>Allen beantwoorden</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/popovers/MessageListActionPopover.qml" line="121"/>
        <source>Forward</source>
        <translation>Doorsturen</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/popovers/MessageListActionPopover.qml" line="135"/>
        <source>Move</source>
        <translation>Verplaatsen</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/popovers/MessageListActionPopover.qml" line="144"/>
        <source>Restore to %1</source>
        <translation>Terugzetten in %1</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/popovers/MessageListActionPopover.qml" line="153"/>
        <source>Delete</source>
        <translation>Verwijderen</translation>
    </message>
</context>
<context>
    <name>MessageListDelegate</name>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/delegates/MessageListDelegate.qml" line="51"/>
        <source>Un-mark flagged</source>
        <translation>Label verwijderen</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/delegates/MessageListDelegate.qml" line="51"/>
        <source>Mark flagged</source>
        <translation>Label toevoegen</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/delegates/MessageListDelegate.qml" line="60"/>
        <source>Mark as un-read</source>
        <translation>Markeren als ongelezen</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/delegates/MessageListDelegate.qml" line="60"/>
        <source>Mark as read</source>
        <translation>Markeren als gelezen</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/delegates/MessageListDelegate.qml" line="67"/>
        <source>Move message</source>
        <translation>Bericht verplaatsen</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/delegates/MessageListDelegate.qml" line="72"/>
        <source>Context menu</source>
        <translation>Rechtermuisknopmenu</translation>
    </message>
</context>
<context>
    <name>MessageListView</name>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/views/MessageListView.qml" line="115"/>
        <source>Unselect all</source>
        <translation>Niets selecteren</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/views/MessageListView.qml" line="115"/>
        <source>Select all</source>
        <translation>Alles selecteren</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/views/MessageListView.qml" line="127"/>
        <source>Star</source>
        <translation>Toevoegen aan favorieten</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/views/MessageListView.qml" line="127"/>
        <source>Remove star</source>
        <translation>Verwijderen uit favorieten</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/views/MessageListView.qml" line="131"/>
        <source>Mark as un-read</source>
        <translation>Markeren als ongelezen</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/views/MessageListView.qml" line="131"/>
        <source>Mark as read</source>
        <translation>Markeren als gelezen</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/views/MessageListView.qml" line="138"/>
        <source>Delete</source>
        <translation>Verwijderen</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/views/MessageListView.qml" line="216"/>
        <source>Undo</source>
        <translation>Ongedaan maken</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/views/MessageListView.qml" line="340"/>
        <source>Load more messages ...</source>
        <translation>Meer berichten laden…</translation>
    </message>
</context>
<context>
    <name>MessageViewContextMenu</name>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/popovers/MessageViewContextMenu.qml" line="46"/>
        <source>Open in browser</source>
        <translation>Openen in webbrowser</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/popovers/MessageViewContextMenu.qml" line="54"/>
        <source>Copy link</source>
        <translation>Link kopiëren</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/popovers/MessageViewContextMenu.qml" line="63"/>
        <source>Share link</source>
        <translation>Link delen</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/popovers/MessageViewContextMenu.qml" line="77"/>
        <source>Reply</source>
        <translation>Beantwoorden</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/popovers/MessageViewContextMenu.qml" line="86"/>
        <source>Reply all</source>
        <translation>Allen beantwoorden</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/popovers/MessageViewContextMenu.qml" line="94"/>
        <source>Forward</source>
        <translation>Doorsturen</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/popovers/MessageViewContextMenu.qml" line="106"/>
        <source>View source</source>
        <translation>Broncode tonen</translation>
    </message>
</context>
<context>
    <name>MinimalMessage</name>
    <message>
        <location filename="../Dekko/backend/mail/Message.cpp" line="110"/>
        <location filename="../Dekko/backend/mail/Message.cpp" line="126"/>
        <source>hh:mm</source>
        <comment>time format as hours:minutes e.g. 12:45</comment>
        <translation>hh:mm</translation>
    </message>
    <message>
        <location filename="../Dekko/backend/mail/Message.cpp" line="115"/>
        <source>ddd hh:mm</source>
        <comment>time/date format as short day name hours:minutes e.g. Mon. 12:45</comment>
        <translation>ddd hh:mm</translation>
    </message>
    <message>
        <location filename="../Dekko/backend/mail/Message.cpp" line="117"/>
        <source>dd MMM</source>
        <comment>date format as day name and month e.g. 02 May</comment>
        <translation>dd MMM</translation>
    </message>
    <message>
        <location filename="../Dekko/backend/mail/Message.cpp" line="119"/>
        <source>dd MMM yy</source>
        <comment>date format as day month year e.g. 31 02 2020</comment>
        <translation>dd MMM yy</translation>
    </message>
    <message>
        <location filename="../Dekko/backend/mail/Message.cpp" line="126"/>
        <source>dddd dd</source>
        <comment>time format as long day name and day e.g. Monday 02</comment>
        <translation>dddd dd</translation>
    </message>
</context>
<context>
    <name>NavMenuAccountSettingsModel</name>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/models/NavMenuAccountSettingsModel.qml" line="29"/>
        <source>Manage accounts</source>
        <translation>Accounts beheren</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/models/NavMenuAccountSettingsModel.qml" line="45"/>
        <source>Display settings</source>
        <translation>Weergave-instellingen</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/models/NavMenuAccountSettingsModel.qml" line="67"/>
        <source>Privacy settings</source>
        <translation>Privacy-instellingen</translation>
    </message>
</context>
<context>
    <name>NavMenuContactsModel</name>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/models/NavMenuContactsModel.qml" line="27"/>
        <source>Addressbook</source>
        <translation>Adresboek</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/models/NavMenuContactsModel.qml" line="43"/>
        <source>Recent contacts</source>
        <translation>Recente contactpersonen</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/models/NavMenuContactsModel.qml" line="58"/>
        <source>Import contacts</source>
        <translation>Contactpersonen importeren</translation>
    </message>
</context>
<context>
    <name>NavMenuDekkoVisualModel</name>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/models/NavMenuDekkoVisualModel.qml" line="27"/>
        <source>Version</source>
        <translation>Versie</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/models/NavMenuDekkoVisualModel.qml" line="45"/>
        <source>Licenses</source>
        <translation>Licenties</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/models/NavMenuDekkoVisualModel.qml" line="63"/>
        <source>Contributors</source>
        <translation>Bijdragers</translation>
    </message>
</context>
<context>
    <name>NavMenuModel</name>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/models/NavMenuModel.qml" line="98"/>
        <source>Smart folders</source>
        <translation>Slimme mappen</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/models/NavMenuModel.qml" line="120"/>
        <source>Folders</source>
        <translation>Mappen</translation>
    </message>
</context>
<context>
    <name>NavMenuPage</name>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/views/NavMenuPage.qml" line="30"/>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/views/NavMenuPage.qml" line="64"/>
        <source>Mail</source>
        <translation>E-mail</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/views/NavMenuPage.qml" line="67"/>
        <source>Contacts</source>
        <translation>Contactpersonen</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/views/NavMenuPage.qml" line="70"/>
        <source>Settings</source>
        <translation>Instellingen</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/views/NavMenuPage.qml" line="73"/>
        <source>About</source>
        <translation>Over</translation>
    </message>
</context>
<context>
    <name>NavMenuStandardFolderDelegate</name>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/delegates/NavMenuStandardFolderDelegate.qml" line="192"/>
        <source>Inbox (%1)</source>
        <translation>Postvak in (%1)</translation>
    </message>
</context>
<context>
    <name>NavSideBar</name>
    <message>
        <location filename="../plugins/ubuntu-plugin/imports/components/private/NavSideBar.qml" line="155"/>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/views/NavSideBar.qml" line="151"/>
        <source>Smart folders</source>
        <translation>Slimme mappen</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/imports/components/private/NavSideBar.qml" line="169"/>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/views/NavSideBar.qml" line="165"/>
        <source>Folders</source>
        <translation>Mappen</translation>
    </message>
</context>
<context>
    <name>NavViewContextMenu</name>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/popovers/NavViewContextMenu.qml" line="44"/>
        <source>Sync folder</source>
        <translation>Map synchroniseren</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/popovers/NavViewContextMenu.qml" line="53"/>
        <source>Send pending</source>
        <translation>Berichten in wachtrij versturen</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/popovers/NavViewContextMenu.qml" line="62"/>
        <source>Mark folder read</source>
        <translation>Map markeren als gelezen</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/popovers/NavViewContextMenu.qml" line="76"/>
        <source>Empty trash</source>
        <translation>Prullenbak legen</translation>
    </message>
</context>
<context>
    <name>NewAccountsUI</name>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/setupwizard/components/NewAccountsUI.qml" line="32"/>
        <source>New account</source>
        <translation>Nieuw account</translation>
    </message>
</context>
<context>
    <name>NoAccountsUI</name>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/setupwizard/components/NoAccountsUI.qml" line="31"/>
        <source>Accounts</source>
        <translation>Accounts</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/setupwizard/components/NoAccountsUI.qml" line="93"/>
        <source>No email account is setup.</source>
        <translation>Je hebt nog geen account ingesteld.</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/setupwizard/components/NoAccountsUI.qml" line="102"/>
        <source>Add now</source>
        <translation>Nu instellen</translation>
    </message>
</context>
<context>
    <name>NothingSelectedPage</name>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/views/NothingSelectedPage.qml" line="55"/>
        <source>No message selected</source>
        <translation>Er is geen bericht geselecteerd</translation>
    </message>
</context>
<context>
    <name>NoticePopup</name>
    <message>
        <location filename="../plugins/ubuntu-plugin/imports/dialogs/NoticePopup.qml" line="27"/>
        <source>Ok</source>
        <translation>Oké</translation>
    </message>
</context>
<context>
    <name>NotificationSettings</name>
    <message>
        <location filename="../plugins/ubuntu-notification-plugin/qml/NotificationSettings.qml" line="7"/>
        <source>Notifications</source>
        <translation>Meldingen</translation>
    </message>
</context>
<context>
    <name>NotificationSettingsPage</name>
    <message>
        <location filename="../plugins/ubuntu-notification-plugin/qml/NotificationSettingsPage.qml" line="26"/>
        <source>Notification Settings: %1</source>
        <translation>Meldingsinstellingen: %1</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-notification-plugin/qml/NotificationSettingsPage.qml" line="52"/>
        <source>Enabled</source>
        <translation>Meldingen tonen</translation>
    </message>
</context>
<context>
    <name>NotificationWatcher</name>
    <message>
        <location filename="../plugins/ubuntu-notification-plugin/service/notificationwatcher.cpp" line="57"/>
        <source>%1 New messages</source>
        <translation>%1 nieuwe berichten</translation>
    </message>
</context>
<context>
    <name>OutgoingServerGroup</name>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/settings/OutgoingServerGroup.qml" line="29"/>
        <source>Outgoing Server: %1</source>
        <translation>Uitgaande server: %1</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/settings/OutgoingServerGroup.qml" line="76"/>
        <source>Hostname</source>
        <translation>Hostnaam</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/settings/OutgoingServerGroup.qml" line="84"/>
        <source>Port</source>
        <translation>Poort</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/settings/OutgoingServerGroup.qml" line="93"/>
        <source>Username</source>
        <translation>Gebruikersnaam</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/settings/OutgoingServerGroup.qml" line="101"/>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/settings/OutgoingServerGroup.qml" line="104"/>
        <source>Password</source>
        <translation>Wachtwoord</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/settings/OutgoingServerGroup.qml" line="111"/>
        <source>Show password</source>
        <translation>Wachtwoord tonen</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/settings/OutgoingServerGroup.qml" line="117"/>
        <source>Security settings</source>
        <translation>Beveiligingsinstellingen</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/settings/OutgoingServerGroup.qml" line="150"/>
        <source>Authenticate from server capabilities</source>
        <translation>Verifiëren met behulp van server</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/settings/OutgoingServerGroup.qml" line="156"/>
        <source>Allow untrusted certificates</source>
        <translation>Onvertrouwde certificaten toestaan</translation>
    </message>
</context>
<context>
    <name>PopClient</name>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/pop/popclient.cpp" line="186"/>
        <location filename="../upstream/qmf/plugins/messageservices/pop/popclient.cpp" line="222"/>
        <source>Cannot open connection without POP server configuration</source>
        <translation>Er kan geen verbinding zonder pop-serverinstellingen worden opgezet</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/pop/popclient.cpp" line="345"/>
        <source>Inbox</source>
        <translation>Postvak in</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/pop/popclient.cpp" line="409"/>
        <source>Connected</source>
        <translation>Verbonden</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/pop/popclient.cpp" line="786"/>
        <source>Logging in</source>
        <translation>Bezig met inloggen…</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/pop/popclient.cpp" line="860"/>
        <source>Previewing</source>
        <comment>Previewing &lt;no of messages&gt;</comment>
        <translation>Bezig met voorvertonen…</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/pop/popclient.cpp" line="867"/>
        <source>Completing %1 / %2</source>
        <translation>Bezig met voltooien van %1/%2…</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/pop/popclient.cpp" line="883"/>
        <source>Removing old messages</source>
        <translation>Bezig met verwijderen van oude berichten…</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/pop/popclient.cpp" line="911"/>
        <source>Removing message from server</source>
        <translation>Bezig met verwijderen van server…</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/pop/popclient.cpp" line="995"/>
        <source>Logging out</source>
        <translation>Bezig met uitloggen…</translation>
    </message>
</context>
<context>
    <name>PopService</name>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/pop/popservice.cpp" line="424"/>
        <source>Account disabled</source>
        <translation>Het account is uitgeschakeld</translation>
    </message>
</context>
<context>
    <name>PopService::Source</name>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/pop/popservice.cpp" line="116"/>
        <location filename="../upstream/qmf/plugins/messageservices/pop/popservice.cpp" line="153"/>
        <location filename="../upstream/qmf/plugins/messageservices/pop/popservice.cpp" line="215"/>
        <location filename="../upstream/qmf/plugins/messageservices/pop/popservice.cpp" line="231"/>
        <source>No account specified</source>
        <translation>Er is geen account gekozen</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/pop/popservice.cpp" line="140"/>
        <source>No folders specified</source>
        <translation>Er zijn geen mappen doorgegeven</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/pop/popservice.cpp" line="186"/>
        <source>No messages to retrieve</source>
        <translation>Er zijn geen berichten om op te halen</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/pop/popservice.cpp" line="251"/>
        <source>No messages to delete</source>
        <translation>Er zijn geen berichten om te verwijderen</translation>
    </message>
</context>
<context>
    <name>PopSettings</name>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/pop/popsettings.ui" line="14"/>
        <source>Form</source>
        <translation>Formulier</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/pop/popsettings.ui" line="56"/>
        <source>Username</source>
        <translation>Gebruikersnaam</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/pop/popsettings.ui" line="82"/>
        <source>Password</source>
        <translation>Wachtwoord</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/pop/popsettings.ui" line="111"/>
        <source>Server</source>
        <translation>Server</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/pop/popsettings.ui" line="137"/>
        <source>Port</source>
        <translation>Poort</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/pop/popsettings.ui" line="163"/>
        <source>Encryption</source>
        <translation>Versleuteling</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/pop/popsettings.ui" line="180"/>
        <source>None</source>
        <translation>Geen</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/pop/popsettings.ui" line="185"/>
        <source>SSL</source>
        <translation>Ssl</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/pop/popsettings.ui" line="190"/>
        <source>TLS</source>
        <translation>Tls</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/pop/popsettings.ui" line="204"/>
        <source>Remove deleted mail from server</source>
        <translation>Verwijderde e-mail verwijderen van server</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/pop/popsettings.ui" line="226"/>
        <source>Skip larger</source>
        <translation>Grotere overslaan</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/pop/popsettings.ui" line="248"/>
        <source>Interval</source>
        <translation>Controleren, elke</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/pop/popsettings.ui" line="270"/>
        <source>Disable when Roaming</source>
        <translation>Uitschakelen tijdens roamen</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/pop/popsettings.ui" line="286"/>
        <source>min</source>
        <comment>short for minutes</comment>
        <translation>min.</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/pop/popsettings.ui" line="314"/>
        <source>K</source>
        <translation>K</translation>
    </message>
</context>
<context>
    <name>PrivacySettings</name>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/settings/PrivacySettings.qml" line="28"/>
        <source>Message content</source>
        <translation>Berichtinhoud</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/settings/PrivacySettings.qml" line="32"/>
        <source>Allow remote content</source>
        <translation>Externe inhoud toestaan</translation>
    </message>
</context>
<context>
    <name>PrivacySettingsPage</name>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/settings/PrivacySettingsPage.qml" line="21"/>
        <source>Privacy Settings</source>
        <translation>Privacy-instellingen</translation>
    </message>
</context>
<context>
    <name>PrivacySettingsPopup</name>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/settings/PrivacySettingsPopup.qml" line="21"/>
        <source>Privacy Settings</source>
        <translation>Privacy-instellingen</translation>
    </message>
</context>
<context>
    <name>PushFolderList</name>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapsettings.cpp" line="153"/>
        <source>Push folder</source>
        <translation>Pushmap</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapsettings.cpp" line="163"/>
        <source>...</source>
        <translation>…</translation>
    </message>
</context>
<context>
    <name>QCoreApplication</name>
    <message>
        <location filename="../upstream/qmf/libraries/qmfclient/longstream.cpp" line="236"/>
        <source>Storage for messages is full. Some new messages could not be retrieved.</source>
        <translation>Er is geen vrije ruimte meer in de berichtopslag. Enkele nieuwe berichten zijn daardoor niet opgehaald.</translation>
    </message>
</context>
<context>
    <name>QMailMessageModelBase</name>
    <message>
        <location filename="../upstream/qmf/libraries/qmfclient/qmailmessagemodelbase.cpp" line="67"/>
        <source>Draft message</source>
        <translation>Concept</translation>
    </message>
    <message numerus="yes">
        <location filename="../upstream/qmf/libraries/qmfclient/qmailmessagemodelbase.cpp" line="78"/>
        <location filename="../upstream/qmf/libraries/qmfclient/qmailmessagemodelbase.cpp" line="80"/>
        <source>%n byte(s)</source>
        <translation>
            <numerusform>%n byte</numerusform>
            <numerusform>%n bytes</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../upstream/qmf/libraries/qmfclient/qmailmessagemodelbase.cpp" line="83"/>
        <source>%1 KB</source>
        <translation>%1 KB</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/libraries/qmfclient/qmailmessagemodelbase.cpp" line="85"/>
        <source>%1 MB</source>
        <translation>%1 MB</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/libraries/qmfclient/qmailmessagemodelbase.cpp" line="87"/>
        <source>%1 GB</source>
        <translation>%1 GB</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/libraries/qmfclient/qmailmessagemodelbase.cpp" line="264"/>
        <source>Received</source>
        <translation>Ontvangen</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/libraries/qmfclient/qmailmessagemodelbase.cpp" line="267"/>
        <source>Sent</source>
        <translation>Verzonden</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/libraries/qmfclient/qmailmessagemodelbase.cpp" line="269"/>
        <source>Last edited</source>
        <translation>Recentste bewerking</translation>
    </message>
</context>
<context>
    <name>QMailMessageService</name>
    <message>
        <location filename="../upstream/qmf/libraries/qmfmessageserver/qmailmessageservice.h" line="300"/>
        <location filename="../upstream/qmf/libraries/qmfmessageserver/qmailmessageservice.h" line="301"/>
        <source>Cancelled by user</source>
        <translation>Afgebroken door gebruiker</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapservice.cpp" line="2235"/>
        <source>IMAP</source>
        <translation>Imap</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/pop/popservice.cpp" line="464"/>
        <source>POP</source>
        <translation>Pop</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/qmfsettings/service.cpp" line="81"/>
        <source>Mailfile</source>
        <translation>Mailfile</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/smtp/smtpservice.cpp" line="371"/>
        <source>SMTP</source>
        <translation>Smtp</translation>
    </message>
</context>
<context>
    <name>QMailMessageSource</name>
    <message>
        <location filename="../upstream/qmf/libraries/qmfmessageserver/qmailmessageservice.cpp" line="1104"/>
        <source>Unable to delete messages</source>
        <translation>De berichten kunnen niet worden verwijderd</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/libraries/qmfmessageserver/qmailmessageservice.cpp" line="1117"/>
        <source>Insufficient space to copy messages to folder</source>
        <translation>Er is onvoldoende vrije ruimte om de berichten te kopiëren</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/libraries/qmfmessageserver/qmailmessageservice.cpp" line="1138"/>
        <source>Unable to copy messages for account</source>
        <translation>Berichten van dit account kunnen niet worden gekopieerd</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/libraries/qmfmessageserver/qmailmessageservice.cpp" line="1172"/>
        <source>Unable to move messages to folder</source>
        <translation>De berichten kunnen niet worden verplaatst</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/libraries/qmfmessageserver/qmailmessageservice.cpp" line="1189"/>
        <source>Unable to flag messages</source>
        <translation>De berichten kunnen niet worden gemarkeerd</translation>
    </message>
</context>
<context>
    <name>QMailServiceAction</name>
    <message>
        <location filename="../upstream/qmf/libraries/qmfmessageserver/qmailmessageservice.cpp" line="277"/>
        <source>Connection refused</source>
        <translation>Verbinding geweigerd</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/libraries/qmfmessageserver/qmailmessageservice.cpp" line="278"/>
        <source>Remote host closed the connection</source>
        <translation>De externe host heeft de verbinding verbroken</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/libraries/qmfmessageserver/qmailmessageservice.cpp" line="279"/>
        <source>Host not found</source>
        <translation>De host is niet beschikbaar</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/libraries/qmfmessageserver/qmailmessageservice.cpp" line="280"/>
        <source>Permission denied</source>
        <translation>De toegang is geweigerd</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/libraries/qmfmessageserver/qmailmessageservice.cpp" line="281"/>
        <source>Insufficient resources</source>
        <translation>Er zijn onvoldoende bronnen</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/libraries/qmfmessageserver/qmailmessageservice.cpp" line="282"/>
        <source>Operation timed out</source>
        <translation>De actie is verlopen</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/libraries/qmfmessageserver/qmailmessageservice.cpp" line="283"/>
        <source>Datagram too large</source>
        <translation>Het datagram is te groot</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/libraries/qmfmessageserver/qmailmessageservice.cpp" line="284"/>
        <source>Network error</source>
        <translation>Er is geen internetverbinding</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/libraries/qmfmessageserver/qmailmessageservice.cpp" line="285"/>
        <source>Address in use</source>
        <translation>Het adres is al in gebruik</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/libraries/qmfmessageserver/qmailmessageservice.cpp" line="286"/>
        <source>Address not available</source>
        <translation>Het adres is niet beschikbaar</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/libraries/qmfmessageserver/qmailmessageservice.cpp" line="287"/>
        <source>Unsupported operation</source>
        <translation>Deze actie wordt niet ondersteund</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/libraries/qmfmessageserver/qmailmessageservice.cpp" line="288"/>
        <source>Unknown error</source>
        <translation>Onbekende foutmelding</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/libraries/qmfmessageserver/qmailmessageservice.cpp" line="298"/>
        <source>This function is not currently supported.</source>
        <translation>Deze functie wordt momenteel niet ondersteund.</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/libraries/qmfmessageserver/qmailmessageservice.cpp" line="299"/>
        <source>Framework error occurred.</source>
        <translation>Er is een frameworkfout opgetreden.</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/libraries/qmfmessageserver/qmailmessageservice.cpp" line="302"/>
        <source>Login failed - check user name and password.</source>
        <translation>Het inloggen is mislukt - controleer je gebruikersnaam en wachtwoord.</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/libraries/qmfmessageserver/qmailmessageservice.cpp" line="303"/>
        <source>Operation cancelled.</source>
        <translation>De actie is afgebroken.</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/libraries/qmfmessageserver/qmailmessageservice.cpp" line="304"/>
        <source>Mail check failed.</source>
        <translation>De controle kan niet worden uitgevoerd.</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/libraries/qmfmessageserver/qmailmessageservice.cpp" line="305"/>
        <source>Message deleted from server.</source>
        <translation>Het bericht is verwijderd van de server.</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/libraries/qmfmessageserver/qmailmessageservice.cpp" line="306"/>
        <source>Unable to queue message for transmission.</source>
        <translation>Het bericht kan niet worden klaargemaakt voor overdracht.</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/libraries/qmfmessageserver/qmailmessageservice.cpp" line="307"/>
        <source>Cannot determine the connection to transmit message on.</source>
        <translation>Er kan geen verbinding worden vastgesteld waarover het bericht moet worden overgedragen.</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/libraries/qmfmessageserver/qmailmessageservice.cpp" line="308"/>
        <source>Outgoing connection already in use by another operation.</source>
        <translation>De uitgaande verbinding is bezet.</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/libraries/qmfmessageserver/qmailmessageservice.cpp" line="309"/>
        <source>Outgoing connection is not ready to transmit message.</source>
        <translation>De uitgaande verbinding is niet gereed.</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/libraries/qmfmessageserver/qmailmessageservice.cpp" line="310"/>
        <source>Unable to use account due to invalid configuration.</source>
        <translation>Het account kan niet worden gebruikt omdat de instellingen ongeldig zijn.</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/libraries/qmfmessageserver/qmailmessageservice.cpp" line="311"/>
        <source>Message origin or recipient addresses are not correctly formatted.</source>
        <translation>Het afzender- of ontvangeradres is onjuist opgemaakt.</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/libraries/qmfmessageserver/qmailmessageservice.cpp" line="312"/>
        <source>Configured service unable to handle supplied data.</source>
        <translation>De ingestelde dienst kan de aangeleverde gegevens niet verwerken.</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/libraries/qmfmessageserver/qmailmessageservice.cpp" line="313"/>
        <source>Configured service failed to perform action within a reasonable period of time.</source>
        <translation>De ingestelde dienst kan de actie niet binnen een redelijk tijdsbestek uitvoeren.</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/libraries/qmfmessageserver/qmailmessageservice.cpp" line="356"/>
        <source>Unable to send; message moved to Drafts folder</source>
        <translation>Het bericht kan niet worden verstuurd en is verplaatst naar de concepten</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/libraries/qmfmessageserver/qmailmessageservice.cpp" line="358"/>
        <source>Unexpected response from server: </source>
        <translation>Onverwacht antwoord van server: </translation>
    </message>
    <message>
        <location filename="../upstream/qmf/libraries/qmfmessageserver/qmailmessageservice.cpp" line="366"/>
        <source>Error %1</source>
        <comment>%1 contains numeric error code</comment>
        <translation>Foutmelding %1</translation>
    </message>
</context>
<context>
    <name>QMailStorePrivate</name>
    <message>
        <location filename="../upstream/qmf/libraries/qmfclient/qmailstore_p.cpp" line="3175"/>
        <source>Local Storage</source>
        <translation>Lokale opslag</translation>
    </message>
</context>
<context>
    <name>QMailTransport</name>
    <message>
        <location filename="../upstream/qmf/libraries/qmfmessageserver/qmailtransport.cpp" line="289"/>
        <source>DNS lookup</source>
        <translation>Dns-opzoeking</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/libraries/qmfmessageserver/qmailtransport.cpp" line="415"/>
        <location filename="../upstream/qmf/libraries/qmfmessageserver/qmailtransport.cpp" line="435"/>
        <source>Connected</source>
        <translation>Verbonden</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/libraries/qmfmessageserver/qmailtransport.cpp" line="426"/>
        <source>Connection timed out</source>
        <translation>De verbinding is verlopen</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/libraries/qmfmessageserver/qmailtransport.cpp" line="455"/>
        <location filename="../upstream/qmf/libraries/qmfmessageserver/qmailtransport.cpp" line="494"/>
        <source>Error occurred</source>
        <translation>Er is een fout opgetreden</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/libraries/qmfmessageserver/qmailtransport.cpp" line="458"/>
        <location filename="../upstream/qmf/libraries/qmfmessageserver/qmailtransport.cpp" line="460"/>
        <location filename="../upstream/qmf/libraries/qmfmessageserver/qmailtransport.cpp" line="504"/>
        <source>Socket error</source>
        <translation>Socketfout</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message numerus="yes">
        <location filename="../Dekko/backend/mail/Attachments.cpp" line="344"/>
        <source>%n byte(s)</source>
        <translation>
            <numerusform>%n byte</numerusform>
            <numerusform>%n bytes</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../Dekko/backend/mail/Attachments.cpp" line="346"/>
        <source>%1 KB</source>
        <translation>%1 KB</translation>
    </message>
    <message>
        <location filename="../Dekko/backend/mail/Attachments.cpp" line="348"/>
        <source>%1 MB</source>
        <translation>%1 MB</translation>
    </message>
    <message>
        <location filename="../Dekko/backend/mail/Attachments.cpp" line="350"/>
        <source>%1 GB</source>
        <translation>%1 GB</translation>
    </message>
    <message>
        <location filename="../upstream/maz-db/leveldb/util/env_qt.cc" line="122"/>
        <source>File not open</source>
        <translation>Het bestand is niet geopend</translation>
    </message>
    <message>
        <location filename="../upstream/maz-db/leveldb/util/env_qt.cc" line="277"/>
        <source>Could not create path</source>
        <translation>De locatie kan niet worden aangemaakt</translation>
    </message>
    <message>
        <location filename="../upstream/maz-db/leveldb/util/env_qt.cc" line="285"/>
        <source>Could not delete path</source>
        <translation>De locatie kan niet worden verwijderd</translation>
    </message>
    <message>
        <location filename="../upstream/maz-db/leveldb/util/env_qt.cc" line="324"/>
        <source>No error</source>
        <translation>Geen foutmelding</translation>
    </message>
    <message>
        <location filename="../upstream/maz-db/leveldb/util/env_qt.cc" line="327"/>
        <source>Lock failed error</source>
        <translation>Het vergrendelen is mislukt</translation>
    </message>
    <message>
        <location filename="../upstream/maz-db/leveldb/util/env_qt.cc" line="330"/>
        <source>Permission error</source>
        <translation>Toegangsfout</translation>
    </message>
    <message>
        <location filename="../upstream/maz-db/leveldb/util/env_qt.cc" line="333"/>
        <source>Unknown error</source>
        <translation>Onbekende fout</translation>
    </message>
    <message>
        <location filename="../upstream/maz-db/src/quick/MazDBPlugin.cpp" line="44"/>
        <location filename="../upstream/maz-db/tests/runner.cpp" line="27"/>
        <source>Cannot create separate instance of Options</source>
        <translation>Er kan geen gescheiden proces van ‘Options’ worden gemaakt</translation>
    </message>
    <message>
        <location filename="../upstream/maz-db/src/quick/MazDBPlugin.cpp" line="45"/>
        <location filename="../upstream/maz-db/tests/runner.cpp" line="28"/>
        <source>Cannot create separate instance of Batch</source>
        <translation>Er kan geen gescheiden proces van ‘Batch’ worden gemaakt</translation>
    </message>
    <message>
        <location filename="../upstream/maz-db/src/quick/MazDBPlugin.cpp" line="46"/>
        <location filename="../upstream/maz-db/tests/runner.cpp" line="29"/>
        <source>Cannot create separate instance of QueryBase</source>
        <translation>Er kan geen gescheiden proces van ‘QueryBase’ worden gemaakt</translation>
    </message>
    <message>
        <location filename="../upstream/maz-db/src/quick/MazDBPlugin.cpp" line="47"/>
        <source>Cannot create separate instance of Range</source>
        <translation>Er kan geen gescheiden proces van ‘Range’ worden gemaakt</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapprotocol.cpp" line="2035"/>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapprotocol.cpp" line="2118"/>
        <source>This server does not provide a complete IMAP4rev1 implementation.</source>
        <translation>Deze server biedt geen volledige IMAP4rev1-implementatie.</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapstrategy.cpp" line="1753"/>
        <source>Completing %1 / %2</source>
        <translation>Bezig met voltooien van %1/%2…</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapstrategy.cpp" line="2201"/>
        <source>Previewing</source>
        <comment>Previewing &lt;number of messages&gt;</comment>
        <translation>Bezig met voorvertonen…</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapstrategy.cpp" line="2240"/>
        <source>Checking</source>
        <comment>Checking &lt;mailbox name&gt;</comment>
        <translation>Bezig met controleren…</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapstrategy.cpp" line="2358"/>
        <source>Retrieving folders</source>
        <translation>Bezig met ophalen van mappen…</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapstrategy.cpp" line="2810"/>
        <source>Marking message as read</source>
        <translation>Bezig met markeren als gelezen…</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapstrategy.cpp" line="2829"/>
        <source>Marking message as unread</source>
        <translation>Bezig met markeren als ongelezen…</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapstrategy.cpp" line="2848"/>
        <source>Marking message as important</source>
        <translation>Bezig met markeren als belangrijk…</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapstrategy.cpp" line="2867"/>
        <source>Marking message as unimportant</source>
        <translation>Bezig met markeren als onbelangrijk…</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapstrategy.cpp" line="2888"/>
        <source>Deleting message</source>
        <translation>Bezig met verwijderen…</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapstrategy.cpp" line="3350"/>
        <source>Scanning folders</source>
        <translation>Bezig met doorzoeken van mappen…</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapstrategy.cpp" line="3352"/>
        <source>Scanning folder</source>
        <translation>Bezig met doorzoeken van map…</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapstrategy.cpp" line="3958"/>
        <source>Copying %1 / %2</source>
        <translation>Bezig met kopiëren van %1/%2…</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapstrategy.cpp" line="4262"/>
        <source>Moving %1 / %2</source>
        <translation>Bezig met verplaatsen van %1/%2…</translation>
    </message>
</context>
<context>
    <name>QtopiamailfileSettings</name>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/qmfsettings/settings.ui" line="20"/>
        <source>Form</source>
        <translation>Formulier</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/qmfsettings/settings.ui" line="32"/>
        <source>Location</source>
        <translation>Locatie</translation>
    </message>
</context>
<context>
    <name>RecipientField</name>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/composer/RecipientField.qml" line="98"/>
        <source>Enter an address</source>
        <translation>Voer een adres in</translation>
    </message>
</context>
<context>
    <name>RecipientInfo</name>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/messageview/RecipientInfo.qml" line="42"/>
        <source>Back</source>
        <translation>Terug</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/messageview/RecipientInfo.qml" line="94"/>
        <source>Copy to clipboard</source>
        <translation>Kopiëren naar klembord</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/messageview/RecipientInfo.qml" line="108"/>
        <source>Add to addressbook</source>
        <translation>Toevoegen aan adresboek</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/messageview/RecipientInfo.qml" line="123"/>
        <source>Send message</source>
        <translation>Bericht versturen</translation>
    </message>
</context>
<context>
    <name>RecipientInputContextMenu</name>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/composer/RecipientInputContextMenu.qml" line="56"/>
        <source>Add CC</source>
        <translation>Cc toevoegen</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/composer/RecipientInputContextMenu.qml" line="69"/>
        <source>Add BCC</source>
        <translation>Bcc toevoegen</translation>
    </message>
</context>
<context>
    <name>RecipientPopover</name>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/popovers/RecipientPopover.qml" line="81"/>
        <source>Copy to clipboard</source>
        <translation>Kopiëren naar klembord</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/popovers/RecipientPopover.qml" line="90"/>
        <source>Add to addressbook</source>
        <translation>Toevoegen aan adresboek</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/popovers/RecipientPopover.qml" line="99"/>
        <source>Send message</source>
        <translation>Bericht versturen</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/popovers/RecipientPopover.qml" line="109"/>
        <source>Remove</source>
        <translation>Verwijderen</translation>
    </message>
</context>
<context>
    <name>S:</name>
    <message>
        <location filename="../build/x86_64-linux-gnu/build/install-root/lib/x86_64-linux-gnu/Dekko/Mail/Stores/Accounts/AccountSetup.qml" line="536"/>
        <source></source>
        <comment>&apos;convergent&apos; describes the apps ability of being usable on desktop and mobile devices running different OS</comment>
        <translation></translation>
    </message>
</context>
<context>
    <name>SenderIdentityField</name>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/composer/SenderIdentityField.qml" line="53"/>
        <source>From:</source>
        <translation>Van:</translation>
    </message>
</context>
<context>
    <name>ServerDetails</name>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/setupwizard/components/ServerDetails.qml" line="70"/>
        <source>Hostname</source>
        <translation>Hostnaam</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/setupwizard/components/ServerDetails.qml" line="79"/>
        <source>Port</source>
        <translation>Poort</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/setupwizard/components/ServerDetails.qml" line="101"/>
        <source>Username</source>
        <translation>Gebruikersnaam</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/setupwizard/components/ServerDetails.qml" line="110"/>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/setupwizard/components/ServerDetails.qml" line="113"/>
        <source>Password</source>
        <translation>Wachtwoord</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/setupwizard/components/ServerDetails.qml" line="120"/>
        <source>Show password</source>
        <translation>Wachtwoord tonen</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/setupwizard/components/ServerDetails.qml" line="141"/>
        <source>Allow untrusted certificates</source>
        <translation>Onvertrouwde certificaten toestaan</translation>
    </message>
</context>
<context>
    <name>ServiceHandler</name>
    <message>
        <location filename="../Dekko/server/servicehandler.cpp" line="593"/>
        <location filename="../upstream/qmf/tools/messageserver/servicehandler.cpp" line="589"/>
        <source>Destroying Service handler</source>
        <translation>Bezig met vernietigen van dienstafhandeling…</translation>
    </message>
    <message>
        <location filename="../Dekko/server/servicehandler.cpp" line="645"/>
        <location filename="../upstream/qmf/tools/messageserver/servicehandler.cpp" line="641"/>
        <source>Service became unavailable, couldn&apos;t dispatch</source>
        <translation>De dienst is niet beschikbaar - verwerken is niet mogelijk</translation>
    </message>
    <message>
        <location filename="../Dekko/server/servicehandler.cpp" line="826"/>
        <location filename="../upstream/qmf/tools/messageserver/servicehandler.cpp" line="822"/>
        <source>Account updated by other process</source>
        <translation>Het account is bijgewerkt door een ander proces</translation>
    </message>
    <message>
        <location filename="../Dekko/server/servicehandler.cpp" line="833"/>
        <location filename="../upstream/qmf/tools/messageserver/servicehandler.cpp" line="829"/>
        <source>Account removed</source>
        <translation>Het account is verwijderd</translation>
    </message>
    <message>
        <location filename="../Dekko/server/servicehandler.cpp" line="1234"/>
        <location filename="../Dekko/server/servicehandler.cpp" line="1283"/>
        <location filename="../upstream/qmf/tools/messageserver/servicehandler.cpp" line="1230"/>
        <location filename="../upstream/qmf/tools/messageserver/servicehandler.cpp" line="1279"/>
        <source>Request is not progressing</source>
        <translation>Het verzoek kan niet worden verwerkt</translation>
    </message>
    <message>
        <location filename="../Dekko/server/servicehandler.cpp" line="1320"/>
        <location filename="../Dekko/server/servicehandler.cpp" line="1322"/>
        <location filename="../Dekko/server/servicehandler.cpp" line="1370"/>
        <location filename="../upstream/qmf/tools/messageserver/servicehandler.cpp" line="1316"/>
        <location filename="../upstream/qmf/tools/messageserver/servicehandler.cpp" line="1318"/>
        <location filename="../upstream/qmf/tools/messageserver/servicehandler.cpp" line="1366"/>
        <source>Cancelled by user</source>
        <translation>Afgebroken door gebruiker</translation>
    </message>
    <message>
        <location filename="../Dekko/server/servicehandler.cpp" line="1379"/>
        <location filename="../Dekko/server/servicehandler.cpp" line="1415"/>
        <location filename="../upstream/qmf/tools/messageserver/servicehandler.cpp" line="1375"/>
        <location filename="../upstream/qmf/tools/messageserver/servicehandler.cpp" line="1411"/>
        <source>Unable to enqueue messages for transmission</source>
        <translation>De berichten kunnen niet in de wachtrij worden geplaatst</translation>
    </message>
    <message>
        <location filename="../Dekko/server/servicehandler.cpp" line="1463"/>
        <location filename="../Dekko/server/servicehandler.cpp" line="1574"/>
        <location filename="../Dekko/server/servicehandler.cpp" line="1612"/>
        <location filename="../Dekko/server/servicehandler.cpp" line="1650"/>
        <location filename="../Dekko/server/servicehandler.cpp" line="1686"/>
        <location filename="../Dekko/server/servicehandler.cpp" line="1722"/>
        <location filename="../Dekko/server/servicehandler.cpp" line="1763"/>
        <location filename="../Dekko/server/servicehandler.cpp" line="1803"/>
        <location filename="../Dekko/server/servicehandler.cpp" line="1842"/>
        <location filename="../Dekko/server/servicehandler.cpp" line="1882"/>
        <location filename="../Dekko/server/servicehandler.cpp" line="1918"/>
        <location filename="../Dekko/server/servicehandler.cpp" line="1951"/>
        <location filename="../Dekko/server/servicehandler.cpp" line="1988"/>
        <location filename="../Dekko/server/servicehandler.cpp" line="2056"/>
        <location filename="../Dekko/server/servicehandler.cpp" line="2111"/>
        <location filename="../Dekko/server/servicehandler.cpp" line="2188"/>
        <location filename="../Dekko/server/servicehandler.cpp" line="2397"/>
        <location filename="../Dekko/server/servicehandler.cpp" line="2517"/>
        <location filename="../Dekko/server/servicehandler.cpp" line="2553"/>
        <location filename="../Dekko/server/servicehandler.cpp" line="2584"/>
        <location filename="../Dekko/server/servicehandler.cpp" line="2680"/>
        <location filename="../Dekko/server/servicehandler.cpp" line="2949"/>
        <location filename="../upstream/qmf/tools/messageserver/servicehandler.cpp" line="1459"/>
        <location filename="../upstream/qmf/tools/messageserver/servicehandler.cpp" line="1570"/>
        <location filename="../upstream/qmf/tools/messageserver/servicehandler.cpp" line="1608"/>
        <location filename="../upstream/qmf/tools/messageserver/servicehandler.cpp" line="1646"/>
        <location filename="../upstream/qmf/tools/messageserver/servicehandler.cpp" line="1682"/>
        <location filename="../upstream/qmf/tools/messageserver/servicehandler.cpp" line="1718"/>
        <location filename="../upstream/qmf/tools/messageserver/servicehandler.cpp" line="1759"/>
        <location filename="../upstream/qmf/tools/messageserver/servicehandler.cpp" line="1799"/>
        <location filename="../upstream/qmf/tools/messageserver/servicehandler.cpp" line="1838"/>
        <location filename="../upstream/qmf/tools/messageserver/servicehandler.cpp" line="1878"/>
        <location filename="../upstream/qmf/tools/messageserver/servicehandler.cpp" line="1914"/>
        <location filename="../upstream/qmf/tools/messageserver/servicehandler.cpp" line="1947"/>
        <location filename="../upstream/qmf/tools/messageserver/servicehandler.cpp" line="1984"/>
        <location filename="../upstream/qmf/tools/messageserver/servicehandler.cpp" line="2052"/>
        <location filename="../upstream/qmf/tools/messageserver/servicehandler.cpp" line="2107"/>
        <location filename="../upstream/qmf/tools/messageserver/servicehandler.cpp" line="2184"/>
        <location filename="../upstream/qmf/tools/messageserver/servicehandler.cpp" line="2393"/>
        <location filename="../upstream/qmf/tools/messageserver/servicehandler.cpp" line="2513"/>
        <location filename="../upstream/qmf/tools/messageserver/servicehandler.cpp" line="2549"/>
        <location filename="../upstream/qmf/tools/messageserver/servicehandler.cpp" line="2580"/>
        <location filename="../upstream/qmf/tools/messageserver/servicehandler.cpp" line="2676"/>
        <location filename="../upstream/qmf/tools/messageserver/servicehandler.cpp" line="2945"/>
        <source>Unable to locate source for account</source>
        <translation>De accountbron kan niet worden gevonden</translation>
    </message>
    <message>
        <location filename="../Dekko/server/servicehandler.cpp" line="1500"/>
        <location filename="../Dekko/server/servicehandler.cpp" line="1536"/>
        <location filename="../upstream/qmf/tools/messageserver/servicehandler.cpp" line="1496"/>
        <location filename="../upstream/qmf/tools/messageserver/servicehandler.cpp" line="1532"/>
        <source>Unable to locate sink for account</source>
        <translation>De accountsink kan niet worden gevonden</translation>
    </message>
    <message>
        <location filename="../Dekko/server/servicehandler.cpp" line="1547"/>
        <location filename="../upstream/qmf/tools/messageserver/servicehandler.cpp" line="1543"/>
        <source>Unable to retrieve folder list for unconfigured account</source>
        <translation>Mappenlijsten van niet-ingestelde accounts kunnen niet worden opgehaald</translation>
    </message>
    <message>
        <location filename="../Dekko/server/servicehandler.cpp" line="1585"/>
        <location filename="../Dekko/server/servicehandler.cpp" line="1623"/>
        <location filename="../upstream/qmf/tools/messageserver/servicehandler.cpp" line="1581"/>
        <location filename="../upstream/qmf/tools/messageserver/servicehandler.cpp" line="1619"/>
        <source>Unable to retrieve message list for unconfigured account</source>
        <translation>Berichtlijsten van niet-ingestelde accounts kunnen niet worden opgehaald</translation>
    </message>
    <message>
        <location filename="../Dekko/server/servicehandler.cpp" line="1661"/>
        <location filename="../upstream/qmf/tools/messageserver/servicehandler.cpp" line="1657"/>
        <source>Unable to retrieve new messages for unconfigured account</source>
        <translation>Berichten van niet-ingestelde accounts kunnen niet worden opgehaald</translation>
    </message>
    <message>
        <location filename="../Dekko/server/servicehandler.cpp" line="1697"/>
        <location filename="../upstream/qmf/tools/messageserver/servicehandler.cpp" line="1693"/>
        <source>Unable to retrieve standard folders for unconfigured account</source>
        <translation>Standaardmappen van niet-ingestelde accounts kunnen niet worden opgehaald</translation>
    </message>
    <message>
        <location filename="../Dekko/server/servicehandler.cpp" line="1733"/>
        <location filename="../upstream/qmf/tools/messageserver/servicehandler.cpp" line="1729"/>
        <source>Unable to retrieve messages for unconfigured account</source>
        <translation>Berichten van niet-ingestelde accounts kunnen niet worden opgehaald</translation>
    </message>
    <message>
        <location filename="../Dekko/server/servicehandler.cpp" line="1777"/>
        <location filename="../upstream/qmf/tools/messageserver/servicehandler.cpp" line="1773"/>
        <source>Unable to retrieve message part for unconfigured account</source>
        <translation>Berichtdelen van niet-ingestelde accounts kunnen niet worden opgehaald</translation>
    </message>
    <message>
        <location filename="../Dekko/server/servicehandler.cpp" line="1815"/>
        <location filename="../upstream/qmf/tools/messageserver/servicehandler.cpp" line="1811"/>
        <source>Unable to retrieve message range for unconfigured account</source>
        <translation>Het berichtbereik van niet-ingestelde accounts kan niet worden opgehaald</translation>
    </message>
    <message>
        <location filename="../Dekko/server/servicehandler.cpp" line="1854"/>
        <location filename="../upstream/qmf/tools/messageserver/servicehandler.cpp" line="1850"/>
        <source>Unable to retrieve message part range for unconfigured account</source>
        <translation>Her berichtdeelbereik van niet-ingestelde accounts kan niet worden opgehaald</translation>
    </message>
    <message>
        <location filename="../Dekko/server/servicehandler.cpp" line="1893"/>
        <location filename="../upstream/qmf/tools/messageserver/servicehandler.cpp" line="1889"/>
        <source>Unable to retrieve all messages for unconfigured account</source>
        <translation>Alle berichten van niet-ingestelde accounts kunnen niet worden opgehaald</translation>
    </message>
    <message>
        <location filename="../Dekko/server/servicehandler.cpp" line="1929"/>
        <location filename="../upstream/qmf/tools/messageserver/servicehandler.cpp" line="1925"/>
        <source>Unable to export updates for unconfigured account</source>
        <translation>Updates van niet-ingestelde accounts kunnen niet worden geëxporteerd</translation>
    </message>
    <message>
        <location filename="../Dekko/server/servicehandler.cpp" line="1962"/>
        <location filename="../upstream/qmf/tools/messageserver/servicehandler.cpp" line="1958"/>
        <source>Unable to synchronize unconfigured account</source>
        <translation>Niet-ingestelde accounts kunnen niet worden gesynchroniseerd</translation>
    </message>
    <message>
        <location filename="../Dekko/server/servicehandler.cpp" line="2006"/>
        <location filename="../upstream/qmf/tools/messageserver/servicehandler.cpp" line="2002"/>
        <source>Unable to delete messages for unconfigured account</source>
        <translation>Berichten kunnen niet worden verwijderd van niet-ingestelde accounts</translation>
    </message>
    <message>
        <location filename="../Dekko/server/servicehandler.cpp" line="2008"/>
        <location filename="../upstream/qmf/tools/messageserver/servicehandler.cpp" line="2004"/>
        <source>Deleting messages</source>
        <translation>Bezig met verwijderen…</translation>
    </message>
    <message>
        <location filename="../Dekko/server/servicehandler.cpp" line="2026"/>
        <location filename="../upstream/qmf/tools/messageserver/servicehandler.cpp" line="2022"/>
        <source>Unable to discard messages</source>
        <translation>De berichten kunnen niet worden afgebroken</translation>
    </message>
    <message>
        <location filename="../Dekko/server/servicehandler.cpp" line="2075"/>
        <location filename="../Dekko/server/servicehandler.cpp" line="2096"/>
        <location filename="../upstream/qmf/tools/messageserver/servicehandler.cpp" line="2071"/>
        <location filename="../upstream/qmf/tools/messageserver/servicehandler.cpp" line="2092"/>
        <source>Unable to copy messages to unconfigured account</source>
        <translation>Berichten van niet-ingestelde accounts kunnen niet worden gekopieerd</translation>
    </message>
    <message>
        <location filename="../Dekko/server/servicehandler.cpp" line="2077"/>
        <location filename="../upstream/qmf/tools/messageserver/servicehandler.cpp" line="2073"/>
        <source>Unable to copy messages to multiple destination accounts!</source>
        <translation>Berichten kunnen niet worden gekopieerd naar meerdere bestemmingen!</translation>
    </message>
    <message>
        <location filename="../Dekko/server/servicehandler.cpp" line="2144"/>
        <location filename="../upstream/qmf/tools/messageserver/servicehandler.cpp" line="2140"/>
        <source>Unable to copy messages for account</source>
        <translation>Berichten van dit account kunnen niet worden gekopieerd</translation>
    </message>
    <message>
        <location filename="../Dekko/server/servicehandler.cpp" line="2162"/>
        <location filename="../upstream/qmf/tools/messageserver/servicehandler.cpp" line="2158"/>
        <source>Unable to move messages for unconfigured account</source>
        <translation>Berichten van niet-ingestelde accounts kunnen niet worden verplaatst</translation>
    </message>
    <message>
        <location filename="../Dekko/server/servicehandler.cpp" line="2203"/>
        <location filename="../upstream/qmf/tools/messageserver/servicehandler.cpp" line="2199"/>
        <source>Unable to flag messages for unconfigured account</source>
        <translation>Berichten van niet-ingestelde accounts kunnen niet worden gelabeld</translation>
    </message>
    <message>
        <location filename="../Dekko/server/servicehandler.cpp" line="2216"/>
        <location filename="../upstream/qmf/tools/messageserver/servicehandler.cpp" line="2212"/>
        <source>Unable to async add messages</source>
        <translation>Berichttoevoegingen kunnen niet worden gesynchroniseerd</translation>
    </message>
    <message>
        <location filename="../Dekko/server/servicehandler.cpp" line="2218"/>
        <location filename="../Dekko/server/servicehandler.cpp" line="2305"/>
        <location filename="../Dekko/server/servicehandler.cpp" line="2365"/>
        <location filename="../upstream/qmf/tools/messageserver/servicehandler.cpp" line="2214"/>
        <location filename="../upstream/qmf/tools/messageserver/servicehandler.cpp" line="2301"/>
        <location filename="../upstream/qmf/tools/messageserver/servicehandler.cpp" line="2361"/>
        <source>Unable to async update messages</source>
        <translation>Berichtupdates kunnen niet worden gesynchroniseerd</translation>
    </message>
    <message>
        <location filename="../Dekko/server/servicehandler.cpp" line="2275"/>
        <location filename="../upstream/qmf/tools/messageserver/servicehandler.cpp" line="2271"/>
        <source>Unable to async add messages, inconsistent contentscheme</source>
        <translation>Berichttoevoegingen kunnen niet worden gesynchroniseerd: inconsistent inhoudsschema</translation>
    </message>
    <message>
        <location filename="../Dekko/server/servicehandler.cpp" line="2327"/>
        <location filename="../upstream/qmf/tools/messageserver/servicehandler.cpp" line="2323"/>
        <source>Unable to async update messages, inconsistent contentscheme</source>
        <translation>Berichttoevoegingen kunnen niet worden bijgewerkt: inconsistent inhoudsschema</translation>
    </message>
    <message>
        <location filename="../Dekko/server/servicehandler.cpp" line="2412"/>
        <location filename="../upstream/qmf/tools/messageserver/servicehandler.cpp" line="2408"/>
        <source>Could not delete messages</source>
        <translation>De berichten kunnen niet worden verwijderd</translation>
    </message>
    <message>
        <location filename="../Dekko/server/servicehandler.cpp" line="2493"/>
        <location filename="../upstream/qmf/tools/messageserver/servicehandler.cpp" line="2489"/>
        <source>Unable to create folder for invalid account</source>
        <translation>Er kunnen geen mappen worden aangemaakt op ongeldige accounts</translation>
    </message>
    <message>
        <location filename="../Dekko/server/servicehandler.cpp" line="2530"/>
        <location filename="../upstream/qmf/tools/messageserver/servicehandler.cpp" line="2526"/>
        <source>Unable to rename invalid folder</source>
        <translation>Ongeldige mappen kunnen niet van naam worden gewijzigd</translation>
    </message>
    <message>
        <location filename="../Dekko/server/servicehandler.cpp" line="2566"/>
        <location filename="../upstream/qmf/tools/messageserver/servicehandler.cpp" line="2562"/>
        <source>Unable to delete invalid folder</source>
        <translation>Ongeldige mappen kunnen niet worden verwijderd</translation>
    </message>
    <message>
        <location filename="../Dekko/server/servicehandler.cpp" line="2612"/>
        <location filename="../upstream/qmf/tools/messageserver/servicehandler.cpp" line="2608"/>
        <source>Unable to search messages for unconfigured account</source>
        <translation>Er kunnen geen berichten worden gezocht op niet-ingestelde accounts</translation>
    </message>
    <message>
        <location filename="../Dekko/server/servicehandler.cpp" line="2926"/>
        <location filename="../upstream/qmf/tools/messageserver/servicehandler.cpp" line="2922"/>
        <source>Unable to forward protocol-specific request for unconfigured account</source>
        <translation>Er kunnen geen specifieke doorstuurverzoeken worden gedaan op niet-ingestelde accounts</translation>
    </message>
    <message>
        <location filename="../Dekko/server/servicehandler.cpp" line="3174"/>
        <location filename="../upstream/qmf/tools/messageserver/servicehandler.cpp" line="3170"/>
        <source>Failed to perform requested action!</source>
        <translation>De actie kan niet worden uitgevoerd!</translation>
    </message>
</context>
<context>
    <name>Settings</name>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/settings/Settings.qml" line="11"/>
        <source>Settings</source>
        <translation>Instellingen</translation>
    </message>
</context>
<context>
    <name>SettingsWindow</name>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/settings/SettingsWindow.qml" line="26"/>
        <source>Dekko Settings</source>
        <translation>Dekko-instellingen</translation>
    </message>
</context>
<context>
    <name>SettingsWorker</name>
    <message>
        <location filename="../Dekko/workers/SettingsWorker.qml" line="103"/>
        <source>Account saved</source>
        <translation>Het account is opgeslagen</translation>
    </message>
</context>
<context>
    <name>SetupWizardWindow</name>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/setupwizard/SetupWizardWindow.qml" line="20"/>
        <source>Mail Setup Wizard</source>
        <translation>Instelhulp</translation>
    </message>
</context>
<context>
    <name>SigEntry</name>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/smtp/smtpsettings.cpp" line="79"/>
        <source>Signature</source>
        <translation>Handtekening</translation>
    </message>
</context>
<context>
    <name>SmartFolderDelegate</name>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/delegates/SmartFolderDelegate.qml" line="155"/>
        <source>Inbox (%1)</source>
        <translation>Postvak in (%1)</translation>
    </message>
</context>
<context>
    <name>SmartFolderSet</name>
    <message>
        <location filename="../Dekko/backend/mail/MessageSet.cpp" line="332"/>
        <source>Today, %1</source>
        <translation>Vandaag (%1)</translation>
    </message>
</context>
<context>
    <name>SmtpAccountConfiguration</name>
    <message>
        <location filename="../Dekko/backend/accounts/AccountConfiguration.cpp" line="459"/>
        <source>Sent with Dekko</source>
        <translation>Verstuurd met Dekko</translation>
    </message>
</context>
<context>
    <name>SmtpClient</name>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/smtp/smtpclient.cpp" line="236"/>
        <source>Cannot send message; transport in use</source>
        <translation>Het bericht kan niet worden verstuurd: overdracht bezig</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/smtp/smtpclient.cpp" line="242"/>
        <source>Cannot send message without account configuration</source>
        <translation>Het bericht kan niet worden verstuurd: accountinstellingen ontbreken</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/smtp/smtpclient.cpp" line="249"/>
        <source>Cannot send message without SMTP server configuration</source>
        <translation>Het bericht kan niet worden verstuurd: smtp-serverinstellingen ontbreken</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/smtp/smtpclient.cpp" line="353"/>
        <source>Connected</source>
        <translation>Verbonden</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/smtp/smtpclient.cpp" line="863"/>
        <source>Sending: %1</source>
        <translation>Bezig met versturen van %1…</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/smtp/smtpclient.cpp" line="929"/>
        <source>Received response 354 while sending.</source>
        <translation>Foutcode 354 is ontvangen tijdens het versturen.</translation>
    </message>
    <message numerus="yes">
        <location filename="../upstream/qmf/plugins/messageservices/smtp/smtpclient.cpp" line="1060"/>
        <source>Sent %n messages</source>
        <translation>
            <numerusform>%n bericht verstuurd</numerusform>
            <numerusform>%n berichten verstuurd</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/smtp/smtpclient.cpp" line="1221"/>
        <source>Have not received any greeting from SMTP server, probably configuration error</source>
        <translation>Er is geen antwoord van de smtp-server ontvangen, waarschijnlijk door een foutieve instelling</translation>
    </message>
</context>
<context>
    <name>SmtpSettings</name>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/smtp/smtpsettings.ui" line="14"/>
        <source>Form</source>
        <translation>Formulier</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/smtp/smtpsettings.ui" line="56"/>
        <source>From</source>
        <translation>Van</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/smtp/smtpsettings.ui" line="82"/>
        <source>Email</source>
        <translation>E-mailadres</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/smtp/smtpsettings.ui" line="108"/>
        <source>Server</source>
        <translation>Server</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/smtp/smtpsettings.ui" line="134"/>
        <source>Port</source>
        <translation>Poort</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/smtp/smtpsettings.ui" line="160"/>
        <source>Encryption</source>
        <translation>Versleuteling</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/smtp/smtpsettings.ui" line="177"/>
        <location filename="../upstream/qmf/plugins/messageservices/smtp/smtpsettings.ui" line="218"/>
        <source>None</source>
        <translation>Geen</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/smtp/smtpsettings.ui" line="182"/>
        <source>SSL</source>
        <translation>Ssl</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/smtp/smtpsettings.ui" line="187"/>
        <source>TLS</source>
        <translation>Tls</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/smtp/smtpsettings.ui" line="201"/>
        <source>Authentication</source>
        <translation>Verificatie</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/smtp/smtpsettings.ui" line="223"/>
        <source>Login</source>
        <translation>Login</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/smtp/smtpsettings.ui" line="228"/>
        <source>Plain</source>
        <translation>Plain</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/smtp/smtpsettings.ui" line="233"/>
        <source>Cram MD5</source>
        <translation>Cram-md5</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/smtp/smtpsettings.ui" line="247"/>
        <source>Username</source>
        <translation>Gebruikersnaam</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/smtp/smtpsettings.ui" line="276"/>
        <source>Password</source>
        <translation>Wachtwoord</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/smtp/smtpsettings.ui" line="308"/>
        <source>Signature</source>
        <translation>Handtekening</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/smtp/smtpsettings.ui" line="324"/>
        <source>Set...</source>
        <translation>Instellen…</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/smtp/smtpsettings.ui" line="337"/>
        <source>Default sending account</source>
        <translation>Standaard verzendaccount</translation>
    </message>
</context>
<context>
    <name>SubjectField</name>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/composer/SubjectField.qml" line="56"/>
        <source>Subject:</source>
        <translation>Onderwerp:</translation>
    </message>
</context>
<context>
    <name>SyncState</name>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/setupwizard/states/SyncState.qml" line="35"/>
        <source>Synchronizing account.</source>
        <translation>Bezig met synchroniseren…</translation>
    </message>
</context>
<context>
    <name>TitledTextField</name>
    <message>
        <location filename="../plugins/ubuntu-plugin/imports/components/TitledTextField.qml" line="62"/>
        <source> (Required)</source>
        <translation> (vereist)</translation>
    </message>
</context>
<context>
    <name>UserInputUI</name>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/setupwizard/components/UserInputUI.qml" line="36"/>
        <source>Name</source>
        <translation>Naam</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/setupwizard/components/UserInputUI.qml" line="37"/>
        <source>Full name</source>
        <translation>Volledige naam</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/setupwizard/components/UserInputUI.qml" line="44"/>
        <source>Description</source>
        <translation>Omschrijving</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/setupwizard/components/UserInputUI.qml" line="45"/>
        <source>E.g Home, Work...</source>
        <translation>Bijv. Thuis, Werk, …</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/setupwizard/components/UserInputUI.qml" line="52"/>
        <source>Email address</source>
        <translation>E-mailadres</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/setupwizard/components/UserInputUI.qml" line="54"/>
        <source>email@example.org</source>
        <translation>e-mailadres@voorbeeld.nl</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/setupwizard/components/UserInputUI.qml" line="60"/>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/setupwizard/components/UserInputUI.qml" line="63"/>
        <source>Password</source>
        <translation>Wachtwoord</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/setupwizard/components/UserInputUI.qml" line="69"/>
        <source>Show password</source>
        <translation>Wachtwoord tonen</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/setupwizard/components/UserInputUI.qml" line="79"/>
        <source>Cancel</source>
        <translation>Annuleren</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/setupwizard/components/UserInputUI.qml" line="83"/>
        <source>Next</source>
        <translation>Volgende</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/setupwizard/components/UserInputUI.qml" line="125"/>
        <source>Password empty</source>
        <translation>Er is geen wachtwoord opgegeven</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/setupwizard/components/UserInputUI.qml" line="125"/>
        <source>Would you like to continue?</source>
        <translation>Wil je doorgaan?</translation>
    </message>
</context>
<context>
    <name>ValidationState</name>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/setupwizard/states/ValidationState.qml" line="36"/>
        <source>Validating credentials.</source>
        <translation>Bezig met controleren van inloggegevens…</translation>
    </message>
</context>
<context>
    <name>VersionDialog</name>
    <message>
        <location filename="../plugins/ubuntu-plugin/imports/dialogs/VersionDialog.qml" line="26"/>
        <source>Version</source>
        <translation>Versie</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/imports/dialogs/VersionDialog.qml" line="30"/>
        <source>Close</source>
        <translation>Sluiten</translation>
    </message>
</context>
<context>
    <name>ViewStore</name>
    <message>
        <location filename="../Dekko/stores/Views/ViewStore.qml" line="70"/>
        <source>Messages sent</source>
        <translation>De berichten zijn verstuurd</translation>
    </message>
    <message>
        <location filename="../Dekko/stores/Views/ViewStore.qml" line="72"/>
        <source>Message sent</source>
        <translation>Het bericht is verstuurd</translation>
    </message>
    <message>
        <location filename="../Dekko/stores/Views/ViewStore.qml" line="75"/>
        <source>Message sending failed</source>
        <translation>Het bericht kan niet worden verstuurd</translation>
    </message>
</context>
</TS>
