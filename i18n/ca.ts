<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ca">
<context>
    <name>AccountSettingsList</name>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/settings/AccountSettingsList.qml" line="26"/>
        <source>Account Settings: %1</source>
        <translation>Paràmetres del compte: %1</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/settings/AccountSettingsList.qml" line="56"/>
        <source>Details</source>
        <translation>Detalls</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/settings/AccountSettingsList.qml" line="60"/>
        <source>Incoming Server</source>
        <translation>Servidor entrant</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/settings/AccountSettingsList.qml" line="64"/>
        <source>Outgoing Server</source>
        <translation>Servidor sortint</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/settings/AccountSettingsList.qml" line="68"/>
        <source>Copies and Folders</source>
        <translation>Còpies i carpetes</translation>
    </message>
</context>
<context>
    <name>AccountSetup</name>
    <message>
        <location filename="../Dekko/stores/Accounts/AccountSetup.qml" line="220"/>
        <source>Invalid email address</source>
        <translation>Adreça de correu invàlida</translation>
    </message>
    <message>
        <location filename="../Dekko/stores/Accounts/AccountSetup.qml" line="536"/>
        <source>Sent with Dekko</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>AccountsWorker</name>
    <message>
        <location filename="../Dekko/workers/AccountsWorker.qml" line="36"/>
        <location filename="../Dekko/workers/AccountsWorker.qml" line="90"/>
        <source>Account removal failed</source>
        <translation>Ha fallat l&apos;eliminació del compte</translation>
    </message>
    <message>
        <location filename="../Dekko/workers/AccountsWorker.qml" line="54"/>
        <source>Remove %1</source>
        <translation>Elimina %1</translation>
    </message>
    <message>
        <location filename="../Dekko/workers/AccountsWorker.qml" line="54"/>
        <source>Are you sure you wish to remove this account?</source>
        <translation>Segur que voleu eliminar aquest compte?</translation>
    </message>
    <message>
        <location filename="../Dekko/workers/AccountsWorker.qml" line="87"/>
        <source>Account removed</source>
        <translation>Compte eliminat</translation>
    </message>
</context>
<context>
    <name>AddAnotherUI</name>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/setupwizard/components/AddAnotherUI.qml" line="31"/>
        <source>Success</source>
        <translation>Èxit</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/setupwizard/components/AddAnotherUI.qml" line="90"/>
        <source>New account created.</source>
        <translation>Nou compte creat.</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/setupwizard/components/AddAnotherUI.qml" line="99"/>
        <source>Continue</source>
        <translation>Continua</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/setupwizard/components/AddAnotherUI.qml" line="115"/>
        <source>Add another</source>
        <translation>Afegeix un altre</translation>
    </message>
</context>
<context>
    <name>AddressBookList</name>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/contacts/AddressBookList.qml" line="12"/>
        <source>Addressbooks</source>
        <translation>Llibretes d&apos;adreces</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/contacts/AddressBookList.qml" line="65"/>
        <source>Add Collection</source>
        <translation>Afegeix una col·lecció</translation>
    </message>
</context>
<context>
    <name>AttachmentPanel</name>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/components/AttachmentPanel.qml" line="85"/>
        <source>Attachments</source>
        <translation>Fitxers adjunts</translation>
    </message>
</context>
<context>
    <name>AttachmentPopover</name>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/popovers/AttachmentPopover.qml" line="46"/>
        <source>Delete</source>
        <translation>Suprimeix</translation>
    </message>
</context>
<context>
    <name>AuthenticationSelector</name>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/components/AuthenticationSelector.qml" line="45"/>
        <source>Authentication</source>
        <translation>Autenticació</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/components/AuthenticationSelector.qml" line="60"/>
        <source>PLAIN</source>
        <translation>SIMPLE</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/components/AuthenticationSelector.qml" line="61"/>
        <source>LOGIN</source>
        <translation>INICI DE SESSIÓ</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/components/AuthenticationSelector.qml" line="62"/>
        <source>CRAM-MD5</source>
        <translation>CRAM-MD5</translation>
    </message>
</context>
<context>
    <name>AutoConfigState</name>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/setupwizard/states/AutoConfigState.qml" line="35"/>
        <source>Searching for configuration.</source>
        <translation>S&apos;està cercant la configuració.</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/setupwizard/states/AutoConfigState.qml" line="77"/>
        <source>IMAP server found</source>
        <translation>S&apos;ha trobat un servidor IMAP</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/setupwizard/states/AutoConfigState.qml" line="78"/>
        <source>A IMAP server configuration was found for your domain.

Would you like to use this instead?</source>
        <translation>S&apos;ha trobat una configuració del servidor IMAP al domini.

Voleu fer servir aquesta en el seu lloc?</translation>
    </message>
</context>
<context>
    <name>BottomEdgeComposer</name>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/composer/BottomEdgeComposer.qml" line="38"/>
        <source>New message</source>
        <translation>Missatge nou</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/composer/BottomEdgeComposer.qml" line="66"/>
        <source>Attachments</source>
        <translation>Fitxers adjunts</translation>
    </message>
</context>
<context>
    <name>CacheSettings</name>
    <message>
        <location filename="../plugins/cache-manager-plugin/qml/CacheSettings.qml" line="7"/>
        <source>Manage cache</source>
        <translation>Gestiona la memòria cau</translation>
    </message>
</context>
<context>
    <name>CacheSettingsPage</name>
    <message>
        <location filename="../plugins/cache-manager-plugin/qml/CacheSettingsPage.qml" line="26"/>
        <source>Manage cache: %1</source>
        <translation>Gestiona la memòria cau: %1</translation>
    </message>
    <message>
        <location filename="../plugins/cache-manager-plugin/qml/CacheSettingsPage.qml" line="35"/>
        <source>One Week</source>
        <translation>Una setmana</translation>
    </message>
    <message>
        <location filename="../plugins/cache-manager-plugin/qml/CacheSettingsPage.qml" line="36"/>
        <source>Fortnight</source>
        <translation>Quinze dies</translation>
    </message>
    <message>
        <location filename="../plugins/cache-manager-plugin/qml/CacheSettingsPage.qml" line="37"/>
        <source>One Month</source>
        <translation>Un mes</translation>
    </message>
    <message>
        <location filename="../plugins/cache-manager-plugin/qml/CacheSettingsPage.qml" line="38"/>
        <source>Three Months</source>
        <translation>Tres mesos</translation>
    </message>
    <message>
        <location filename="../plugins/cache-manager-plugin/qml/CacheSettingsPage.qml" line="39"/>
        <source>Six Months</source>
        <translation>Sis mesos</translation>
    </message>
    <message>
        <location filename="../plugins/cache-manager-plugin/qml/CacheSettingsPage.qml" line="45"/>
        <source>Clear messages from cache</source>
        <translation>Elimina els missatges de la memòria cau</translation>
    </message>
    <message>
        <location filename="../plugins/cache-manager-plugin/qml/CacheSettingsPage.qml" line="46"/>
        <source>This will clear messages older than the given period</source>
        <translation>Això eliminarà els missatges més antics del període donat</translation>
    </message>
    <message>
        <location filename="../plugins/cache-manager-plugin/qml/CacheSettingsPage.qml" line="75"/>
        <source>Clear now</source>
        <translation>Elimina&apos;ls ara</translation>
    </message>
</context>
<context>
    <name>CleanMessagePage</name>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/messageview/CleanMessagePage.qml" line="29"/>
        <source>Message</source>
        <translation>Missatge</translation>
    </message>
</context>
<context>
    <name>ClientService</name>
    <message>
        <location filename="../Dekko/backend/mail/service/ClientService.cpp" line="78"/>
        <location filename="../Dekko/backend/mail/service/ClientService.cpp" line="82"/>
        <source>messages</source>
        <translation>missatges</translation>
    </message>
    <message>
        <location filename="../Dekko/backend/mail/service/ClientService.cpp" line="78"/>
        <location filename="../Dekko/backend/mail/service/ClientService.cpp" line="82"/>
        <source>message</source>
        <translation>missatge</translation>
    </message>
</context>
<context>
    <name>ComposeWindow</name>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/composer/ComposeWindow.qml" line="24"/>
        <source>Dekko Composer</source>
        <translation>Compositor Dekko</translation>
    </message>
</context>
<context>
    <name>Composer</name>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/composer/Composer.qml" line="56"/>
        <source>Attach</source>
        <translation>Adjunta</translation>
    </message>
</context>
<context>
    <name>ComposerStore</name>
    <message>
        <location filename="../Dekko/stores/Composer/ComposerStore.qml" line="86"/>
        <source>Please select a sender. Otherwise no drafts will be saved automatically. Consider setting a default identity.</source>
        <translation>Seleccioneu un remitent o no es podran desar els esborranys automàticament. Considerau definir un remitent per defecte.</translation>
    </message>
    <message>
        <location filename="../Dekko/stores/Composer/ComposerStore.qml" line="90"/>
        <source>Somethings fishy with your Identity. Please select a valid sender.</source>
        <translation>Quelcom és estran amb la vostra identitat. Seleccioneu-ne una de vàlida.</translation>
    </message>
    <message>
        <location filename="../Dekko/stores/Composer/ComposerStore.qml" line="99"/>
        <source>Message queued.</source>
        <translation>Missatge en cua.</translation>
    </message>
    <message>
        <location filename="../Dekko/stores/Composer/ComposerStore.qml" line="103"/>
        <source>Draft saved.</source>
        <translation>S&apos;ha desat l&apos;esborrany.</translation>
    </message>
</context>
<context>
    <name>ComposerStoreActions</name>
    <message>
        <location filename="../Dekko/stores/Composer/ComposerStoreActions.qml" line="26"/>
        <source>Send</source>
        <translation>Envia</translation>
    </message>
    <message>
        <location filename="../Dekko/stores/Composer/ComposerStoreActions.qml" line="33"/>
        <source>Save draft</source>
        <translation>Desa esborrany</translation>
    </message>
    <message>
        <location filename="../Dekko/stores/Composer/ComposerStoreActions.qml" line="40"/>
        <source>Discard</source>
        <translation>Descarta</translation>
    </message>
    <message>
        <location filename="../Dekko/stores/Composer/ComposerStoreActions.qml" line="47"/>
        <source>Attach</source>
        <translation>Adjunta</translation>
    </message>
</context>
<context>
    <name>ComposerWorker</name>
    <message>
        <location filename="../Dekko/workers/ComposerWorker.qml" line="120"/>
        <source>Discard message</source>
        <translation>Descarta el missatge</translation>
    </message>
    <message>
        <location filename="../Dekko/workers/ComposerWorker.qml" line="120"/>
        <source>Are you sure you want to discard this message?</source>
        <translation>Segur que voleu descartar aquest missatge?</translation>
    </message>
</context>
<context>
    <name>ConfirmationDialog</name>
    <message>
        <location filename="../plugins/ubuntu-plugin/imports/dialogs/ConfirmationDialog.qml" line="51"/>
        <source>Cancel</source>
        <translation>Cancel·la</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/imports/dialogs/ConfirmationDialog.qml" line="63"/>
        <source>Confirm</source>
        <translation>Confirma</translation>
    </message>
</context>
<context>
    <name>ContactFilterView</name>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/views/ContactFilterView.qml" line="113"/>
        <source>Add contact</source>
        <translation>Afegeix un contacte</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/views/ContactFilterView.qml" line="125"/>
        <source>Send message</source>
        <translation>Envia un missatge</translation>
    </message>
</context>
<context>
    <name>ContactListPage</name>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/contacts/ContactListPage.qml" line="11"/>
        <source>Address book</source>
        <translation>Llibreta d&apos;adreces</translation>
    </message>
</context>
<context>
    <name>ContactView</name>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/contacts/ContactView.qml" line="11"/>
        <source>Contact</source>
        <translation>Contacte</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/contacts/ContactView.qml" line="61"/>
        <source>Email</source>
        <translation>Correu electrònic</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/contacts/ContactView.qml" line="78"/>
        <source>Phone</source>
        <translation>Telèfon</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/contacts/ContactView.qml" line="92"/>
        <source>Address</source>
        <translation>Adreça</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/contacts/ContactView.qml" line="96"/>
        <source>Street</source>
        <translation>Carrer</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/contacts/ContactView.qml" line="102"/>
        <source>City</source>
        <translation>Ciutat</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/contacts/ContactView.qml" line="108"/>
        <source>Zip</source>
        <translation>Codi postal</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/contacts/ContactView.qml" line="114"/>
        <source>Country</source>
        <translation>País</translation>
    </message>
</context>
<context>
    <name>ContactsListView</name>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/contacts/ContactsListView.qml" line="36"/>
        <source>Search</source>
        <translation>Cerca</translation>
    </message>
</context>
<context>
    <name>ContentBlockedNotice</name>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/webview/ContentBlockedNotice.qml" line="46"/>
        <source>Remote content blocked</source>
        <translation>S&apos;ha bloquejat el contingut remot</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/webview/ContentBlockedNotice.qml" line="62"/>
        <source>Allow</source>
        <translation>Permet</translation>
    </message>
</context>
<context>
    <name>ContributorsPage</name>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/views/ContributorsPage.qml" line="25"/>
        <source>Contributors</source>
        <translation>Col·laboradors</translation>
    </message>
</context>
<context>
    <name>CopyFoldersGroup</name>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/settings/CopyFoldersGroup.qml" line="31"/>
        <source>Copies and Folders: %1</source>
        <translation>Còpies i carpetes: %1</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/settings/CopyFoldersGroup.qml" line="120"/>
        <source>Detect standard folders</source>
        <translation>Detecta les carpetes estàndard</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/settings/CopyFoldersGroup.qml" line="127"/>
        <source>Detect</source>
        <translation>Detecta</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/settings/CopyFoldersGroup.qml" line="147"/>
        <source>Base folder</source>
        <translation>Carpeta base</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/settings/CopyFoldersGroup.qml" line="149"/>
        <source>Leave empty if you are unsure</source>
        <translation>Deixeu en blanc si no teniu seguretat</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/settings/CopyFoldersGroup.qml" line="164"/>
        <source>Inbox folder</source>
        <translation>Carpeta d&apos;entrada</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/settings/CopyFoldersGroup.qml" line="180"/>
        <source>Drafts folder</source>
        <translation>Carpeta d&apos;esborranys</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/settings/CopyFoldersGroup.qml" line="196"/>
        <source>Spam folder</source>
        <translation>Carpeta de correu brossa</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/settings/CopyFoldersGroup.qml" line="212"/>
        <source>Sent folder</source>
        <translation>Carpeta de lliurats</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/settings/CopyFoldersGroup.qml" line="228"/>
        <source>Outbox folder</source>
        <translation>Carpeta de sortida</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/settings/CopyFoldersGroup.qml" line="244"/>
        <source>Trash folder</source>
        <translation>Paperera</translation>
    </message>
</context>
<context>
    <name>DefaultMessagePage</name>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/messageview/DefaultMessagePage.qml" line="183"/>
        <source>From:</source>
        <translation>De:</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/messageview/DefaultMessagePage.qml" line="236"/>
        <source>To:</source>
        <translation>Per a:</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/messageview/DefaultMessagePage.qml" line="241"/>
        <source>Cc:</source>
        <translation>Cc:</translation>
    </message>
</context>
<context>
    <name>DefaultPlugin</name>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/extensions/addressbook/DefaultPlugin.qml" line="11"/>
        <source>Internal</source>
        <translation>Intern</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/extensions/addressbook/DefaultPlugin.qml" line="106"/>
        <source>Default</source>
        <translation>Predeterminat</translation>
    </message>
</context>
<context>
    <name>DekkoHeader</name>
    <message>
        <location filename="../plugins/ubuntu-plugin/imports/components/DekkoHeader.qml" line="206"/>
        <source>Enter search...</source>
        <translation>Escriviu la cerca...</translation>
    </message>
</context>
<context>
    <name>DekkoWebView</name>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/webview/DekkoWebView.qml" line="303"/>
        <source>Open in browser?</source>
        <translation>Voleu obrir-ho al navegador?</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/webview/DekkoWebView.qml" line="304"/>
        <source>Confirm to open %1 in web browser</source>
        <translation>Confirmeu que voleu obrir %1 al navegador</translation>
    </message>
</context>
<context>
    <name>DetailList</name>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/messageview/DetailList.qml" line="48"/>
        <source>Details</source>
        <translation>Detalls</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/messageview/DetailList.qml" line="53"/>
        <source>To:</source>
        <translation>Per a:</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/messageview/DetailList.qml" line="59"/>
        <source>Cc:</source>
        <translation>Cc:</translation>
    </message>
</context>
<context>
    <name>DetailsGroup</name>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/settings/DetailsGroup.qml" line="27"/>
        <source>Details: %1</source>
        <translation>Detalls: %1</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/settings/DetailsGroup.qml" line="48"/>
        <source>Account name</source>
        <translation>Nom del compte</translation>
    </message>
</context>
<context>
    <name>DialogWorker</name>
    <message>
        <location filename="../Dekko/workers/DialogWorker.qml" line="39"/>
        <source>Error</source>
        <translation>Error</translation>
    </message>
    <message>
        <location filename="../Dekko/workers/DialogWorker.qml" line="48"/>
        <source>Notice</source>
        <translation>Nota</translation>
    </message>
</context>
<context>
    <name>DisplaySettings</name>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/settings/DisplaySettings.qml" line="28"/>
        <source>Navigation menu</source>
        <translation>Menú de navegació</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/settings/DisplaySettings.qml" line="32"/>
        <source>Show smart folders</source>
        <translation>Mostra les carpetes intel·ligents</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/settings/DisplaySettings.qml" line="42"/>
        <source>Show favourite folders</source>
        <translation>Mostra les carpetes preferides</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/settings/DisplaySettings.qml" line="53"/>
        <source>Messages</source>
        <translation>Missatges</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/settings/DisplaySettings.qml" line="57"/>
        <source>Show avatars</source>
        <translation>Mostra els avatars</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/settings/DisplaySettings.qml" line="67"/>
        <source>Prefer plain text</source>
        <translation>Prefereix el text sense format</translation>
    </message>
</context>
<context>
    <name>DisplaySettingsPage</name>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/settings/DisplaySettingsPage.qml" line="21"/>
        <source>Display Settings</source>
        <translation>Mostra la configuració</translation>
    </message>
</context>
<context>
    <name>DisplaySettingsPopup</name>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/settings/DisplaySettingsPopup.qml" line="21"/>
        <source>Display Settings</source>
        <translation>Mostra la configuració</translation>
    </message>
</context>
<context>
    <name>EncryptionSelector</name>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/components/EncryptionSelector.qml" line="46"/>
        <source>Encryption</source>
        <translation>Xifratge</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/components/EncryptionSelector.qml" line="61"/>
        <source>No encryption</source>
        <translation>Sense xifratge</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/components/EncryptionSelector.qml" line="62"/>
        <source>Use encryption (STARTTLS)</source>
        <translation>Utilitza xifratge (STARTTLS)</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/components/EncryptionSelector.qml" line="63"/>
        <source>Force encryption (SSL/TLS)</source>
        <translation>Força el xifratge (SSL/TLS)</translation>
    </message>
</context>
<context>
    <name>ErrorsWorker</name>
    <message>
        <location filename="../Dekko/workers/ErrorsWorker.qml" line="78"/>
        <location filename="../Dekko/workers/ErrorsWorker.qml" line="82"/>
        <source>No connection</source>
        <translation>Sense connexió</translation>
    </message>
    <message>
        <location filename="../Dekko/workers/ErrorsWorker.qml" line="86"/>
        <source>Connection unavailable</source>
        <translation>Connexió no disponible</translation>
    </message>
    <message>
        <location filename="../Dekko/workers/ErrorsWorker.qml" line="102"/>
        <source>Action timed out</source>
        <translation>Acció caducada</translation>
    </message>
</context>
<context>
    <name>ExpandablePanel</name>
    <message>
        <location filename="../plugins/ubuntu-plugin/imports/components/ExpandablePanel.qml" line="63"/>
        <source>Attachments</source>
        <translation>Fitxers adjunts</translation>
    </message>
</context>
<context>
    <name>ExportUpdatesAction</name>
    <message>
        <location filename="../Dekko/backend/mail/service/ClientServiceAction.cpp" line="144"/>
        <source>Syncing changes for %1 account</source>
        <translation>Sincronitzant canvis pel compte %1</translation>
    </message>
</context>
<context>
    <name>FilePickerDialog</name>
    <message>
        <location filename="../plugins/ubuntu-plugin/imports/dialogs/FilePickerDialog.qml" line="23"/>
        <source>Add Attachment</source>
        <translation>Afegeix un adjunt</translation>
    </message>
</context>
<context>
    <name>FlagsAction</name>
    <message>
        <location filename="../Dekko/backend/mail/service/ClientServiceAction.cpp" line="166"/>
        <source>important</source>
        <translation>important</translation>
    </message>
    <message>
        <location filename="../Dekko/backend/mail/service/ClientServiceAction.cpp" line="169"/>
        <source>not important</source>
        <translation>no important</translation>
    </message>
    <message>
        <location filename="../Dekko/backend/mail/service/ClientServiceAction.cpp" line="178"/>
        <source>read</source>
        <translation>llegit</translation>
    </message>
    <message>
        <location filename="../Dekko/backend/mail/service/ClientServiceAction.cpp" line="181"/>
        <source>unread</source>
        <translation>no llegit</translation>
    </message>
    <message>
        <location filename="../Dekko/backend/mail/service/ClientServiceAction.cpp" line="190"/>
        <source>as todo</source>
        <translation>com a tasca</translation>
    </message>
    <message>
        <location filename="../Dekko/backend/mail/service/ClientServiceAction.cpp" line="193"/>
        <source>no longer todo</source>
        <translation>ja no és per fer</translation>
    </message>
    <message>
        <location filename="../Dekko/backend/mail/service/ClientServiceAction.cpp" line="198"/>
        <source>Marking %1 messages %2</source>
        <translation>Marcant %1 missatges %2</translation>
    </message>
</context>
<context>
    <name>Folder</name>
    <message>
        <location filename="../Dekko/backend/mail/Folder.cpp" line="103"/>
        <source>Drafts</source>
        <translation>Esborranys</translation>
    </message>
    <message>
        <location filename="../Dekko/backend/mail/Folder.cpp" line="105"/>
        <source>Inbox</source>
        <translation>Safata d&apos;entrada</translation>
    </message>
    <message>
        <location filename="../Dekko/backend/mail/Folder.cpp" line="107"/>
        <source>Spam</source>
        <translation>Correu brossa</translation>
    </message>
    <message>
        <location filename="../Dekko/backend/mail/Folder.cpp" line="109"/>
        <source>Outbox</source>
        <translation>Safata de sortida</translation>
    </message>
    <message>
        <location filename="../Dekko/backend/mail/Folder.cpp" line="111"/>
        <source>Sent</source>
        <translation>Enviat</translation>
    </message>
    <message>
        <location filename="../Dekko/backend/mail/Folder.cpp" line="113"/>
        <source>Trash</source>
        <translation>Paperera</translation>
    </message>
</context>
<context>
    <name>FolderListDelegate</name>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/delegates/FolderListDelegate.qml" line="53"/>
        <source>Un-favourite</source>
        <translation>Treu dels preferits</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/delegates/FolderListDelegate.qml" line="53"/>
        <source>Favourite</source>
        <translation>Afegeix als preferits</translation>
    </message>
</context>
<context>
    <name>FolderListView</name>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/views/FolderListView.qml" line="29"/>
        <source>Folders</source>
        <translation>Carpetes</translation>
    </message>
</context>
<context>
    <name>ForbiddenReply</name>
    <message>
        <location filename="../Dekko/backend/network/ForbiddenReply.cpp" line="24"/>
        <source>Remote content is banned</source>
        <translation>El contingut remot està blocat</translation>
    </message>
</context>
<context>
    <name>HtmlViewer</name>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/extensions/html-viewer/HtmlViewer.qml" line="11"/>
        <source>HTML Viewer</source>
        <translation>Visualitzador HTML</translation>
    </message>
</context>
<context>
    <name>IdentitiesListPage</name>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/settings/IdentitiesListPage.qml" line="15"/>
        <source>Identities</source>
        <translation>Identitats</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/settings/IdentitiesListPage.qml" line="107"/>
        <source> (Default)</source>
        <translation> (Predeterminat)</translation>
    </message>
</context>
<context>
    <name>IdentityInput</name>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/settings/IdentityInput.qml" line="55"/>
        <source>Default identity</source>
        <translation>Identitat predeterminada</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/settings/IdentityInput.qml" line="68"/>
        <source>Account</source>
        <translation>Compte</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/settings/IdentityInput.qml" line="106"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/settings/IdentityInput.qml" line="111"/>
        <source>Email Address</source>
        <translation>Adreça de correu electrònic</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/settings/IdentityInput.qml" line="116"/>
        <source>Reply-To</source>
        <translation>Respon a</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/settings/IdentityInput.qml" line="120"/>
        <source>Signature</source>
        <translation>Signatura</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/settings/IdentityInput.qml" line="138"/>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/settings/IdentityInput.qml" line="146"/>
        <source>New identity</source>
        <translation>Identitat nova</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/settings/IdentityInput.qml" line="170"/>
        <source>Edit identity</source>
        <translation>Edita la identitat</translation>
    </message>
</context>
<context>
    <name>IdleProtocol</name>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapclient.cpp" line="566"/>
        <source>Idle Error occurred</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ImagePickerScript</name>
    <message>
        <location filename="../upstream/quick-flux/quickflux/examples/photoalbum/scripts/ImagePickerScript.qml" line="12"/>
        <source>Pick Image</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ImagePreview</name>
    <message>
        <location filename="../upstream/quick-flux/quickflux/examples/photoalbum/views/ImagePreview.qml" line="31"/>
        <source>Cancel</source>
        <translation type="unfinished">Cancel·la</translation>
    </message>
    <message>
        <location filename="../upstream/quick-flux/quickflux/examples/photoalbum/views/ImagePreview.qml" line="39"/>
        <source>Confirm</source>
        <translation type="unfinished">Confirma</translation>
    </message>
</context>
<context>
    <name>ImageViewer</name>
    <message>
        <location filename="../upstream/quick-flux/quickflux/examples/photoalbum/views/ImageViewer.qml" line="35"/>
        <source>Pick Image</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ImapClient</name>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapclient.cpp" line="740"/>
        <source>Cannot open connection without IMAP server configuration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapclient.cpp" line="868"/>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapclient.cpp" line="959"/>
        <source>Checking capabilities</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapclient.cpp" line="886"/>
        <source>Starting TLS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapclient.cpp" line="900"/>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapclient.cpp" line="998"/>
        <source>Logging in idle connection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapclient.cpp" line="918"/>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapclient.cpp" line="934"/>
        <source>Logging in</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapclient.cpp" line="1611"/>
        <source>Unable to read fetched data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapclient.cpp" line="1618"/>
        <source>Unable to map fetched data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapclient.cpp" line="1625"/>
        <source>Unable to update part body</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapclient.cpp" line="1648"/>
        <source>Unable to handle dataFetched without context</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapclient.cpp" line="1685"/>
        <source>Cannot send message; socket in use</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapclient.cpp" line="1785"/>
        <source>Logging out</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ImapProtocol</name>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapprotocol.cpp" line="3346"/>
        <source>Connection failed</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ImapService</name>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapservice.cpp" line="1665"/>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapservice.cpp" line="1740"/>
        <source>Account disabled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapservice.cpp" line="1754"/>
        <source>Initiating push email</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ImapService::Source</name>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapservice.cpp" line="238"/>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapservice.cpp" line="313"/>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapservice.cpp" line="358"/>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapservice.cpp" line="409"/>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapservice.cpp" line="449"/>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapservice.cpp" line="493"/>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapservice.cpp" line="538"/>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapservice.cpp" line="592"/>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapservice.cpp" line="614"/>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapservice.cpp" line="704"/>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapservice.cpp" line="732"/>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapservice.cpp" line="766"/>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapservice.cpp" line="835"/>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapservice.cpp" line="1033"/>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapservice.cpp" line="1060"/>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapservice.cpp" line="1123"/>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapservice.cpp" line="1151"/>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapservice.cpp" line="1199"/>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapservice.cpp" line="1222"/>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapservice.cpp" line="1240"/>
        <source>Account disabled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapservice.cpp" line="243"/>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapservice.cpp" line="318"/>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapservice.cpp" line="543"/>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapservice.cpp" line="597"/>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapservice.cpp" line="619"/>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapservice.cpp" line="1038"/>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapservice.cpp" line="1065"/>
        <source>No account specified</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapservice.cpp" line="363"/>
        <source>No messages to retrieve</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapservice.cpp" line="414"/>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapservice.cpp" line="454"/>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapservice.cpp" line="498"/>
        <source>No message to retrieve</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapservice.cpp" line="418"/>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapservice.cpp" line="502"/>
        <source>No part specified</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapservice.cpp" line="422"/>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapservice.cpp" line="458"/>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapservice.cpp" line="506"/>
        <source>Invalid message specified</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapservice.cpp" line="463"/>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapservice.cpp" line="510"/>
        <source>No minimum specified</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapservice.cpp" line="660"/>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapservice.cpp" line="689"/>
        <source>Could not delete messages</source>
        <translation type="unfinished">No s&apos;han pogut esborrar els missatges</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapservice.cpp" line="737"/>
        <source>No messages to copy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapservice.cpp" line="741"/>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapservice.cpp" line="775"/>
        <source>Invalid destination folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapservice.cpp" line="771"/>
        <source>No messages to move</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapservice.cpp" line="840"/>
        <source>No messages to flag</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapservice.cpp" line="844"/>
        <source>No flags to be applied</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapservice.cpp" line="1044"/>
        <source>Cannot create empty named folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapservice.cpp" line="1082"/>
        <source>Drafts</source>
        <translation type="unfinished">Esborranys</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapservice.cpp" line="1085"/>
        <source>Sent</source>
        <translation type="unfinished">Enviat</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapservice.cpp" line="1088"/>
        <source>Junk</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapservice.cpp" line="1091"/>
        <source>Trash</source>
        <translation type="unfinished">Paperera</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapservice.cpp" line="1128"/>
        <source>Deleting invalid folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapservice.cpp" line="1156"/>
        <source>Cannot rename to an empty folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapservice.cpp" line="1160"/>
        <source>Cannot rename an invalid folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapservice.cpp" line="1205"/>
        <source>Empty search provided</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapservice.cpp" line="1245"/>
        <source>No messages to prepare</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapservice.cpp" line="1337"/>
        <source>Destination message failed to match source message</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ImapSettings</name>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapsettings.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapsettings.ui" line="59"/>
        <source>Username</source>
        <translation type="unfinished">Nom d&apos;usuari</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapsettings.ui" line="85"/>
        <source>Password</source>
        <translation type="unfinished">Contrasenya</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapsettings.ui" line="114"/>
        <source>Server</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapsettings.ui" line="140"/>
        <source>Port</source>
        <translation type="unfinished">Port</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapsettings.ui" line="166"/>
        <source>Encryption</source>
        <translation type="unfinished">Xifratge</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapsettings.ui" line="183"/>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapsettings.ui" line="224"/>
        <source>None</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapsettings.ui" line="188"/>
        <source>SSL</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapsettings.ui" line="193"/>
        <source>TLS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapsettings.ui" line="207"/>
        <source>Authentication</source>
        <translation type="unfinished">Autenticació</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapsettings.ui" line="229"/>
        <source>Login</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapsettings.ui" line="234"/>
        <source>Plain</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapsettings.ui" line="239"/>
        <source>Cram MD5</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapsettings.ui" line="253"/>
        <source>Remove deleted messages from server</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapsettings.ui" line="275"/>
        <source>Download limit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapsettings.ui" line="294"/>
        <source>K</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapsettings.ui" line="313"/>
        <source>Prefer HTML content over Plain</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapsettings.ui" line="332"/>
        <source>Interval</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapsettings.ui" line="348"/>
        <source>min</source>
        <comment>short for minutes</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapsettings.ui" line="382"/>
        <source>Disable when Roaming</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapsettings.ui" line="395"/>
        <source>Base folder</source>
        <translation type="unfinished">Carpeta base</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapsettings.ui" line="445"/>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapsettings.ui" line="513"/>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapsettings.ui" line="581"/>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapsettings.ui" line="649"/>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapsettings.ui" line="717"/>
        <source>...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapsettings.ui" line="460"/>
        <source>Drafts folder</source>
        <translation type="unfinished">Carpeta d&apos;esborranys</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapsettings.ui" line="528"/>
        <source>Sent folder</source>
        <translation type="unfinished">Carpeta de lliurats</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapsettings.ui" line="596"/>
        <source>Trash folder</source>
        <translation type="unfinished">Paperera</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapsettings.ui" line="664"/>
        <source>Junk folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapsettings.ui" line="738"/>
        <source>Push Enabled</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>IncomingServerGroup</name>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/settings/IncomingServerGroup.qml" line="29"/>
        <source>Incoming Server: %1</source>
        <translation>Servidor d&apos;entrada: %1</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/settings/IncomingServerGroup.qml" line="106"/>
        <source>Hostname</source>
        <translation>Nom del servidor</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/settings/IncomingServerGroup.qml" line="115"/>
        <source>Port</source>
        <translation>Port</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/settings/IncomingServerGroup.qml" line="124"/>
        <source>Username</source>
        <translation>Nom d&apos;usuari</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/settings/IncomingServerGroup.qml" line="133"/>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/settings/IncomingServerGroup.qml" line="136"/>
        <source>Password</source>
        <translation>Contrasenya</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/settings/IncomingServerGroup.qml" line="143"/>
        <source>Show password</source>
        <translation>Mostra la contrasenya</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/settings/IncomingServerGroup.qml" line="149"/>
        <source>Security settings</source>
        <translation>Configuració de la seguretat</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/settings/IncomingServerGroup.qml" line="184"/>
        <source>Allow untrusted certificates</source>
        <translation>Permet certificats no confiables</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/settings/IncomingServerGroup.qml" line="189"/>
        <source>Server settings</source>
        <translation>Configuració del servidor</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/settings/IncomingServerGroup.qml" line="194"/>
        <source>Check for new mail on start</source>
        <translation>Comprova si hi ha correu nou a l&apos;inici</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/settings/IncomingServerGroup.qml" line="200"/>
        <source>Enable IMAP IDLE</source>
        <translation>Habilita l&apos;IMAP IDLE</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/settings/IncomingServerGroup.qml" line="207"/>
        <source>Check interval (minutes)</source>
        <translation>Interval de comprovació (minuts)</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/settings/IncomingServerGroup.qml" line="214"/>
        <source>Check when roaming</source>
        <translation>Comprova durant la itinerància</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/settings/IncomingServerGroup.qml" line="220"/>
        <source>Maximum mail size (MB)</source>
        <translation>Mida màxima del missatge (MB)</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/settings/IncomingServerGroup.qml" line="226"/>
        <source>No maximum mail size</source>
        <translation>Sense mida màxima de missatge</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/settings/IncomingServerGroup.qml" line="234"/>
        <source>Automatically download attachments</source>
        <translation>Descarrega els adjunts automàticament</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/settings/IncomingServerGroup.qml" line="240"/>
        <source>Allowed to delete mail</source>
        <translation>S&apos;ha permès eliminar missatges</translation>
    </message>
</context>
<context>
    <name>LicensesPage</name>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/views/LicensesPage.qml" line="25"/>
        <source>Licenses</source>
        <translation>Llicències</translation>
    </message>
</context>
<context>
    <name>MailSettings</name>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/settings/MailSettings.qml" line="11"/>
        <source>Mail Settings</source>
        <translation>Configuració del correu</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/settings/MailSettings.qml" line="24"/>
        <source>Accounts</source>
        <translation>Comptes</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/settings/MailSettings.qml" line="34"/>
        <source>Identities</source>
        <translation>Identitats</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/settings/MailSettings.qml" line="44"/>
        <source>Display</source>
        <translation>Mostra</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/settings/MailSettings.qml" line="51"/>
        <source>Privacy</source>
        <translation>Privacitat</translation>
    </message>
</context>
<context>
    <name>MailSettingsAction</name>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/settings/MailSettingsAction.qml" line="7"/>
        <source>Mail</source>
        <translation>Correu</translation>
    </message>
</context>
<context>
    <name>MailStore</name>
    <message>
        <location filename="../Dekko/stores/Mail/MailStore.qml" line="38"/>
        <source>All</source>
        <translation>Tot</translation>
    </message>
    <message>
        <location filename="../Dekko/stores/Mail/MailStore.qml" line="38"/>
        <source>Unread</source>
        <translation>Sense llegir</translation>
    </message>
    <message>
        <location filename="../Dekko/stores/Mail/MailStore.qml" line="38"/>
        <source>Starred</source>
        <translation>Preferits</translation>
    </message>
    <message>
        <location filename="../Dekko/stores/Mail/MailStore.qml" line="38"/>
        <source>Replied</source>
        <translation>Contestats</translation>
    </message>
    <message>
        <location filename="../Dekko/stores/Mail/MailStore.qml" line="38"/>
        <source>Forwarded</source>
        <translation>Reenviats</translation>
    </message>
    <message>
        <location filename="../Dekko/stores/Mail/MailStore.qml" line="38"/>
        <source>Attachments</source>
        <translation>Fitxers adjunts</translation>
    </message>
    <message>
        <location filename="../Dekko/stores/Mail/MailStore.qml" line="38"/>
        <source>Calendar</source>
        <translation>Calendari</translation>
    </message>
    <message>
        <location filename="../Dekko/stores/Mail/MailStore.qml" line="39"/>
        <source>Local</source>
        <translation>Local</translation>
    </message>
</context>
<context>
    <name>MailStoreActions</name>
    <message>
        <location filename="../Dekko/stores/Mail/MailStoreActions.qml" line="36"/>
        <source>Unselect all</source>
        <translation>No en seleccionis cap</translation>
    </message>
    <message>
        <location filename="../Dekko/stores/Mail/MailStoreActions.qml" line="36"/>
        <source>Select all</source>
        <translation>Selecciona-ho tot</translation>
    </message>
    <message>
        <location filename="../Dekko/stores/Mail/MailStoreActions.qml" line="49"/>
        <source>Star</source>
        <translation>Fes preferit</translation>
    </message>
    <message>
        <location filename="../Dekko/stores/Mail/MailStoreActions.qml" line="49"/>
        <source>Remove star</source>
        <translation>No facis preferit</translation>
    </message>
    <message>
        <location filename="../Dekko/stores/Mail/MailStoreActions.qml" line="57"/>
        <source>Delete</source>
        <translation>Suprimeix</translation>
    </message>
    <message>
        <location filename="../Dekko/stores/Mail/MailStoreActions.qml" line="62"/>
        <source>Mark as un-read</source>
        <translation>Marca&apos;l com a no llegit</translation>
    </message>
    <message>
        <location filename="../Dekko/stores/Mail/MailStoreActions.qml" line="62"/>
        <source>Mark as read</source>
        <translation>Marca&apos;l com a llegit</translation>
    </message>
</context>
<context>
    <name>MailUtils</name>
    <message>
        <location filename="../plugins/ubuntu-plugin/imports/constants/MailUtils.qml" line="27"/>
        <source>To</source>
        <translation>Per</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/imports/constants/MailUtils.qml" line="29"/>
        <source>Cc</source>
        <translation>Cc</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/imports/constants/MailUtils.qml" line="31"/>
        <source>Bcc</source>
        <translation>Cco</translation>
    </message>
</context>
<context>
    <name>MailboxPickerPage</name>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/views/MailboxPickerPage.qml" line="34"/>
        <source>Select folder</source>
        <translation>Selecciona la carpeta</translation>
    </message>
</context>
<context>
    <name>MailboxWorker</name>
    <message>
        <location filename="../Dekko/workers/MailboxWorker.qml" line="161"/>
        <source>Message moved</source>
        <translation>S&apos;ha mogut el missatge</translation>
    </message>
</context>
<context>
    <name>MainUI</name>
    <message>
        <location filename="../plugins/ubuntu-plugin/qml/MainUI.qml" line="17"/>
        <source>Dekko Mail</source>
        <translation>Correu Dekko</translation>
    </message>
</context>
<context>
    <name>ManageAccountsPage</name>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/settings/ManageAccountsPage.qml" line="26"/>
        <source>Manage accounts</source>
        <translation>Gestiona els comptes</translation>
    </message>
</context>
<context>
    <name>ManualInputUI</name>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/setupwizard/components/ManualInputUI.qml" line="28"/>
        <source>Server configuration</source>
        <translation>Configuració del servidor</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/setupwizard/components/ManualInputUI.qml" line="33"/>
        <source>IMAP Server:</source>
        <translation>Servidor IMAP:</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/setupwizard/components/ManualInputUI.qml" line="46"/>
        <source>POP3 Server:</source>
        <translation>Servidor POP3:</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/setupwizard/components/ManualInputUI.qml" line="59"/>
        <source>SMTP Server:</source>
        <translation>Servidor SMTP:</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/setupwizard/components/ManualInputUI.qml" line="71"/>
        <source>Back</source>
        <translation>Enrere</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/setupwizard/components/ManualInputUI.qml" line="75"/>
        <source>Next</source>
        <translation>Següent</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/setupwizard/components/ManualInputUI.qml" line="135"/>
        <source>Password empty</source>
        <translation>Contrasenya buida</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/setupwizard/components/ManualInputUI.qml" line="135"/>
        <source>Would you like to continue?</source>
        <translation>Voldríeu continuar?</translation>
    </message>
</context>
<context>
    <name>MarkdownEditor</name>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/extensions/Markdown/MarkdownEditor.qml" line="67"/>
        <source>Preview</source>
        <translation>Previsualització</translation>
    </message>
</context>
<context>
    <name>Message</name>
    <message>
        <location filename="../Dekko/backend/mail/Message.cpp" line="228"/>
        <source>you</source>
        <translation>vostè</translation>
    </message>
    <message>
        <location filename="../Dekko/backend/mail/Message.cpp" line="230"/>
        <source>to %1</source>
        <translation>per %1</translation>
    </message>
</context>
<context>
    <name>MessageActionPopover</name>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/popovers/MessageActionPopover.qml" line="44"/>
        <source>Reply all</source>
        <translation>Respon a tots</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/popovers/MessageActionPopover.qml" line="52"/>
        <source>Forward</source>
        <translation>Reenvia</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/popovers/MessageActionPopover.qml" line="65"/>
        <source>Move</source>
        <translation>Mou</translation>
    </message>
</context>
<context>
    <name>MessageBuilder</name>
    <message>
        <location filename="../Dekko/backend/mail/MessageBuilder.cpp" line="268"/>
        <source>On %1, %2 wrote:
</source>
        <translation>El %1, %2 va escriure:
</translation>
    </message>
    <message>
        <location filename="../Dekko/backend/mail/MessageBuilder.cpp" line="325"/>
        <source>Forwarded Message</source>
        <translation>Missatge reenviat</translation>
    </message>
    <message>
        <location filename="../Dekko/backend/mail/MessageBuilder.cpp" line="326"/>
        <source>Date: </source>
        <translation>Data: </translation>
    </message>
    <message>
        <location filename="../Dekko/backend/mail/MessageBuilder.cpp" line="327"/>
        <source>From: </source>
        <translation>De: </translation>
    </message>
    <message>
        <location filename="../Dekko/backend/mail/MessageBuilder.cpp" line="328"/>
        <source>To: </source>
        <translation>Per: </translation>
    </message>
    <message>
        <location filename="../Dekko/backend/mail/MessageBuilder.cpp" line="329"/>
        <source>Subject: </source>
        <translation>Assumpte: </translation>
    </message>
</context>
<context>
    <name>MessageFilterCollection</name>
    <message>
        <location filename="../Dekko/backend/mail/MessageFilterCollection.cpp" line="79"/>
        <source>All inboxes</source>
        <translation>Totes les s. d&apos;entrada</translation>
    </message>
    <message>
        <location filename="../Dekko/backend/mail/MessageFilterCollection.cpp" line="89"/>
        <source>Drafts</source>
        <translation>Esborranys</translation>
    </message>
    <message>
        <location filename="../Dekko/backend/mail/MessageFilterCollection.cpp" line="99"/>
        <source>Spam</source>
        <translation>Correu brossa</translation>
    </message>
    <message>
        <location filename="../Dekko/backend/mail/MessageFilterCollection.cpp" line="109"/>
        <source>Outbox</source>
        <translation>Safata de sortida</translation>
    </message>
    <message>
        <location filename="../Dekko/backend/mail/MessageFilterCollection.cpp" line="119"/>
        <source>Sent</source>
        <translation>Enviat</translation>
    </message>
    <message>
        <location filename="../Dekko/backend/mail/MessageFilterCollection.cpp" line="128"/>
        <source>Trash</source>
        <translation>Paperera</translation>
    </message>
    <message>
        <location filename="../Dekko/backend/mail/MessageFilterCollection.cpp" line="140"/>
        <source>Today, %1</source>
        <translation>Avui, %1</translation>
    </message>
    <message>
        <location filename="../Dekko/backend/mail/MessageFilterCollection.cpp" line="149"/>
        <source>To-do</source>
        <translation>Pendent</translation>
    </message>
    <message>
        <location filename="../Dekko/backend/mail/MessageFilterCollection.cpp" line="158"/>
        <source>Done</source>
        <translation>Fet</translation>
    </message>
</context>
<context>
    <name>MessageHeader</name>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/messageview/MessageHeader.qml" line="47"/>
        <source>Hide details</source>
        <translation>Oculta els detalls</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/messageview/MessageHeader.qml" line="47"/>
        <source>View details</source>
        <translation>Mostra els detalls</translation>
    </message>
</context>
<context>
    <name>MessageListActionPopover</name>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/popovers/MessageListActionPopover.qml" line="45"/>
        <source>Mark as unread</source>
        <translation>Marca&apos;l com a no llegit</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/popovers/MessageListActionPopover.qml" line="45"/>
        <source>Mark as read</source>
        <translation>Marca&apos;l com a llegit</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/popovers/MessageListActionPopover.qml" line="53"/>
        <source>Mark as not important</source>
        <translation>Marca&apos;l com a no important</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/popovers/MessageListActionPopover.qml" line="53"/>
        <source>Mark as important</source>
        <translation>Marca&apos;l com a important</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/popovers/MessageListActionPopover.qml" line="61"/>
        <source>Mark as spam</source>
        <translation>Marca&apos;l com a correu brossa</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/popovers/MessageListActionPopover.qml" line="74"/>
        <source>To-do</source>
        <translation>Pendent</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/popovers/MessageListActionPopover.qml" line="91"/>
        <source>Done</source>
        <translation>Fet</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/popovers/MessageListActionPopover.qml" line="105"/>
        <source>Reply</source>
        <translation>Respon</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/popovers/MessageListActionPopover.qml" line="113"/>
        <source>Reply all</source>
        <translation>Respon a tots</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/popovers/MessageListActionPopover.qml" line="121"/>
        <source>Forward</source>
        <translation>Reenvia</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/popovers/MessageListActionPopover.qml" line="135"/>
        <source>Move</source>
        <translation>Mou</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/popovers/MessageListActionPopover.qml" line="144"/>
        <source>Restore to %1</source>
        <translation>Restaura a %1</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/popovers/MessageListActionPopover.qml" line="153"/>
        <source>Delete</source>
        <translation>Suprimeix</translation>
    </message>
</context>
<context>
    <name>MessageListDelegate</name>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/delegates/MessageListDelegate.qml" line="51"/>
        <source>Un-mark flagged</source>
        <translation>Desmarca els senyalats</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/delegates/MessageListDelegate.qml" line="51"/>
        <source>Mark flagged</source>
        <translation>Marca els senyalats</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/delegates/MessageListDelegate.qml" line="60"/>
        <source>Mark as un-read</source>
        <translation>Marca&apos;l com a no llegit</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/delegates/MessageListDelegate.qml" line="60"/>
        <source>Mark as read</source>
        <translation>Marca&apos;l com a llegit</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/delegates/MessageListDelegate.qml" line="67"/>
        <source>Move message</source>
        <translation>Mou el missatge</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/delegates/MessageListDelegate.qml" line="72"/>
        <source>Context menu</source>
        <translation>Menú contextual</translation>
    </message>
</context>
<context>
    <name>MessageListView</name>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/views/MessageListView.qml" line="115"/>
        <source>Unselect all</source>
        <translation>Anul·la la selecció</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/views/MessageListView.qml" line="115"/>
        <source>Select all</source>
        <translation>Selecciona-ho tot</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/views/MessageListView.qml" line="127"/>
        <source>Star</source>
        <translation>Posa l&apos;estrella</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/views/MessageListView.qml" line="127"/>
        <source>Remove star</source>
        <translation>Treu l&apos;estrella</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/views/MessageListView.qml" line="131"/>
        <source>Mark as un-read</source>
        <translation>Marca&apos;l com a no llegit</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/views/MessageListView.qml" line="131"/>
        <source>Mark as read</source>
        <translation>Marca&apos;l com a llegit</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/views/MessageListView.qml" line="138"/>
        <source>Delete</source>
        <translation>Suprimeix</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/views/MessageListView.qml" line="216"/>
        <source>Undo</source>
        <translation>Desfés</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/views/MessageListView.qml" line="340"/>
        <source>Load more messages ...</source>
        <translation>Carrega més missatges ...</translation>
    </message>
</context>
<context>
    <name>MessageViewContextMenu</name>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/popovers/MessageViewContextMenu.qml" line="46"/>
        <source>Open in browser</source>
        <translation>Obre en el navegador</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/popovers/MessageViewContextMenu.qml" line="54"/>
        <source>Copy link</source>
        <translation>Copia l&apos;enllaç</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/popovers/MessageViewContextMenu.qml" line="63"/>
        <source>Share link</source>
        <translation>Comparteix l&apos;enllaç</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/popovers/MessageViewContextMenu.qml" line="77"/>
        <source>Reply</source>
        <translation>Respon</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/popovers/MessageViewContextMenu.qml" line="86"/>
        <source>Reply all</source>
        <translation>Respon a tots</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/popovers/MessageViewContextMenu.qml" line="94"/>
        <source>Forward</source>
        <translation>Reenvia</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/popovers/MessageViewContextMenu.qml" line="106"/>
        <source>View source</source>
        <translation>Mostra el codi font</translation>
    </message>
</context>
<context>
    <name>MinimalMessage</name>
    <message>
        <location filename="../Dekko/backend/mail/Message.cpp" line="110"/>
        <location filename="../Dekko/backend/mail/Message.cpp" line="126"/>
        <source>hh:mm</source>
        <comment>time format as hours:minutes e.g. 12:45</comment>
        <translation>hh:mm</translation>
    </message>
    <message>
        <location filename="../Dekko/backend/mail/Message.cpp" line="115"/>
        <source>ddd hh:mm</source>
        <comment>time/date format as short day name hours:minutes e.g. Mon. 12:45</comment>
        <translation>ddd hh:mm</translation>
    </message>
    <message>
        <location filename="../Dekko/backend/mail/Message.cpp" line="117"/>
        <source>dd MMM</source>
        <comment>date format as day name and month e.g. 02 May</comment>
        <translation>dd MMM</translation>
    </message>
    <message>
        <location filename="../Dekko/backend/mail/Message.cpp" line="119"/>
        <source>dd MMM yy</source>
        <comment>date format as day month year e.g. 31 02 2020</comment>
        <translation>dd MMM yy</translation>
    </message>
    <message>
        <location filename="../Dekko/backend/mail/Message.cpp" line="126"/>
        <source>dddd dd</source>
        <comment>time format as long day name and day e.g. Monday 02</comment>
        <translation>dddd dd</translation>
    </message>
</context>
<context>
    <name>NavMenuAccountSettingsModel</name>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/models/NavMenuAccountSettingsModel.qml" line="29"/>
        <source>Manage accounts</source>
        <translation>Gestiona els comptes</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/models/NavMenuAccountSettingsModel.qml" line="45"/>
        <source>Display settings</source>
        <translation>Configuració de la visualització</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/models/NavMenuAccountSettingsModel.qml" line="67"/>
        <source>Privacy settings</source>
        <translation>Configuració de la privacitat</translation>
    </message>
</context>
<context>
    <name>NavMenuContactsModel</name>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/models/NavMenuContactsModel.qml" line="27"/>
        <source>Addressbook</source>
        <translation>Llibreta d&apos;adreces</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/models/NavMenuContactsModel.qml" line="43"/>
        <source>Recent contacts</source>
        <translation>Contactes recents</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/models/NavMenuContactsModel.qml" line="58"/>
        <source>Import contacts</source>
        <translation>Contactes importants</translation>
    </message>
</context>
<context>
    <name>NavMenuDekkoVisualModel</name>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/models/NavMenuDekkoVisualModel.qml" line="27"/>
        <source>Version</source>
        <translation>Versió</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/models/NavMenuDekkoVisualModel.qml" line="45"/>
        <source>Licenses</source>
        <translation>Llicències</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/models/NavMenuDekkoVisualModel.qml" line="63"/>
        <source>Contributors</source>
        <translation>Col·laboradors</translation>
    </message>
</context>
<context>
    <name>NavMenuModel</name>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/models/NavMenuModel.qml" line="98"/>
        <source>Smart folders</source>
        <translation>Carpetes intel·ligents</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/models/NavMenuModel.qml" line="120"/>
        <source>Folders</source>
        <translation>Carpetes</translation>
    </message>
</context>
<context>
    <name>NavMenuPage</name>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/views/NavMenuPage.qml" line="30"/>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/views/NavMenuPage.qml" line="64"/>
        <source>Mail</source>
        <translation>Correu</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/views/NavMenuPage.qml" line="67"/>
        <source>Contacts</source>
        <translation>Contactes</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/views/NavMenuPage.qml" line="70"/>
        <source>Settings</source>
        <translation>Configuració</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/views/NavMenuPage.qml" line="73"/>
        <source>About</source>
        <translation>Quant a</translation>
    </message>
</context>
<context>
    <name>NavMenuStandardFolderDelegate</name>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/delegates/NavMenuStandardFolderDelegate.qml" line="192"/>
        <source>Inbox (%1)</source>
        <translation>Safata d&apos;entrada (%1)</translation>
    </message>
</context>
<context>
    <name>NavSideBar</name>
    <message>
        <location filename="../plugins/ubuntu-plugin/imports/components/private/NavSideBar.qml" line="155"/>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/views/NavSideBar.qml" line="151"/>
        <source>Smart folders</source>
        <translation>Carpetes intel·ligents</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/imports/components/private/NavSideBar.qml" line="169"/>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/views/NavSideBar.qml" line="165"/>
        <source>Folders</source>
        <translation>Carpetes</translation>
    </message>
</context>
<context>
    <name>NavViewContextMenu</name>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/popovers/NavViewContextMenu.qml" line="44"/>
        <source>Sync folder</source>
        <translation>Sincronitza la carpeta</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/popovers/NavViewContextMenu.qml" line="53"/>
        <source>Send pending</source>
        <translation>Envia els pendents</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/popovers/NavViewContextMenu.qml" line="62"/>
        <source>Mark folder read</source>
        <translation>Marca la carpeta com a llegida</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/popovers/NavViewContextMenu.qml" line="76"/>
        <source>Empty trash</source>
        <translation>Buida la paperera</translation>
    </message>
</context>
<context>
    <name>NewAccountsUI</name>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/setupwizard/components/NewAccountsUI.qml" line="32"/>
        <source>New account</source>
        <translation>Compte nou</translation>
    </message>
</context>
<context>
    <name>NoAccountsUI</name>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/setupwizard/components/NoAccountsUI.qml" line="31"/>
        <source>Accounts</source>
        <translation>Comptes</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/setupwizard/components/NoAccountsUI.qml" line="93"/>
        <source>No email account is setup.</source>
        <translation>No s&apos;ha configurat cap compte de correu.</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/setupwizard/components/NoAccountsUI.qml" line="102"/>
        <source>Add now</source>
        <translation>Afegeix ara</translation>
    </message>
</context>
<context>
    <name>NothingSelectedPage</name>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/views/NothingSelectedPage.qml" line="55"/>
        <source>No message selected</source>
        <translation>No s&apos;ha seleccionat cap missatge</translation>
    </message>
</context>
<context>
    <name>NoticePopup</name>
    <message>
        <location filename="../plugins/ubuntu-plugin/imports/dialogs/NoticePopup.qml" line="27"/>
        <source>Ok</source>
        <translation>D&apos;acord</translation>
    </message>
</context>
<context>
    <name>NotificationSettings</name>
    <message>
        <location filename="../plugins/ubuntu-notification-plugin/qml/NotificationSettings.qml" line="7"/>
        <source>Notifications</source>
        <translation>Notificacions</translation>
    </message>
</context>
<context>
    <name>NotificationSettingsPage</name>
    <message>
        <location filename="../plugins/ubuntu-notification-plugin/qml/NotificationSettingsPage.qml" line="26"/>
        <source>Notification Settings: %1</source>
        <translation>Paràmetres de les notificacions: %1</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-notification-plugin/qml/NotificationSettingsPage.qml" line="52"/>
        <source>Enabled</source>
        <translation>habilitat</translation>
    </message>
</context>
<context>
    <name>NotificationWatcher</name>
    <message>
        <location filename="../plugins/ubuntu-notification-plugin/service/notificationwatcher.cpp" line="57"/>
        <source>%1 New messages</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>OutgoingServerGroup</name>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/settings/OutgoingServerGroup.qml" line="29"/>
        <source>Outgoing Server: %1</source>
        <translation>Servidor de sortida: %1</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/settings/OutgoingServerGroup.qml" line="76"/>
        <source>Hostname</source>
        <translation>Nom del servidor</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/settings/OutgoingServerGroup.qml" line="84"/>
        <source>Port</source>
        <translation>Port</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/settings/OutgoingServerGroup.qml" line="93"/>
        <source>Username</source>
        <translation>Nom d&apos;usuari</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/settings/OutgoingServerGroup.qml" line="101"/>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/settings/OutgoingServerGroup.qml" line="104"/>
        <source>Password</source>
        <translation>Contrasenya</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/settings/OutgoingServerGroup.qml" line="111"/>
        <source>Show password</source>
        <translation>Mostra la contrasenya</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/settings/OutgoingServerGroup.qml" line="117"/>
        <source>Security settings</source>
        <translation>Configuració de la seguretat</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/settings/OutgoingServerGroup.qml" line="150"/>
        <source>Authenticate from server capabilities</source>
        <translation>Autentifica des de les funcionalitats del servidor</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/settings/OutgoingServerGroup.qml" line="156"/>
        <source>Allow untrusted certificates</source>
        <translation>Permet certificats no confiables</translation>
    </message>
</context>
<context>
    <name>PopClient</name>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/pop/popclient.cpp" line="186"/>
        <location filename="../upstream/qmf/plugins/messageservices/pop/popclient.cpp" line="222"/>
        <source>Cannot open connection without POP server configuration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/pop/popclient.cpp" line="345"/>
        <source>Inbox</source>
        <translation type="unfinished">Safata d&apos;entrada</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/pop/popclient.cpp" line="409"/>
        <source>Connected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/pop/popclient.cpp" line="786"/>
        <source>Logging in</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/pop/popclient.cpp" line="860"/>
        <source>Previewing</source>
        <comment>Previewing &lt;no of messages&gt;</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/pop/popclient.cpp" line="867"/>
        <source>Completing %1 / %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/pop/popclient.cpp" line="883"/>
        <source>Removing old messages</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/pop/popclient.cpp" line="911"/>
        <source>Removing message from server</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/pop/popclient.cpp" line="995"/>
        <source>Logging out</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PopService</name>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/pop/popservice.cpp" line="424"/>
        <source>Account disabled</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PopService::Source</name>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/pop/popservice.cpp" line="116"/>
        <location filename="../upstream/qmf/plugins/messageservices/pop/popservice.cpp" line="153"/>
        <location filename="../upstream/qmf/plugins/messageservices/pop/popservice.cpp" line="215"/>
        <location filename="../upstream/qmf/plugins/messageservices/pop/popservice.cpp" line="231"/>
        <source>No account specified</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/pop/popservice.cpp" line="140"/>
        <source>No folders specified</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/pop/popservice.cpp" line="186"/>
        <source>No messages to retrieve</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/pop/popservice.cpp" line="251"/>
        <source>No messages to delete</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PopSettings</name>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/pop/popsettings.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/pop/popsettings.ui" line="56"/>
        <source>Username</source>
        <translation type="unfinished">Nom d&apos;usuari</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/pop/popsettings.ui" line="82"/>
        <source>Password</source>
        <translation type="unfinished">Contrasenya</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/pop/popsettings.ui" line="111"/>
        <source>Server</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/pop/popsettings.ui" line="137"/>
        <source>Port</source>
        <translation type="unfinished">Port</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/pop/popsettings.ui" line="163"/>
        <source>Encryption</source>
        <translation type="unfinished">Xifratge</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/pop/popsettings.ui" line="180"/>
        <source>None</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/pop/popsettings.ui" line="185"/>
        <source>SSL</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/pop/popsettings.ui" line="190"/>
        <source>TLS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/pop/popsettings.ui" line="204"/>
        <source>Remove deleted mail from server</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/pop/popsettings.ui" line="226"/>
        <source>Skip larger</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/pop/popsettings.ui" line="248"/>
        <source>Interval</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/pop/popsettings.ui" line="270"/>
        <source>Disable when Roaming</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/pop/popsettings.ui" line="286"/>
        <source>min</source>
        <comment>short for minutes</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/pop/popsettings.ui" line="314"/>
        <source>K</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PrivacySettings</name>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/settings/PrivacySettings.qml" line="28"/>
        <source>Message content</source>
        <translation>Contingut del missatge</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/settings/PrivacySettings.qml" line="32"/>
        <source>Allow remote content</source>
        <translation>Permet contingut remot</translation>
    </message>
</context>
<context>
    <name>PrivacySettingsPage</name>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/settings/PrivacySettingsPage.qml" line="21"/>
        <source>Privacy Settings</source>
        <translation>Configuració de la privacitat</translation>
    </message>
</context>
<context>
    <name>PrivacySettingsPopup</name>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/settings/PrivacySettingsPopup.qml" line="21"/>
        <source>Privacy Settings</source>
        <translation>Configuració de la privacitat</translation>
    </message>
</context>
<context>
    <name>PushFolderList</name>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapsettings.cpp" line="153"/>
        <source>Push folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapsettings.cpp" line="163"/>
        <source>...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QCoreApplication</name>
    <message>
        <location filename="../upstream/qmf/libraries/qmfclient/longstream.cpp" line="236"/>
        <source>Storage for messages is full. Some new messages could not be retrieved.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QMailMessageModelBase</name>
    <message>
        <location filename="../upstream/qmf/libraries/qmfclient/qmailmessagemodelbase.cpp" line="67"/>
        <source>Draft message</source>
        <translation type="unfinished"></translation>
    </message>
    <message numerus="yes">
        <location filename="../upstream/qmf/libraries/qmfclient/qmailmessagemodelbase.cpp" line="78"/>
        <location filename="../upstream/qmf/libraries/qmfclient/qmailmessagemodelbase.cpp" line="80"/>
        <source>%n byte(s)</source>
        <translation type="unfinished">
            <numerusform>%n byte</numerusform>
            <numerusform>%n bytes</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../upstream/qmf/libraries/qmfclient/qmailmessagemodelbase.cpp" line="83"/>
        <source>%1 KB</source>
        <translation type="unfinished">%1 KB</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/libraries/qmfclient/qmailmessagemodelbase.cpp" line="85"/>
        <source>%1 MB</source>
        <translation type="unfinished">%1 MB</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/libraries/qmfclient/qmailmessagemodelbase.cpp" line="87"/>
        <source>%1 GB</source>
        <translation type="unfinished">%1 GB</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/libraries/qmfclient/qmailmessagemodelbase.cpp" line="264"/>
        <source>Received</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../upstream/qmf/libraries/qmfclient/qmailmessagemodelbase.cpp" line="267"/>
        <source>Sent</source>
        <translation type="unfinished">Enviat</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/libraries/qmfclient/qmailmessagemodelbase.cpp" line="269"/>
        <source>Last edited</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QMailMessageService</name>
    <message>
        <location filename="../upstream/qmf/libraries/qmfmessageserver/qmailmessageservice.h" line="300"/>
        <location filename="../upstream/qmf/libraries/qmfmessageserver/qmailmessageservice.h" line="301"/>
        <source>Cancelled by user</source>
        <translation type="unfinished">Cancel·lat per l&apos;usuari/a</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapservice.cpp" line="2235"/>
        <source>IMAP</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/pop/popservice.cpp" line="464"/>
        <source>POP</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/qmfsettings/service.cpp" line="81"/>
        <source>Mailfile</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/smtp/smtpservice.cpp" line="371"/>
        <source>SMTP</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QMailMessageSource</name>
    <message>
        <location filename="../upstream/qmf/libraries/qmfmessageserver/qmailmessageservice.cpp" line="1104"/>
        <source>Unable to delete messages</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../upstream/qmf/libraries/qmfmessageserver/qmailmessageservice.cpp" line="1117"/>
        <source>Insufficient space to copy messages to folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../upstream/qmf/libraries/qmfmessageserver/qmailmessageservice.cpp" line="1138"/>
        <source>Unable to copy messages for account</source>
        <translation type="unfinished">No s&apos;han pogut copiar els missatges del compte</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/libraries/qmfmessageserver/qmailmessageservice.cpp" line="1172"/>
        <source>Unable to move messages to folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../upstream/qmf/libraries/qmfmessageserver/qmailmessageservice.cpp" line="1189"/>
        <source>Unable to flag messages</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QMailServiceAction</name>
    <message>
        <location filename="../upstream/qmf/libraries/qmfmessageserver/qmailmessageservice.cpp" line="277"/>
        <source>Connection refused</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../upstream/qmf/libraries/qmfmessageserver/qmailmessageservice.cpp" line="278"/>
        <source>Remote host closed the connection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../upstream/qmf/libraries/qmfmessageserver/qmailmessageservice.cpp" line="279"/>
        <source>Host not found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../upstream/qmf/libraries/qmfmessageserver/qmailmessageservice.cpp" line="280"/>
        <source>Permission denied</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../upstream/qmf/libraries/qmfmessageserver/qmailmessageservice.cpp" line="281"/>
        <source>Insufficient resources</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../upstream/qmf/libraries/qmfmessageserver/qmailmessageservice.cpp" line="282"/>
        <source>Operation timed out</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../upstream/qmf/libraries/qmfmessageserver/qmailmessageservice.cpp" line="283"/>
        <source>Datagram too large</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../upstream/qmf/libraries/qmfmessageserver/qmailmessageservice.cpp" line="284"/>
        <source>Network error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../upstream/qmf/libraries/qmfmessageserver/qmailmessageservice.cpp" line="285"/>
        <source>Address in use</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../upstream/qmf/libraries/qmfmessageserver/qmailmessageservice.cpp" line="286"/>
        <source>Address not available</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../upstream/qmf/libraries/qmfmessageserver/qmailmessageservice.cpp" line="287"/>
        <source>Unsupported operation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../upstream/qmf/libraries/qmfmessageserver/qmailmessageservice.cpp" line="288"/>
        <source>Unknown error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../upstream/qmf/libraries/qmfmessageserver/qmailmessageservice.cpp" line="298"/>
        <source>This function is not currently supported.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../upstream/qmf/libraries/qmfmessageserver/qmailmessageservice.cpp" line="299"/>
        <source>Framework error occurred.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../upstream/qmf/libraries/qmfmessageserver/qmailmessageservice.cpp" line="302"/>
        <source>Login failed - check user name and password.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../upstream/qmf/libraries/qmfmessageserver/qmailmessageservice.cpp" line="303"/>
        <source>Operation cancelled.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../upstream/qmf/libraries/qmfmessageserver/qmailmessageservice.cpp" line="304"/>
        <source>Mail check failed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../upstream/qmf/libraries/qmfmessageserver/qmailmessageservice.cpp" line="305"/>
        <source>Message deleted from server.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../upstream/qmf/libraries/qmfmessageserver/qmailmessageservice.cpp" line="306"/>
        <source>Unable to queue message for transmission.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../upstream/qmf/libraries/qmfmessageserver/qmailmessageservice.cpp" line="307"/>
        <source>Cannot determine the connection to transmit message on.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../upstream/qmf/libraries/qmfmessageserver/qmailmessageservice.cpp" line="308"/>
        <source>Outgoing connection already in use by another operation.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../upstream/qmf/libraries/qmfmessageserver/qmailmessageservice.cpp" line="309"/>
        <source>Outgoing connection is not ready to transmit message.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../upstream/qmf/libraries/qmfmessageserver/qmailmessageservice.cpp" line="310"/>
        <source>Unable to use account due to invalid configuration.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../upstream/qmf/libraries/qmfmessageserver/qmailmessageservice.cpp" line="311"/>
        <source>Message origin or recipient addresses are not correctly formatted.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../upstream/qmf/libraries/qmfmessageserver/qmailmessageservice.cpp" line="312"/>
        <source>Configured service unable to handle supplied data.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../upstream/qmf/libraries/qmfmessageserver/qmailmessageservice.cpp" line="313"/>
        <source>Configured service failed to perform action within a reasonable period of time.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../upstream/qmf/libraries/qmfmessageserver/qmailmessageservice.cpp" line="356"/>
        <source>Unable to send; message moved to Drafts folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../upstream/qmf/libraries/qmfmessageserver/qmailmessageservice.cpp" line="358"/>
        <source>Unexpected response from server: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../upstream/qmf/libraries/qmfmessageserver/qmailmessageservice.cpp" line="366"/>
        <source>Error %1</source>
        <comment>%1 contains numeric error code</comment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QMailStorePrivate</name>
    <message>
        <location filename="../upstream/qmf/libraries/qmfclient/qmailstore_p.cpp" line="3175"/>
        <source>Local Storage</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QMailTransport</name>
    <message>
        <location filename="../upstream/qmf/libraries/qmfmessageserver/qmailtransport.cpp" line="289"/>
        <source>DNS lookup</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../upstream/qmf/libraries/qmfmessageserver/qmailtransport.cpp" line="415"/>
        <location filename="../upstream/qmf/libraries/qmfmessageserver/qmailtransport.cpp" line="435"/>
        <source>Connected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../upstream/qmf/libraries/qmfmessageserver/qmailtransport.cpp" line="426"/>
        <source>Connection timed out</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../upstream/qmf/libraries/qmfmessageserver/qmailtransport.cpp" line="455"/>
        <location filename="../upstream/qmf/libraries/qmfmessageserver/qmailtransport.cpp" line="494"/>
        <source>Error occurred</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../upstream/qmf/libraries/qmfmessageserver/qmailtransport.cpp" line="458"/>
        <location filename="../upstream/qmf/libraries/qmfmessageserver/qmailtransport.cpp" line="460"/>
        <location filename="../upstream/qmf/libraries/qmfmessageserver/qmailtransport.cpp" line="504"/>
        <source>Socket error</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message numerus="yes">
        <location filename="../Dekko/backend/mail/Attachments.cpp" line="344"/>
        <source>%n byte(s)</source>
        <translation>
            <numerusform>%n byte</numerusform>
            <numerusform>%n bytes</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../Dekko/backend/mail/Attachments.cpp" line="346"/>
        <source>%1 KB</source>
        <translation>%1 KB</translation>
    </message>
    <message>
        <location filename="../Dekko/backend/mail/Attachments.cpp" line="348"/>
        <source>%1 MB</source>
        <translation>%1 MB</translation>
    </message>
    <message>
        <location filename="../Dekko/backend/mail/Attachments.cpp" line="350"/>
        <source>%1 GB</source>
        <translation>%1 GB</translation>
    </message>
    <message>
        <location filename="../upstream/maz-db/leveldb/util/env_qt.cc" line="122"/>
        <source>File not open</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../upstream/maz-db/leveldb/util/env_qt.cc" line="277"/>
        <source>Could not create path</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../upstream/maz-db/leveldb/util/env_qt.cc" line="285"/>
        <source>Could not delete path</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../upstream/maz-db/leveldb/util/env_qt.cc" line="324"/>
        <source>No error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../upstream/maz-db/leveldb/util/env_qt.cc" line="327"/>
        <source>Lock failed error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../upstream/maz-db/leveldb/util/env_qt.cc" line="330"/>
        <source>Permission error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../upstream/maz-db/leveldb/util/env_qt.cc" line="333"/>
        <source>Unknown error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../upstream/maz-db/src/quick/MazDBPlugin.cpp" line="44"/>
        <location filename="../upstream/maz-db/tests/runner.cpp" line="27"/>
        <source>Cannot create separate instance of Options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../upstream/maz-db/src/quick/MazDBPlugin.cpp" line="45"/>
        <location filename="../upstream/maz-db/tests/runner.cpp" line="28"/>
        <source>Cannot create separate instance of Batch</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../upstream/maz-db/src/quick/MazDBPlugin.cpp" line="46"/>
        <location filename="../upstream/maz-db/tests/runner.cpp" line="29"/>
        <source>Cannot create separate instance of QueryBase</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../upstream/maz-db/src/quick/MazDBPlugin.cpp" line="47"/>
        <source>Cannot create separate instance of Range</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapprotocol.cpp" line="2035"/>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapprotocol.cpp" line="2118"/>
        <source>This server does not provide a complete IMAP4rev1 implementation.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapstrategy.cpp" line="1753"/>
        <source>Completing %1 / %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapstrategy.cpp" line="2201"/>
        <source>Previewing</source>
        <comment>Previewing &lt;number of messages&gt;</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapstrategy.cpp" line="2240"/>
        <source>Checking</source>
        <comment>Checking &lt;mailbox name&gt;</comment>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapstrategy.cpp" line="2358"/>
        <source>Retrieving folders</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapstrategy.cpp" line="2810"/>
        <source>Marking message as read</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapstrategy.cpp" line="2829"/>
        <source>Marking message as unread</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapstrategy.cpp" line="2848"/>
        <source>Marking message as important</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapstrategy.cpp" line="2867"/>
        <source>Marking message as unimportant</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapstrategy.cpp" line="2888"/>
        <source>Deleting message</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapstrategy.cpp" line="3350"/>
        <source>Scanning folders</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapstrategy.cpp" line="3352"/>
        <source>Scanning folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapstrategy.cpp" line="3958"/>
        <source>Copying %1 / %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/imap/imapstrategy.cpp" line="4262"/>
        <source>Moving %1 / %2</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QtopiamailfileSettings</name>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/qmfsettings/settings.ui" line="20"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/qmfsettings/settings.ui" line="32"/>
        <source>Location</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RecipientField</name>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/composer/RecipientField.qml" line="98"/>
        <source>Enter an address</source>
        <translation>Entra una adreça</translation>
    </message>
</context>
<context>
    <name>RecipientInfo</name>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/messageview/RecipientInfo.qml" line="42"/>
        <source>Back</source>
        <translation>Enrere</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/messageview/RecipientInfo.qml" line="94"/>
        <source>Copy to clipboard</source>
        <translation>Copia al porta-retalls</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/messageview/RecipientInfo.qml" line="108"/>
        <source>Add to addressbook</source>
        <translation>Afegeix a la llibreta d&apos;adreces</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/messageview/RecipientInfo.qml" line="123"/>
        <source>Send message</source>
        <translation>Envia el missatge</translation>
    </message>
</context>
<context>
    <name>RecipientInputContextMenu</name>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/composer/RecipientInputContextMenu.qml" line="56"/>
        <source>Add CC</source>
        <translation>Afegeix CC</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/composer/RecipientInputContextMenu.qml" line="69"/>
        <source>Add BCC</source>
        <translation>Afegeix BCC</translation>
    </message>
</context>
<context>
    <name>RecipientPopover</name>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/popovers/RecipientPopover.qml" line="81"/>
        <source>Copy to clipboard</source>
        <translation>Copia al porta-retalls</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/popovers/RecipientPopover.qml" line="90"/>
        <source>Add to addressbook</source>
        <translation>Afegeix a la llibreta d&apos;adreces</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/popovers/RecipientPopover.qml" line="99"/>
        <source>Send message</source>
        <translation>Envia el missatge</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/popovers/RecipientPopover.qml" line="109"/>
        <source>Remove</source>
        <translation>Elimina</translation>
    </message>
</context>
<context>
    <name>S:</name>
    <message>
        <location filename="../build/x86_64-linux-gnu/build/install-root/lib/x86_64-linux-gnu/Dekko/Mail/Stores/Accounts/AccountSetup.qml" line="536"/>
        <source></source>
        <comment>&apos;convergent&apos; describes the apps ability of being usable on desktop and mobile devices running different OS</comment>
        <translation></translation>
    </message>
</context>
<context>
    <name>SenderIdentityField</name>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/composer/SenderIdentityField.qml" line="53"/>
        <source>From:</source>
        <translation>De:</translation>
    </message>
</context>
<context>
    <name>ServerDetails</name>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/setupwizard/components/ServerDetails.qml" line="70"/>
        <source>Hostname</source>
        <translation>Nom del servidor</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/setupwizard/components/ServerDetails.qml" line="79"/>
        <source>Port</source>
        <translation>Port</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/setupwizard/components/ServerDetails.qml" line="101"/>
        <source>Username</source>
        <translation>Nom d&apos;usuari</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/setupwizard/components/ServerDetails.qml" line="110"/>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/setupwizard/components/ServerDetails.qml" line="113"/>
        <source>Password</source>
        <translation>Contrasenya</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/setupwizard/components/ServerDetails.qml" line="120"/>
        <source>Show password</source>
        <translation>Mostra la contrasenya</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/setupwizard/components/ServerDetails.qml" line="141"/>
        <source>Allow untrusted certificates</source>
        <translation>Permet els certificats no confiables</translation>
    </message>
</context>
<context>
    <name>ServiceHandler</name>
    <message>
        <location filename="../Dekko/server/servicehandler.cpp" line="593"/>
        <location filename="../upstream/qmf/tools/messageserver/servicehandler.cpp" line="589"/>
        <source>Destroying Service handler</source>
        <translation>Destruint en gestor de serveis</translation>
    </message>
    <message>
        <location filename="../Dekko/server/servicehandler.cpp" line="645"/>
        <location filename="../upstream/qmf/tools/messageserver/servicehandler.cpp" line="641"/>
        <source>Service became unavailable, couldn&apos;t dispatch</source>
        <translation>El servei no està disponible, no s&apos;ha pogut lliurar</translation>
    </message>
    <message>
        <location filename="../Dekko/server/servicehandler.cpp" line="826"/>
        <location filename="../upstream/qmf/tools/messageserver/servicehandler.cpp" line="822"/>
        <source>Account updated by other process</source>
        <translation>El compte ha estat actualitzat per un altre procés</translation>
    </message>
    <message>
        <location filename="../Dekko/server/servicehandler.cpp" line="833"/>
        <location filename="../upstream/qmf/tools/messageserver/servicehandler.cpp" line="829"/>
        <source>Account removed</source>
        <translation>S&apos;ha eliminat el compte</translation>
    </message>
    <message>
        <location filename="../Dekko/server/servicehandler.cpp" line="1234"/>
        <location filename="../Dekko/server/servicehandler.cpp" line="1283"/>
        <location filename="../upstream/qmf/tools/messageserver/servicehandler.cpp" line="1230"/>
        <location filename="../upstream/qmf/tools/messageserver/servicehandler.cpp" line="1279"/>
        <source>Request is not progressing</source>
        <translation>Sol·licitud sense progrés</translation>
    </message>
    <message>
        <location filename="../Dekko/server/servicehandler.cpp" line="1320"/>
        <location filename="../Dekko/server/servicehandler.cpp" line="1322"/>
        <location filename="../Dekko/server/servicehandler.cpp" line="1370"/>
        <location filename="../upstream/qmf/tools/messageserver/servicehandler.cpp" line="1316"/>
        <location filename="../upstream/qmf/tools/messageserver/servicehandler.cpp" line="1318"/>
        <location filename="../upstream/qmf/tools/messageserver/servicehandler.cpp" line="1366"/>
        <source>Cancelled by user</source>
        <translation>Cancel·lat per l&apos;usuari/a</translation>
    </message>
    <message>
        <location filename="../Dekko/server/servicehandler.cpp" line="1379"/>
        <location filename="../Dekko/server/servicehandler.cpp" line="1415"/>
        <location filename="../upstream/qmf/tools/messageserver/servicehandler.cpp" line="1375"/>
        <location filename="../upstream/qmf/tools/messageserver/servicehandler.cpp" line="1411"/>
        <source>Unable to enqueue messages for transmission</source>
        <translation>No s&apos;han pogut posar els missatges a la cua per transmetre&apos;ls</translation>
    </message>
    <message>
        <location filename="../Dekko/server/servicehandler.cpp" line="1463"/>
        <location filename="../Dekko/server/servicehandler.cpp" line="1574"/>
        <location filename="../Dekko/server/servicehandler.cpp" line="1612"/>
        <location filename="../Dekko/server/servicehandler.cpp" line="1650"/>
        <location filename="../Dekko/server/servicehandler.cpp" line="1686"/>
        <location filename="../Dekko/server/servicehandler.cpp" line="1722"/>
        <location filename="../Dekko/server/servicehandler.cpp" line="1763"/>
        <location filename="../Dekko/server/servicehandler.cpp" line="1803"/>
        <location filename="../Dekko/server/servicehandler.cpp" line="1842"/>
        <location filename="../Dekko/server/servicehandler.cpp" line="1882"/>
        <location filename="../Dekko/server/servicehandler.cpp" line="1918"/>
        <location filename="../Dekko/server/servicehandler.cpp" line="1951"/>
        <location filename="../Dekko/server/servicehandler.cpp" line="1988"/>
        <location filename="../Dekko/server/servicehandler.cpp" line="2056"/>
        <location filename="../Dekko/server/servicehandler.cpp" line="2111"/>
        <location filename="../Dekko/server/servicehandler.cpp" line="2188"/>
        <location filename="../Dekko/server/servicehandler.cpp" line="2397"/>
        <location filename="../Dekko/server/servicehandler.cpp" line="2517"/>
        <location filename="../Dekko/server/servicehandler.cpp" line="2553"/>
        <location filename="../Dekko/server/servicehandler.cpp" line="2584"/>
        <location filename="../Dekko/server/servicehandler.cpp" line="2680"/>
        <location filename="../Dekko/server/servicehandler.cpp" line="2949"/>
        <location filename="../upstream/qmf/tools/messageserver/servicehandler.cpp" line="1459"/>
        <location filename="../upstream/qmf/tools/messageserver/servicehandler.cpp" line="1570"/>
        <location filename="../upstream/qmf/tools/messageserver/servicehandler.cpp" line="1608"/>
        <location filename="../upstream/qmf/tools/messageserver/servicehandler.cpp" line="1646"/>
        <location filename="../upstream/qmf/tools/messageserver/servicehandler.cpp" line="1682"/>
        <location filename="../upstream/qmf/tools/messageserver/servicehandler.cpp" line="1718"/>
        <location filename="../upstream/qmf/tools/messageserver/servicehandler.cpp" line="1759"/>
        <location filename="../upstream/qmf/tools/messageserver/servicehandler.cpp" line="1799"/>
        <location filename="../upstream/qmf/tools/messageserver/servicehandler.cpp" line="1838"/>
        <location filename="../upstream/qmf/tools/messageserver/servicehandler.cpp" line="1878"/>
        <location filename="../upstream/qmf/tools/messageserver/servicehandler.cpp" line="1914"/>
        <location filename="../upstream/qmf/tools/messageserver/servicehandler.cpp" line="1947"/>
        <location filename="../upstream/qmf/tools/messageserver/servicehandler.cpp" line="1984"/>
        <location filename="../upstream/qmf/tools/messageserver/servicehandler.cpp" line="2052"/>
        <location filename="../upstream/qmf/tools/messageserver/servicehandler.cpp" line="2107"/>
        <location filename="../upstream/qmf/tools/messageserver/servicehandler.cpp" line="2184"/>
        <location filename="../upstream/qmf/tools/messageserver/servicehandler.cpp" line="2393"/>
        <location filename="../upstream/qmf/tools/messageserver/servicehandler.cpp" line="2513"/>
        <location filename="../upstream/qmf/tools/messageserver/servicehandler.cpp" line="2549"/>
        <location filename="../upstream/qmf/tools/messageserver/servicehandler.cpp" line="2580"/>
        <location filename="../upstream/qmf/tools/messageserver/servicehandler.cpp" line="2676"/>
        <location filename="../upstream/qmf/tools/messageserver/servicehandler.cpp" line="2945"/>
        <source>Unable to locate source for account</source>
        <translation>No s&apos;ha pogut trobar la font del compte</translation>
    </message>
    <message>
        <location filename="../Dekko/server/servicehandler.cpp" line="1500"/>
        <location filename="../Dekko/server/servicehandler.cpp" line="1536"/>
        <location filename="../upstream/qmf/tools/messageserver/servicehandler.cpp" line="1496"/>
        <location filename="../upstream/qmf/tools/messageserver/servicehandler.cpp" line="1532"/>
        <source>Unable to locate sink for account</source>
        <translation>No s&apos;ha pogut trobar el col·lector del compte</translation>
    </message>
    <message>
        <location filename="../Dekko/server/servicehandler.cpp" line="1547"/>
        <location filename="../upstream/qmf/tools/messageserver/servicehandler.cpp" line="1543"/>
        <source>Unable to retrieve folder list for unconfigured account</source>
        <translation>No s&apos;ha pogut aconseguir la llista de carpetes del compte sense configuració</translation>
    </message>
    <message>
        <location filename="../Dekko/server/servicehandler.cpp" line="1585"/>
        <location filename="../Dekko/server/servicehandler.cpp" line="1623"/>
        <location filename="../upstream/qmf/tools/messageserver/servicehandler.cpp" line="1581"/>
        <location filename="../upstream/qmf/tools/messageserver/servicehandler.cpp" line="1619"/>
        <source>Unable to retrieve message list for unconfigured account</source>
        <translation>No s&apos;ha pogut aconseguir la llista de missatges del compte sense configuració</translation>
    </message>
    <message>
        <location filename="../Dekko/server/servicehandler.cpp" line="1661"/>
        <location filename="../upstream/qmf/tools/messageserver/servicehandler.cpp" line="1657"/>
        <source>Unable to retrieve new messages for unconfigured account</source>
        <translation>No s&apos;ha pogut aconseguir la llista dels missatges nous del compte sense configuració</translation>
    </message>
    <message>
        <location filename="../Dekko/server/servicehandler.cpp" line="1697"/>
        <location filename="../upstream/qmf/tools/messageserver/servicehandler.cpp" line="1693"/>
        <source>Unable to retrieve standard folders for unconfigured account</source>
        <translation>No s&apos;ha pogut aconseguir les carpetes estàndard del compte sense configuració</translation>
    </message>
    <message>
        <location filename="../Dekko/server/servicehandler.cpp" line="1733"/>
        <location filename="../upstream/qmf/tools/messageserver/servicehandler.cpp" line="1729"/>
        <source>Unable to retrieve messages for unconfigured account</source>
        <translation>No s&apos;ha pogut aconseguir els missatges del compte sense configuració</translation>
    </message>
    <message>
        <location filename="../Dekko/server/servicehandler.cpp" line="1777"/>
        <location filename="../upstream/qmf/tools/messageserver/servicehandler.cpp" line="1773"/>
        <source>Unable to retrieve message part for unconfigured account</source>
        <translation>No s&apos;ha pogut aconseguir la part del missatge del compte sense configuració</translation>
    </message>
    <message>
        <location filename="../Dekko/server/servicehandler.cpp" line="1815"/>
        <location filename="../upstream/qmf/tools/messageserver/servicehandler.cpp" line="1811"/>
        <source>Unable to retrieve message range for unconfigured account</source>
        <translation>No s&apos;ha pogut aconseguir l&apos;interval de missatges del compte sense configuració</translation>
    </message>
    <message>
        <location filename="../Dekko/server/servicehandler.cpp" line="1854"/>
        <location filename="../upstream/qmf/tools/messageserver/servicehandler.cpp" line="1850"/>
        <source>Unable to retrieve message part range for unconfigured account</source>
        <translation>No s&apos;ha pogut aconseguir l&apos;interval de les parts dels missatges del compte sense configuració</translation>
    </message>
    <message>
        <location filename="../Dekko/server/servicehandler.cpp" line="1893"/>
        <location filename="../upstream/qmf/tools/messageserver/servicehandler.cpp" line="1889"/>
        <source>Unable to retrieve all messages for unconfigured account</source>
        <translation>No s&apos;ha pogut aconseguir la totalitat dels missatges del compte sense configuració</translation>
    </message>
    <message>
        <location filename="../Dekko/server/servicehandler.cpp" line="1929"/>
        <location filename="../upstream/qmf/tools/messageserver/servicehandler.cpp" line="1925"/>
        <source>Unable to export updates for unconfigured account</source>
        <translation>No s&apos;ha pogut exportar les actualitzacions del compte sense configuració</translation>
    </message>
    <message>
        <location filename="../Dekko/server/servicehandler.cpp" line="1962"/>
        <location filename="../upstream/qmf/tools/messageserver/servicehandler.cpp" line="1958"/>
        <source>Unable to synchronize unconfigured account</source>
        <translation>No s&apos;ha pogut sincronitzar el compte sense configuració</translation>
    </message>
    <message>
        <location filename="../Dekko/server/servicehandler.cpp" line="2006"/>
        <location filename="../upstream/qmf/tools/messageserver/servicehandler.cpp" line="2002"/>
        <source>Unable to delete messages for unconfigured account</source>
        <translation>No s&apos;han pogut esborrar els missatges del compte sense configuració</translation>
    </message>
    <message>
        <location filename="../Dekko/server/servicehandler.cpp" line="2008"/>
        <location filename="../upstream/qmf/tools/messageserver/servicehandler.cpp" line="2004"/>
        <source>Deleting messages</source>
        <translation>Esborrant els missatges</translation>
    </message>
    <message>
        <location filename="../Dekko/server/servicehandler.cpp" line="2026"/>
        <location filename="../upstream/qmf/tools/messageserver/servicehandler.cpp" line="2022"/>
        <source>Unable to discard messages</source>
        <translation>No s&apos;han pogut descartar els missatges</translation>
    </message>
    <message>
        <location filename="../Dekko/server/servicehandler.cpp" line="2075"/>
        <location filename="../Dekko/server/servicehandler.cpp" line="2096"/>
        <location filename="../upstream/qmf/tools/messageserver/servicehandler.cpp" line="2071"/>
        <location filename="../upstream/qmf/tools/messageserver/servicehandler.cpp" line="2092"/>
        <source>Unable to copy messages to unconfigured account</source>
        <translation>No s&apos;han pogut copiar els missatges al compte sense configuració</translation>
    </message>
    <message>
        <location filename="../Dekko/server/servicehandler.cpp" line="2077"/>
        <location filename="../upstream/qmf/tools/messageserver/servicehandler.cpp" line="2073"/>
        <source>Unable to copy messages to multiple destination accounts!</source>
        <translation>No s&apos;han pogut copiar els missatges als comptes destí</translation>
    </message>
    <message>
        <location filename="../Dekko/server/servicehandler.cpp" line="2144"/>
        <location filename="../upstream/qmf/tools/messageserver/servicehandler.cpp" line="2140"/>
        <source>Unable to copy messages for account</source>
        <translation>No s&apos;han pogut copiar els missatges del compte</translation>
    </message>
    <message>
        <location filename="../Dekko/server/servicehandler.cpp" line="2162"/>
        <location filename="../upstream/qmf/tools/messageserver/servicehandler.cpp" line="2158"/>
        <source>Unable to move messages for unconfigured account</source>
        <translation>No s&apos;han pogut moure els missatges al compte sense configuració</translation>
    </message>
    <message>
        <location filename="../Dekko/server/servicehandler.cpp" line="2203"/>
        <location filename="../upstream/qmf/tools/messageserver/servicehandler.cpp" line="2199"/>
        <source>Unable to flag messages for unconfigured account</source>
        <translation>No s&apos;han pogut marcar els missatges al compte sense configuració</translation>
    </message>
    <message>
        <location filename="../Dekko/server/servicehandler.cpp" line="2216"/>
        <location filename="../upstream/qmf/tools/messageserver/servicehandler.cpp" line="2212"/>
        <source>Unable to async add messages</source>
        <translation>No s&apos;han pogut afegir els missatges asincrònicament</translation>
    </message>
    <message>
        <location filename="../Dekko/server/servicehandler.cpp" line="2218"/>
        <location filename="../Dekko/server/servicehandler.cpp" line="2305"/>
        <location filename="../Dekko/server/servicehandler.cpp" line="2365"/>
        <location filename="../upstream/qmf/tools/messageserver/servicehandler.cpp" line="2214"/>
        <location filename="../upstream/qmf/tools/messageserver/servicehandler.cpp" line="2301"/>
        <location filename="../upstream/qmf/tools/messageserver/servicehandler.cpp" line="2361"/>
        <source>Unable to async update messages</source>
        <translation>No s&apos;han pogut actualitzar els missatges asincrònicament</translation>
    </message>
    <message>
        <location filename="../Dekko/server/servicehandler.cpp" line="2275"/>
        <location filename="../upstream/qmf/tools/messageserver/servicehandler.cpp" line="2271"/>
        <source>Unable to async add messages, inconsistent contentscheme</source>
        <translation>No s&apos;han pogut afegir els missatges asincrònicament, contentscheme inconsistent</translation>
    </message>
    <message>
        <location filename="../Dekko/server/servicehandler.cpp" line="2327"/>
        <location filename="../upstream/qmf/tools/messageserver/servicehandler.cpp" line="2323"/>
        <source>Unable to async update messages, inconsistent contentscheme</source>
        <translation>No s&apos;han pogut actualitzar els missatges asincrònicament, contentscheme inconsistent</translation>
    </message>
    <message>
        <location filename="../Dekko/server/servicehandler.cpp" line="2412"/>
        <location filename="../upstream/qmf/tools/messageserver/servicehandler.cpp" line="2408"/>
        <source>Could not delete messages</source>
        <translation>No s&apos;han pogut esborrar els missatges</translation>
    </message>
    <message>
        <location filename="../Dekko/server/servicehandler.cpp" line="2493"/>
        <location filename="../upstream/qmf/tools/messageserver/servicehandler.cpp" line="2489"/>
        <source>Unable to create folder for invalid account</source>
        <translation>No s&apos;ha pogut crear la carpeta pel compte invàlid</translation>
    </message>
    <message>
        <location filename="../Dekko/server/servicehandler.cpp" line="2530"/>
        <location filename="../upstream/qmf/tools/messageserver/servicehandler.cpp" line="2526"/>
        <source>Unable to rename invalid folder</source>
        <translation>No s&apos;ha pogut reanomenar la carpeta invàlida</translation>
    </message>
    <message>
        <location filename="../Dekko/server/servicehandler.cpp" line="2566"/>
        <location filename="../upstream/qmf/tools/messageserver/servicehandler.cpp" line="2562"/>
        <source>Unable to delete invalid folder</source>
        <translation>No s&apos;ha pogut eliminar la carpeta invàlida</translation>
    </message>
    <message>
        <location filename="../Dekko/server/servicehandler.cpp" line="2612"/>
        <location filename="../upstream/qmf/tools/messageserver/servicehandler.cpp" line="2608"/>
        <source>Unable to search messages for unconfigured account</source>
        <translation>No s&apos;ha pogut cercar els missatges al compte sense configuració</translation>
    </message>
    <message>
        <location filename="../Dekko/server/servicehandler.cpp" line="2926"/>
        <location filename="../upstream/qmf/tools/messageserver/servicehandler.cpp" line="2922"/>
        <source>Unable to forward protocol-specific request for unconfigured account</source>
        <translation>No s&apos;ha pogut passar la petició de protocol específic al compte sense configuració</translation>
    </message>
    <message>
        <location filename="../Dekko/server/servicehandler.cpp" line="3174"/>
        <location filename="../upstream/qmf/tools/messageserver/servicehandler.cpp" line="3170"/>
        <source>Failed to perform requested action!</source>
        <translation>No s&apos;ha pogut realitzar l&apos;acció sol·licitada</translation>
    </message>
</context>
<context>
    <name>Settings</name>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/settings/Settings.qml" line="11"/>
        <source>Settings</source>
        <translation>Configuració</translation>
    </message>
</context>
<context>
    <name>SettingsWindow</name>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/settings/SettingsWindow.qml" line="26"/>
        <source>Dekko Settings</source>
        <translation>Configuració del Dekko</translation>
    </message>
</context>
<context>
    <name>SettingsWorker</name>
    <message>
        <location filename="../Dekko/workers/SettingsWorker.qml" line="103"/>
        <source>Account saved</source>
        <translation>Compte desat</translation>
    </message>
</context>
<context>
    <name>SetupWizardWindow</name>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/setupwizard/SetupWizardWindow.qml" line="20"/>
        <source>Mail Setup Wizard</source>
        <translation>Assistent de configuració del correu</translation>
    </message>
</context>
<context>
    <name>SigEntry</name>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/smtp/smtpsettings.cpp" line="79"/>
        <source>Signature</source>
        <translation type="unfinished">Signatura</translation>
    </message>
</context>
<context>
    <name>SmartFolderDelegate</name>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/delegates/SmartFolderDelegate.qml" line="155"/>
        <source>Inbox (%1)</source>
        <translation>Safata d&apos;entrada (%1)</translation>
    </message>
</context>
<context>
    <name>SmartFolderSet</name>
    <message>
        <location filename="../Dekko/backend/mail/MessageSet.cpp" line="332"/>
        <source>Today, %1</source>
        <translation>Avui, %1</translation>
    </message>
</context>
<context>
    <name>SmtpAccountConfiguration</name>
    <message>
        <location filename="../Dekko/backend/accounts/AccountConfiguration.cpp" line="459"/>
        <source>Sent with Dekko</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SmtpClient</name>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/smtp/smtpclient.cpp" line="236"/>
        <source>Cannot send message; transport in use</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/smtp/smtpclient.cpp" line="242"/>
        <source>Cannot send message without account configuration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/smtp/smtpclient.cpp" line="249"/>
        <source>Cannot send message without SMTP server configuration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/smtp/smtpclient.cpp" line="353"/>
        <source>Connected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/smtp/smtpclient.cpp" line="863"/>
        <source>Sending: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/smtp/smtpclient.cpp" line="929"/>
        <source>Received response 354 while sending.</source>
        <translation type="unfinished"></translation>
    </message>
    <message numerus="yes">
        <location filename="../upstream/qmf/plugins/messageservices/smtp/smtpclient.cpp" line="1060"/>
        <source>Sent %n messages</source>
        <translation type="unfinished">
            <numerusform></numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/smtp/smtpclient.cpp" line="1221"/>
        <source>Have not received any greeting from SMTP server, probably configuration error</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SmtpSettings</name>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/smtp/smtpsettings.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/smtp/smtpsettings.ui" line="56"/>
        <source>From</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/smtp/smtpsettings.ui" line="82"/>
        <source>Email</source>
        <translation type="unfinished">Correu electrònic</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/smtp/smtpsettings.ui" line="108"/>
        <source>Server</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/smtp/smtpsettings.ui" line="134"/>
        <source>Port</source>
        <translation type="unfinished">Port</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/smtp/smtpsettings.ui" line="160"/>
        <source>Encryption</source>
        <translation type="unfinished">Xifratge</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/smtp/smtpsettings.ui" line="177"/>
        <location filename="../upstream/qmf/plugins/messageservices/smtp/smtpsettings.ui" line="218"/>
        <source>None</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/smtp/smtpsettings.ui" line="182"/>
        <source>SSL</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/smtp/smtpsettings.ui" line="187"/>
        <source>TLS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/smtp/smtpsettings.ui" line="201"/>
        <source>Authentication</source>
        <translation type="unfinished">Autenticació</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/smtp/smtpsettings.ui" line="223"/>
        <source>Login</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/smtp/smtpsettings.ui" line="228"/>
        <source>Plain</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/smtp/smtpsettings.ui" line="233"/>
        <source>Cram MD5</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/smtp/smtpsettings.ui" line="247"/>
        <source>Username</source>
        <translation type="unfinished">Nom d&apos;usuari</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/smtp/smtpsettings.ui" line="276"/>
        <source>Password</source>
        <translation type="unfinished">Contrasenya</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/smtp/smtpsettings.ui" line="308"/>
        <source>Signature</source>
        <translation type="unfinished">Signatura</translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/smtp/smtpsettings.ui" line="324"/>
        <source>Set...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../upstream/qmf/plugins/messageservices/smtp/smtpsettings.ui" line="337"/>
        <source>Default sending account</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SubjectField</name>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/composer/SubjectField.qml" line="56"/>
        <source>Subject:</source>
        <translation>Assumpte:</translation>
    </message>
</context>
<context>
    <name>SyncState</name>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/setupwizard/states/SyncState.qml" line="35"/>
        <source>Synchronizing account.</source>
        <translation>S&apos;està sincronitzant el compte.</translation>
    </message>
</context>
<context>
    <name>TitledTextField</name>
    <message>
        <location filename="../plugins/ubuntu-plugin/imports/components/TitledTextField.qml" line="62"/>
        <source> (Required)</source>
        <translation> (Requerit)</translation>
    </message>
</context>
<context>
    <name>UserInputUI</name>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/setupwizard/components/UserInputUI.qml" line="36"/>
        <source>Name</source>
        <translation>Nom</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/setupwizard/components/UserInputUI.qml" line="37"/>
        <source>Full name</source>
        <translation>Nom complet</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/setupwizard/components/UserInputUI.qml" line="44"/>
        <source>Description</source>
        <translation>Descripció</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/setupwizard/components/UserInputUI.qml" line="45"/>
        <source>E.g Home, Work...</source>
        <translation>Per exemple Casa, Feina...</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/setupwizard/components/UserInputUI.qml" line="52"/>
        <source>Email address</source>
        <translation>Adreça electrònica</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/setupwizard/components/UserInputUI.qml" line="54"/>
        <source>email@example.org</source>
        <translation>email@exemple.org</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/setupwizard/components/UserInputUI.qml" line="60"/>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/setupwizard/components/UserInputUI.qml" line="63"/>
        <source>Password</source>
        <translation>Contrasenya</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/setupwizard/components/UserInputUI.qml" line="69"/>
        <source>Show password</source>
        <translation>Mostra la contrasenya</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/setupwizard/components/UserInputUI.qml" line="79"/>
        <source>Cancel</source>
        <translation>Cancel·la</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/setupwizard/components/UserInputUI.qml" line="83"/>
        <source>Next</source>
        <translation>Següent</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/setupwizard/components/UserInputUI.qml" line="125"/>
        <source>Password empty</source>
        <translation>Contrasenya buida</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/setupwizard/components/UserInputUI.qml" line="125"/>
        <source>Would you like to continue?</source>
        <translation>Voldríeu continuar?</translation>
    </message>
</context>
<context>
    <name>ValidationState</name>
    <message>
        <location filename="../plugins/ubuntu-plugin/plugins/core/mail/setupwizard/states/ValidationState.qml" line="36"/>
        <source>Validating credentials.</source>
        <translation>S&apos;estan validant les credencials.</translation>
    </message>
</context>
<context>
    <name>VersionDialog</name>
    <message>
        <location filename="../plugins/ubuntu-plugin/imports/dialogs/VersionDialog.qml" line="26"/>
        <source>Version</source>
        <translation>Versió</translation>
    </message>
    <message>
        <location filename="../plugins/ubuntu-plugin/imports/dialogs/VersionDialog.qml" line="30"/>
        <source>Close</source>
        <translation>Tanca</translation>
    </message>
</context>
<context>
    <name>ViewStore</name>
    <message>
        <location filename="../Dekko/stores/Views/ViewStore.qml" line="70"/>
        <source>Messages sent</source>
        <translation>S&apos;han enviat els missatges</translation>
    </message>
    <message>
        <location filename="../Dekko/stores/Views/ViewStore.qml" line="72"/>
        <source>Message sent</source>
        <translation>S&apos;ha enviat el missatge</translation>
    </message>
    <message>
        <location filename="../Dekko/stores/Views/ViewStore.qml" line="75"/>
        <source>Message sending failed</source>
        <translation>El missatge no s&apos;ha enviat</translation>
    </message>
</context>
</TS>
