Dekko includes an developer wiki included in the source. Please see the files in the `docs` subfolder. Just be aware, that they may need some updating...

To update translation files, open dekko main folder in a terminal and run the following command:

`python3 ./scripts/update-i18n .`

This python script will parse all source files in all subfolders and update the translation files in the `i18n` folder.
