/* Copyright (C) 2016 - 2017 Dan Chapman <dpniel@ubuntu.com>

   This file is part of Dekko email client for Ubuntu devices

   This program is free software; you can redistribute it and/or
   modify it under the terms of the GNU General Public License as
   published by the Free Software Foundation; either version 2 of
   the License or (at your option) version 3

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
import QtQuick 2.4
import QtQuick.Controls.Suru 2.2
import Lomiri.Components 1.3
import Lomiri.Components.Popups 1.3
import QtWebEngine 1.5 
import Dekko.Mail 1.0
import Dekko.Components 1.0
import Dekko.Mail.Settings 1.0
import Dekko.Mail.API 1.0
import Dekko.Mail.Stores.Views 1.0
import Dekko.Lomiri.Components 1.0
import "../components"
import "../webview"
import "../composer"
import Dekko.Lomiri.Constants 1.0

DekkoPage {

    // Hide the default header
    pageHeader.showDivider: false
    pageHeader.visible: false

    DefaultMessagePageHeader {
        id: msgPgHeader
        anchors {
            left: parent.left
            top: parent.top
            right: parent.right
        }
        msg: message
    }

    property alias msgId: message.messageId

    Connections {
        target: ViewStore
        onFormFactorChanged: {
            if ((ViewStore.formFactor !== "small") && (stageArea.stageID === ViewKeys.messageListStack)) {
                ViewActions.switchMessageViewLocation(ViewKeys.messageListStack, msgId)
            } else if ((ViewStore.formFactor === "small") && (stageArea.stageID === ViewKeys.messageViewStack)) {
                ViewActions.switchMessageViewLocation(ViewKeys.messageViewStack, msgId)
            }
        }
    }

    Message {
        id: message
        onBodyChanged: {
            webview.setCidQuery(message.messageId)
            webview.setBodyUrl(body)
            if (!message.isRead) {
                switch(mailPolicy.markRead) {
                case MailPolicy.Never:
                    Log.logInfo("DefaultMessagePage::onBodyChanged", "Not marking message as read");
                    break; // do nothing
                case MailPolicy.AfterInterval:
                    Log.logInfo("DefaultMessagePage::onBodyChanged", "Marking message read after interval");
                    markReadTimer.start()
                    break
                case MailPolicy.Immediately:
                    Log.logInfo("DefaultMessagePage::onBodyChanged", "Marking message read immediately");
                    MessageActions.markMessageRead(message.messageId, true)
                    break
                }
            }
        }
    }

    MailPolicy {
        id: mailPolicy
        accountId: message.parentAccountId
    }

    Timer {
        id: markReadTimer
        interval: mailPolicy.markAsReadInterval
        repeat: false
        onTriggered: MessageActions.markMessageRead(message.messageId, true)
    }

    ContentBlockedNotice {
        id: contentBlockedItem
        contentBlocked: webview.contentBlocked
        anchors {
            left: parent.left
            top: msgPgHeader.bottom
            right: parent.right
        }
        onAllowClicked: webview.allowBlockedContent()
    }

    Item {
        id: container
        clip: true
        anchors {
            left: parent.left
            right: parent.right
            top: contentBlockedItem.bottom
            bottom: hiddenMargin.top
        }

        DekkoWebView {
            id: webview
            anchors {
                left: parent.left
                right: parent.right
                top: detailsPanel.bottom
                bottom: parent.bottom
                leftMargin: !dekko.isSmallFF ? Style.smallSpacing : 0
            }
        }

        LomiriShape {
            // Allows to copy the currently selected text to the clipboard.
            //
            // Ideas for this shape taken from Morph Browser
            // https://gitlab.com/ubports/development/core/morph-browser
            // licensed under GPLv3
            id: contextMenuShape

            property string contextContentText: ""
            property bool contextContentIsMail: contextContentText.startsWith("mailto:")

            backgroundColor: theme.palette.normal.overlay
            width: actionShapeRow.width + units.gu(1)
            height: copyShape.height + units.gu(0.2) + (linkToCopyOrOpenLabel.visible ? linkToCopyOrOpenLabel.contentHeight + units.gu(0.1) : 0)

            visible: false

            MouseArea {
                // to catch clicks on edges of the shape and the spacing of the Row which would
                // otherwise go through to the underlying page as the space is not covered
                // by the MouseAreas of the Row children
                anchors.fill: parent
            }

            Row {
                id: actionShapeRow
                spacing: units.gu(3)

                anchors {
                    horizontalCenter: parent.horizontalCenter
                    top: parent.top
                    topMargin: units.gu(0.1)
                }

                LomiriShape {
                    width: gripImage.width
                    height: copyShape.height // height should be as for the other shapes
                    backgroundColor: theme.palette.normal.overlay
                    aspect: LomiriShape.Flat
                    anchors.verticalCenter: parent.verticalCenter

                    Icon {
                        id: gripIcon
                        name: "grip-large"
                        visible: false
                    }

                    Image {
                        id: gripImage
                        height: parent.height
                        width: units.gu(3)

                        anchors {
                            top: parent.top
                            left: parent.left
                        }

                        source: gripIcon.source
                        fillMode: Image.PreserveAspectCrop
                    }

                    MouseArea {
                        anchors.fill: parent
                        drag.target: contextMenuShape
                        drag.axis: Drag.XAndYAxis
                    }
                }

                LomiriShape {
                    id: selectAllShape
                    width: (selectAllIcon.width > selectAllLabel.contentWidth ? selectAllIcon.width : selectAllLabel.contentWidth)
                    height: selectAllIcon.height + selectAllLabel.contentHeight + units.gu(1.5)
                    backgroundColor: theme.palette.normal.overlay
                    aspect: LomiriShape.Flat

                    Icon {
                        id: selectAllIcon
                        name: "edit-select-all"
                        height: units.gu(3)

                        anchors {
                            top: parent.top
                            topMargin: units.gu(0.5)
                            horizontalCenter: parent.horizontalCenter
                        }
                    }

                    Label {
                        id: selectAllLabel
                        anchors {
                            top: selectAllIcon.bottom
                            topMargin: units.gu(0.5)
                            horizontalCenter: parent.horizontalCenter
                        }
                        text: i18n.tr("Select all")
                        fontSize: "x-small"
                    }

                    MouseArea {
                        anchors.fill: parent
                        onClicked: {
                            webview.triggerWebAction(WebEngineView.SelectAll)
                        }
                    }
                }

                LomiriShape {
                    id: copyShape
                    width: (copyIcon.width > copyLabel.contentWidth ? copyIcon.width : copyLabel.contentWidth)
                    height: copyIcon.height + copyLabel.contentHeight + units.gu(1.5)
                    backgroundColor: theme.palette.normal.overlay
                    aspect: LomiriShape.Flat

                    Icon {
                        id: copyIcon
                        name: "edit-copy"
                        height: units.gu(3)

                        anchors {
                            top: parent.top
                            topMargin: units.gu(0.5)
                            horizontalCenter: parent.horizontalCenter
                        }
                    }

                    Label {
                        id: copyLabel
                        anchors {
                            top: copyIcon.bottom
                            topMargin: units.gu(0.5)
                            horizontalCenter: parent.horizontalCenter
                        }
                        text: i18n.tr("Copy")
                        fontSize: "x-small"
                    }

                    MouseArea {
                        anchors.fill: parent
                        onClicked: {
                            if (contextMenuShape.contextContentText !== "") {
                                webview.triggerWebAction(WebEngineView.CopyLinkToClipboard)
                            } else {
                                webview.triggerWebAction(WebEngineView.Copy)
                            }
                            contextMenuShape.visible = false
                        }
                    }
                }

                LomiriShape {
                    id: openUrlShape
                    width: (openUrlIcon.width > openUrlLabel.contentWidth ? openUrlIcon.width : openUrlLabel.contentWidth)
                    height: openUrlIcon.height + openUrlLabel.contentHeight + units.gu(1.5)
                    backgroundColor: theme.palette.normal.overlay
                    aspect: LomiriShape.Flat
                    visible: contextMenuShape.contextContentText !== ""

                    Icon {
                        id: openUrlIcon
                        name: contextMenuShape.contextContentIsMail ? "mail-unread" : "external-link"
                        height: units.gu(3)

                        anchors {
                            top: parent.top
                            topMargin: units.gu(0.5)
                            horizontalCenter: parent.horizontalCenter
                        }
                    }

                    Label {
                        id: openUrlLabel
                        anchors {
                            top: openUrlIcon.bottom
                            topMargin: units.gu(0.5)
                            horizontalCenter: parent.horizontalCenter
                        }
                        text: contextMenuShape.contextContentIsMail ? i18n.tr("New mail") : i18n.tr("Open link")
                        fontSize: "x-small"
                    }

                    MouseArea {
                        anchors.fill: parent
                        onClicked: {
                            Qt.openUrlExternally(contextMenuShape.contextContentText)
                            contextMenuShape.visible = false
                        }
                    }
                }

                LomiriShape {
                    width: (closeIcon.width > closeLabel.contentWidth ? closeIcon.width : closeLabel.contentWidth)
                    height: closeIcon.height + closeLabel.contentHeight + units.gu(1.5)
                    backgroundColor: theme.palette.normal.overlay
                    aspect: LomiriShape.Flat

                    Icon {
                        id: closeIcon
                        name: "close"
                        height: units.gu(3)

                        anchors {
                            top: parent.top
                            topMargin: units.gu(0.5)
                            horizontalCenter: parent.horizontalCenter
                        }
                    }

                    Label {
                        id: closeLabel
                        anchors {
                            top: closeIcon.bottom
                            topMargin: units.gu(0.5)
                            horizontalCenter: parent.horizontalCenter
                        }
                        text: i18n.tr("Close")
                        fontSize: "x-small"
                    }

                    MouseArea {
                        anchors.fill: parent
                        onClicked: {
                            contextMenuShape.visible = false
                        }
                    }
                }
            }

            Label {
                id: linkToCopyOrOpenLabel
                text: contextMenuShape.contextContentText
                width: parent.width - units.gu(1)
                anchors {
                    top: actionShapeRow.bottom
                    topMargin: units.gu(0.1)
                    left: parent.left
                    leftMargin: units.gu(0.5)
                }
                wrapMode: Text.Wrap
                elide: Text.ElideRight
                maximumLineCount: 2
                visible: text !== ""
            }
        }

        MessageHeader {
            id: h
            msg: message
            detailsVisible: detailsPanel.showDetails
            width: parent.width
            onShowDetails: detailsPanel.showDetails = !detailsPanel.showDetails
        }

        ListItem {
            id: detailsPanel
            property bool showDetails: false
            property int detailsHeight: details.height > units.gu(30) ? units.gu(30) : details.height
            color: Suru.backgroundColor
            anchors {
                left: parent.left
                right: parent.right
                top: h.bottom
            }
            height: 0
            expansion.height: detailsHeight
            expansion.expanded: showDetails
            divider.visible: showDetails
            Flickable {
                anchors.fill: parent
                contentHeight: details.height
                Column {
                    id: details
                    anchors {
                        left: parent.left
                        leftMargin: Style.smallSpacing
                        right: parent.right
                        rightMargin: Style.smallSpacing
                        top: parent.top
                    }
                    ListItem {
                        height: units.gu(5)
                        Item {
                            id: p
                            width: tl.width + Style.smallSpacing
                            anchors {
                                left: parent.left
                                top: parent.top
                                bottom: parent.bottom
                            }

                            Label {
                                id: tl
                                anchors {
                                    left: parent.left
                                    top: parent.top
                                    topMargin: Style.defaultSpacing
                                    leftMargin: Style.smallSpacing
                                }
                                text: qsTr("From:")
                            }
                        }
                        LomiriShape {
                            id: delegate
                            aspect: LomiriShape.Flat
                            color: Qt.rgba(0, 0, 0, 0.05)
                            radius: "small"
                            height: units.gu(3)
                            width: inner_avatar.width + label.width + units.gu(1.5)
                            anchors {
                                left: p.right
                                leftMargin: Style.smallSpacing
                                top: parent.top
                                topMargin: units.gu(1.5)
                            }

                            Avatar {
                                id: inner_avatar
                                width: height
                                anchors {
                                    left: parent.left
                                    top: parent.top
                                    bottom: parent.bottom
                                    margins: units.dp(1)
                                }
                                name: message.from.name
                                initials: message.from.initials
                                email: message.from.address
                                fontSize: "x-small"
                                validContact: true
                            }

                            Label {
                                id: label
                                anchors {
                                    left: inner_avatar.right
                                    leftMargin: units.gu(0.5)
                                    verticalCenter: parent.verticalCenter
                                }
                                text: message.from.name
                            }

                            MouseArea {
                                anchors.fill: parent
                                onClicked: {
                                    PopupUtils.open(Qt.resolvedUrl("../popovers/RecipientPopover.qml"), delegate, {address: message.from})
                                }
                            }
                        }
                    }

                    RecipientFlow {
                        title: qsTr("To:")
                        model: message.to
                    }
                    RecipientFlow {
                        visible: message.cc.count
                        title: qsTr("Cc:")
                        model: message.cc
                    }
                }
            }
        }
    }

    Item {
        id: hiddenMargin
        anchors {
            left: parent.left
            bottom: parent.bottom
            right: parent.right
        }
        height: attachmentPanel.visible ? attachmentPanel.height : 0
    }

    AttachmentPanel {
        id: attachmentPanel
        attachments: message.attachments.model
        isReadOnly: true
        anchors {
            left: parent.left
            bottom: parent.bottom
            right: parent.right
        }
        maxHeight: container.height - Style.defaultSpacing
    }
}

